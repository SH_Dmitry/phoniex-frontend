import * as React from "react";

import {BrowserRouter as Router, Route} from "react-router-dom";

import {MainPage} from "../modules/MainModule";
import {SignInPage} from "../modules/SecurityGateModule";

import styles from "./App.module.less"

export const App = () => {
        return (
                <Router>
                    <div className={styles.root}>
                        <Route path='/main' component={MainPage}/>
                        <Route exact path='/' component={SignInPage}/>
                    </div>
                </Router>
        )
};
