import {ThunkAction} from "redux-thunk";
import {Action} from "redux";
import {AppState} from "./app.reducer";


export type ThunkActionType = ThunkAction<Promise<void>,
    AppState,
    unknown,
    Action<string>>

type ActionType<T> = T extends { [key: string]: infer U } ? U : never
export type GetActionsType<T extends { [key: string]: (...args: any[]) => any }> = ReturnType<ActionType<T>>

export interface DragItem<T> {
    item: T
    type: string
}

//Normalized selectors
export interface INObjects<T> {
    byId: { [id: string]: T }
    allIds: string[]
}

export interface INData<T> {
    [id: string]: T
}

//Empty Object
export const emptyNObject = {
    byId: { },
    allIds: []
};







