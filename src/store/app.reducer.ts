import {reducer as formReducer} from 'redux-form'
import {combineReducers} from "redux";
import {securityReducer} from "../modules/SecurityGateModule";
import {materialsReducer} from "../modules/MaterialsModule";
import {ImportReducer} from "../modules/ImportModule";
import {partInfoReducer, partsReducer} from "../modules/PartsModule";

export const appReducer = combineReducers({
  security:securityReducer,
  parts: partsReducer,
  part_info: partInfoReducer,
  materials: materialsReducer,
  import: ImportReducer,
  form: formReducer
});

export type AppState = ReturnType<typeof appReducer>