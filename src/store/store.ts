import {applyMiddleware, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {appReducer} from "./app.reducer";


const loggerMiddleware = createLogger();


export const store = createStore(
    appReducer,
    applyMiddleware(
       thunkMiddleware,
       loggerMiddleware
    )
);