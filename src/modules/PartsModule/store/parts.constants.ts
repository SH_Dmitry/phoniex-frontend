export const partConstants = {
    GET_PARTS: 'part/get/request' as const,
    GET_PARTS_SUCCESS: 'part/get/success' as const,
    GET_PARTS_FAILURE: 'part/get/failure' as const,

    GET_PART_TYPES: 'part-selectors/get/request',
    GET_PART_TYPES_SUCCESS: 'part-selectors/get/success'  as const,
    GET_PART_TYPES_FAILURE: 'part-selectors/get/failure'  as const,

    ADD_PART:'part/add/request' as const,
    ADD_PART_SUCCESS: 'part/add/success' as const,
    ADD_PART_FAILURE: 'part/add/failure' as const,

    UPDATE_PART:'part/update/request' as const,
    UPDATE_PART_SUCCESS: 'part/update/success' as const,
    UPDATE_PART_FAILURE: 'part/update/failure' as const,

    DELETE_PART:'part/delete/request' as const,
    DELETE_PART_SUCCESS: 'part/delete/success' as const,
    DELETE_PART_FAILURE: 'part/delete/failure' as const,

    ADD_FILTER: 'part/filter/add' as const,
    UPDATE_FILTER: 'part/filter/update' as const,
    DELETE_FILTER: 'part/filter/delete' as const,
    DELETE_ALL_FILTERS: 'part/filter/delete/all' as const,

    UPDATE_SORT: 'part/sort/update' as const,

    GET_PAGE_COUNT: 'part/page_count/get' as const,
    UPDATE_PAGE_SIZE: 'part/page_size/update' as const,
    UPDATE_CURRENT_PAGE: 'part/page/update' as const,
    UPDATE_PART_COUNT: 'part/count/update' as const
};