import {PartHistoryActionTypes} from "./history.operations";
import {partHistoryConstants} from "./history.constants";
import {IPartHistory} from "../../part.types";
import {emptyNObject, INObjects} from "../../../../store/base.types";

const initialState = {
    history: emptyNObject as INObjects<IPartHistory>,
};

export type PartHistoryStateType = typeof initialState


export function historyReducer(state = initialState, action: PartHistoryActionTypes): PartHistoryStateType {
    switch (action.type) {
        case partHistoryConstants.GET_HISTORY_SUCCESS: {
            return {
                ...state,
                history: {
                    byId: action.payload.entities.history,
                    allIds: action.payload.result
                }
            }
        }
        default: return state
    }
}