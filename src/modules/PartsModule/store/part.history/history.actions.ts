import {partHistoryConstants} from "./history.constants";
import {INHistory} from "../../part.types";

export const PartHistoryActions = {
    //Action Success
    getPartHistorySuccess: (payload: INHistory) => ({
        type: partHistoryConstants.GET_HISTORY_SUCCESS,
        payload
    } as const),

    //Action Failure
    getPartHistoryFailure: (error: Error) => ({
        type: partHistoryConstants.GET_HISTORY_FAILURE,
        payload: error,
        error: true
    } as const)
};