import {GetActionsType, ThunkActionType} from "../../../../store/base.types";
import {Dispatch} from "redux";
import {PartHistoryActions} from "./history.actions";
import {partHistoryService} from "../../services/history.service";

export type PartHistoryActionTypes = GetActionsType<typeof PartHistoryActions>
type DispatchType = Dispatch<PartHistoryActionTypes>

export const PartHistoryOperations = {
    getPartHistory: (part_id): ThunkActionType => async (dispatch: DispatchType) => {
        partHistoryService.getPartHistory(part_id)
            .then(part_history => {
                dispatch(PartHistoryActions.getPartHistorySuccess(part_history));
            })
            .catch(error => {
                dispatch(PartHistoryActions.getPartHistoryFailure(error));
            })
    }
};