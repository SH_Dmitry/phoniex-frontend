export const partHistoryConstants = {
    GET_HISTORY: 'part-part.info/part.history/get/request' as const,
    GET_HISTORY_SUCCESS: 'part-part.info/part.history/get/success' as const,
    GET_HISTORY_FAILURE: 'part-part.info/part.history/get/failure' as const,
};