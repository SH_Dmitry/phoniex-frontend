import {PartStructureActionTypes} from "./structure.operations";
import {partStructureConstants} from "./structure.constants";
import {IHierarchyParts, IStructurePart} from "../../part.types";
import {emptyNObject, INObjects} from "../../../../store/base.types";

const _ = require('lodash');


const initialState = {
    structure: emptyNObject as INObjects<IStructurePart>,
    hierarchy: [] as Array<IHierarchyParts>,
};

export type PartStructureStateType = typeof initialState

export function structureReducer(state = initialState, action: PartStructureActionTypes): PartStructureStateType {
    switch (action.type) {

        case partStructureConstants.GET_STRUCTURE_SUCCESS: {
            return {
                ...state,
                structure:
                    {
                        byId: action.payload.entities.parts,
                        allIds: action.payload.result
                    }
            }
        }
        case partStructureConstants.GET_HIERARCHY_SUCCESS: {
            return {
                ...state,
                hierarchy: action.payload
            }
        }
        case partStructureConstants.ADD_PART_SUCCESS: {
            const parts = action.payload.entities.parts;
            return {
                ...state,
                structure:
                    {
                        byId: _.merge(state.structure.byId, parts),
                        allIds: _.union(state.structure.allIds, action.payload.result)
                    }
            }
        }
        case partStructureConstants.UPDATE_PART_COUNT_SUCCESS: {
            return {
                ...state,
                structure: {
                    ...state.structure,
                    byId: {
                        ...state.structure.byId,
                        [action.payload.child_id]: {
                            ...state.structure.byId[action.payload.child_id],
                            count: action.payload.count
                        }
                    }
                }
            }
        }
        case
        partStructureConstants.DELETE_PART_SUCCESS: {
            return {
                ...state,
                structure:
                    {
                        byId: _.omit(state.structure.byId, action.payload.child_id),
                        allIds: _.filter(state.structure.allIds, {part_id: action.payload.child_id})
                    }
            }
        }

        default:
            return state
    }
}