export const partStructureConstants = {
    GET_STRUCTURE: 'part-part.info/part.structure/get/request' as const,
    GET_STRUCTURE_SUCCESS: 'part-part.info/part.structure/get/success' as const,
    GET_STRUCTURE_FAILURE: 'part-part.info/part.structure/get/failure' as const,

    GET_HIERARCHY: 'part-part.info/hierarchy/get/request' as const,
    GET_HIERARCHY_SUCCESS: 'part-part.info/hierarchy/get/success' as const,
    GET_HIERARCHY_FAILURE: 'part-part.info/hierarchy/get/failure' as const,

    ADD_PART: 'part-part.info/part.structure/add/request' as const,
    ADD_PART_SUCCESS: 'part-part.info/part.structure/add/success' as const,
    ADD_PART_FAILURE: 'part-part.info/part.structure/add/failure' as const,

    UPDATE_PART_COUNT: 'part-part.info/part.structure/update/count/request' as const,
    UPDATE_PART_COUNT_SUCCESS: 'part-part.info/part.structure/update/count/success' as const,
    UPDATE_PART_COUNT_FAILURE: 'part-part.info/part.structure/update/count/failure' as const,

    DELETE_PART: 'part-part.info/part.structure/delete/request' as const,
    DELETE_PART_SUCCESS: 'part-part.info/part.structure/delete/success' as const,
    DELETE_PART_FAILURE: 'part-part.info/part.structure/delete/failure' as const,
};