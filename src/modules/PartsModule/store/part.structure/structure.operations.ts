import {GetActionsType, ThunkActionType} from "../../../../store/base.types";
import {Dispatch} from "redux";
import {PartStructureActions} from "./structure.actions";
import {partStructureService} from "../../services/structure.service";

export type PartStructureActionTypes = GetActionsType<typeof PartStructureActions>
type DispatchType = Dispatch<PartStructureActionTypes>

export const PartStructureOperations = {
    getStructure: (part_id): ThunkActionType => async (dispatch: DispatchType) => {
        partStructureService.getPartStructure(part_id)
            .then(parts => {
                dispatch(PartStructureActions.getPartStructureSuccess(parts));
            })
            .catch(error => {
                dispatch(PartStructureActions.getPartStructureFailure(error));
            })
    },
    getHierarchy: (part_id): ThunkActionType => async (dispatch: DispatchType) => {
        partStructureService.getPartHierarchy(part_id)
            .then(parts => {
                dispatch(PartStructureActions.getPartHierarchySuccess(parts));
            })
            .catch(error => {
                dispatch(PartStructureActions.getPartHierarchyFailure(error));
            })
    },
    addPart: (part_id: string, child_id: string): ThunkActionType => async (dispatch: DispatchType) => {
        partStructureService.addPartToStructure(part_id, child_id)
            .then(parts => {
                dispatch(PartStructureActions.addPartSuccess(parts));
            })
            .catch(error => {
                dispatch(PartStructureActions.addPartFailure(error));
            })
    },

    updatePartCount: (part_id: string, child_id: string, count: number): ThunkActionType => async (dispatch: DispatchType) => {
        partStructureService.updatePartCountStructure(part_id, child_id, count)
            .then(() => {
                dispatch(PartStructureActions.updatePartCountSuccess(part_id,child_id,count));
            })
            .catch(error => {
                dispatch(PartStructureActions.updatePartCountFailure(error));
            })
    },

    deletePart: (part_id: string, child_id: string): ThunkActionType => async (dispatch: DispatchType) => {
        partStructureService.deletePartFromStructure(part_id, child_id)
            .then(() => {
                dispatch(PartStructureActions.deletePartSuccess(part_id, child_id));
            })
            .catch(error => {
                dispatch(PartStructureActions.deletePartFailure(error));
            })
    },
};