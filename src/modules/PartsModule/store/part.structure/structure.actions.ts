import {partStructureConstants} from "./structure.constants";
import {IHierarchyParts, INParts} from "../../part.types";


export const PartStructureActions = {

    //Action Success

    //get part hierarchy
    getPartHierarchySuccess: (payload: Array<IHierarchyParts>) => ({
        type: partStructureConstants.GET_HIERARCHY_SUCCESS,
        payload
    } as const),
    //get part part.structure
    getPartStructureSuccess: (payload: INParts) => ({
        type: partStructureConstants.GET_STRUCTURE_SUCCESS,
        payload
    } as const),
    //add part to part.structure
    addPartSuccess: (payload: INParts) => ({
        type: partStructureConstants.ADD_PART_SUCCESS,
        payload
    } as const),
    //update part count in part.structure
    updatePartCountSuccess: (part_id:string, child_id:string, count: number) => ({
        type: partStructureConstants.UPDATE_PART_COUNT_SUCCESS,
        payload: {part_id, child_id, count}
    } as const),
    //delete part from part.structure
    deletePartSuccess: (part_id:string, child_id:string) => ({
        type: partStructureConstants.DELETE_PART_SUCCESS,
        payload: {part_id, child_id}
    } as const),

    //Action failure
    getPartHierarchyFailure: (error: Error) => ({
        type: partStructureConstants.GET_HIERARCHY_FAILURE,
        payload: error,
        error: true
    } as const),
    getPartStructureFailure: (error: Error) => ({
        type: partStructureConstants.GET_STRUCTURE_FAILURE,
        payload: error,
        error: true
    } as const),
    addPartFailure: (error: Error) => ({
        type: partStructureConstants.ADD_PART_FAILURE,
        payload: error,
        error: true
    } as const),
    updatePartCountFailure: (error: Error) => ({
        type: partStructureConstants.UPDATE_PART_COUNT_FAILURE,
        payload: error,
        error: true
    } as const),
    deletePartFailure: (error: Error) => ({
        type: partStructureConstants.DELETE_PART_FAILURE,
        payload: error,
        error: true
    } as const),
};