import {PartCommentsActionTypes} from "./comments.operations";
import {partCommentsConstants} from "./comments.constants";
import {IComment} from "../../part.types";
import {emptyNObject, INObjects} from "../../../../store/base.types";

const _ = require('lodash');


const PartCommentsState = {
    root_comments: [] as Array<string>,
    comments: emptyNObject as INObjects<IComment>,
};

export type PartCommentsStateType = typeof PartCommentsState

export function commentReducer(state = PartCommentsState, action: PartCommentsActionTypes): PartCommentsStateType {
    switch (action.type) {
        case partCommentsConstants.GET_COMMENTS_SUCCESS: {
            const comments = action.payload.entities.comments || {};
            return {
                ...state,
                comments: {
                    byId: action.payload.entities.comments,
                    allIds: Object.getOwnPropertyNames(comments)
                },
                root_comments: action.payload.result
            }
        }
        case partCommentsConstants.ADD_COMMENT_SUCCESS: {
            const comments = action.payload.entities.comments || {};
            return {
                ...state,
                comments: {
                    byId: _.assign(state.comments.byId, comments),
                    allIds: _.union(state.comments.allIds, action.payload.result)
                },
                root_comments: _.union(state.root_comments, action.payload.result)
            };
        }
        case partCommentsConstants.ADD_REPLY_SUCCESS: {
            const comments = action.payload.comment.entities.comments || {};
            const parentId = action.payload.parent_id;
            return {
                ...state,
                comments: {
                    ...state.comments,
                    byId: {
                        ..._.assign(state.comments.byId, comments),
                        [parentId]: {
                            ...state.comments.byId[parentId],
                            replies: _.union(state.comments.byId[parentId].replies, Object.getOwnPropertyNames(comments))
                        }
                    },
                    allIds: _.union(state.comments.allIds, Object.getOwnPropertyNames(comments))
                }
            };
        }
        default: return state
    }
}