import {partCommentsConstants} from "./comments.constants";
import {INComments} from "../../part.types";

export const PartCommentsActions = {
    //Action Success
    getPartCommentsSuccess: (payload: INComments) => ({
        type: partCommentsConstants.GET_COMMENTS_SUCCESS,
        payload
    } as const),
    addCommentToPartSuccess: (payload: INComments) => ({
        type: partCommentsConstants.ADD_COMMENT_SUCCESS,
        payload
    } as const),
    addReplyToCommentSuccess: (parent_id: string, comment: INComments) => ({
        type: partCommentsConstants.ADD_REPLY_SUCCESS,
        payload: {parent_id, comment}
    } as const),

    //Action Failure
    getPartCommentsFailure: (error: Error) => ({
        type: partCommentsConstants.GET_COMMENTS_FAILURE,
        payload: error,
        error: true
    } as const),
    addCommentToPartFailure: (error: Error) => ({
        type: partCommentsConstants.ADD_COMMENT_FAILURE,
        payload: error,
        error: true
    } as const),
    addReplyToCommentFailure: (error: Error) => ({
        type: partCommentsConstants.ADD_REPLY_FAILURE,
        payload: error,
        error: true
    } as const)
};