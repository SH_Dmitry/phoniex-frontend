export const partCommentsConstants = {
    GET_COMMENTS: 'part-part.info/comment/get/request' as const,
    GET_COMMENTS_SUCCESS: 'part-part.info/comment/get/success' as const,
    GET_COMMENTS_FAILURE: 'part-part.info/comment/get/failure' as const,

    ADD_COMMENT: 'part-part.info/comment/add/request' as const,
    ADD_COMMENT_SUCCESS: 'part-part.info/comment/add/success' as const,
    ADD_COMMENT_FAILURE: 'part-part.info/comment/add/failure' as const,

    ADD_REPLY: 'part-part.info/comment/reply/add/request' as const,
    ADD_REPLY_SUCCESS: 'part-part.info/comment/reply/add/success' as const,
    ADD_REPLY_FAILURE: 'part-part.info/comment/reply/add/failure' as const,
};