import {GetActionsType, ThunkActionType} from "../../../../store/base.types";
import {partCommentService} from "../../services/comments.service";
import {PartCommentsActions} from "./comments.actions";
import {Dispatch} from "redux";

export type PartCommentsActionTypes = GetActionsType<typeof PartCommentsActions>
type DispatchType = Dispatch<PartCommentsActionTypes>

export const PartCommentsOperations = {
    getPartComments: (part_id): ThunkActionType => async (dispatch: DispatchType) => {
        partCommentService.getPartComments(part_id)
            .then(result => {
                dispatch(PartCommentsActions.getPartCommentsSuccess(result));
            })
            .catch(error => {
                dispatch(PartCommentsActions.getPartCommentsFailure(error));
            })
    },

    addCommentToPart: (part_id, text: string): ThunkActionType => async (dispatch: DispatchType) => {
        partCommentService.addPartComment(part_id, text)
            .then(result => {
                dispatch(PartCommentsActions.addCommentToPartSuccess(result));
            })
            .catch(error => {
                dispatch(PartCommentsActions.addCommentToPartFailure(error));
            })
    },
    addReplyToComment: (part_id:string, comment_id:string, text: string, ): ThunkActionType => async (dispatch: DispatchType) => {
        partCommentService.addCommentReply(part_id, comment_id, text)
            .then(result => {
                dispatch(PartCommentsActions.addReplyToCommentSuccess(comment_id, result));
            })
            .catch(error => {
                dispatch(PartCommentsActions.addReplyToCommentFailure(error));
            })
    }
};