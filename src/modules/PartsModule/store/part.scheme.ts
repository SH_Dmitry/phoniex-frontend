import {schema} from "normalizr";
import {user} from "../../SecurityGateModule";


const part = new schema.Entity("parts", {},{idAttribute:'part_id'});
export const partsScheme = [part];
export const partScheme = part;

const partType = new schema.Entity("part_types", {}, {idAttribute: 'part_type_id'});
export const partTypeScheme = [partType];

const history = new schema.Entity("history", {user: user}, {idAttribute:"history_id"});
export const historyScheme = [history];

const reply = new schema.Entity('comments', {user: user}, {idAttribute: "comment_id"});
const comments = new schema.Array(reply);
reply.define({replies: comments});
const comment = new schema.Entity("comments", {user: user, replies: comments}, {idAttribute: "comment_id"});
export const commentsScheme = [comment];