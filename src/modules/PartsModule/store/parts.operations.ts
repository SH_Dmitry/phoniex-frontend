import {GetActionsType, ThunkActionType} from "../../../store/base.types";
import {partService} from "../services/parts.service";
import {message} from "antd";
import {PartsActions} from "./parts.actions";
import {Dispatch} from "redux";
import {IFilter, IOption, IPart} from "../part.types";

export type PartActionTypes = GetActionsType<typeof PartsActions>
type DispatchType = Dispatch<PartActionTypes>


const PartsHelpers = {
    getPartHelper: async (dispatch: DispatchType, options: IOption) => {
        try {
            const result = await partService.getParts(options);
            await dispatch(PartsActions.getAllPartsSuccess(result.parts));
            await dispatch(PartsActions.changePartCount(result.part_count))
        }
        catch (error) {
            dispatch(PartsActions.getAllPartsFailure(error));
        }
    }
};


export const PartOperations = {

    addFilter:(filter:IFilter) => {
        return async (dispatch: DispatchType, getState) => {
            dispatch(PartsActions.addFilter(filter));
            await PartsHelpers.getPartHelper(dispatch, {
                pagination: getState().parts.pagination,
                sort: getState().parts.sort,
                filters: getState().parts.filters,
            })
        }
    },
    updateFilter:(filter:IFilter) => {
        return async (dispatch: DispatchType, getState) => {
            dispatch(PartsActions.updateFilter(filter));
            await PartsHelpers.getPartHelper(dispatch, {
                pagination: getState().parts.pagination,
                sort: getState().parts.sort,
                filters: getState().parts.filters,
            })
        }
    },
    deleteFilter:(filter:IFilter) => {
        return async (dispatch: DispatchType, getState) => {
            dispatch(PartsActions.deleteFilter(filter));
            await PartsHelpers.getPartHelper(dispatch, {
                pagination: getState().parts.pagination,
                sort: getState().parts.sort,
                filters: getState().parts.filters,
            })
        }
    },
    deleteAllFilter:() => {
        return async (dispatch: DispatchType, getState) => {
            dispatch(PartsActions.deleteAllFilter());
            await PartsHelpers.getPartHelper(dispatch, {
                pagination: getState().parts.pagination,
                sort: getState().parts.sort,
                filters: getState().parts.filters,
            })
        }
    },
    changePartPerPage: (page_size: number) => {
        return async (dispatch: DispatchType, getState) => {
            dispatch(PartsActions.changePartPerPage(page_size));
            await PartsHelpers.getPartHelper(dispatch, {
                pagination: getState().parts.pagination,
                sort: getState().parts.sort,
                filters: getState().parts.filters,
            })
        };
    },
    changePage: (page: number) => {
        return async (dispatch: DispatchType, getState) => {
            dispatch(PartsActions.changePage(page));
            PartsHelpers.getPartHelper(dispatch, {
                pagination: getState().parts.pagination,
                sort: getState().parts.sort,
                filters: getState().parts.filters,
            })
        };
    },


    getParts: (): ThunkActionType => async (dispatch: DispatchType, getState) => {
        partService.getParts({
            pagination: getState().parts.pagination,
            sort: getState().parts.sort,
            filters: getState().parts.filters,
        })
            .then(result => {
                dispatch(PartsActions.getAllPartsSuccess(result.parts));
                dispatch(PartsActions.changePartCount(result.part_count))
            })
            .catch(error => {
                dispatch(PartsActions.getAllPartsFailure(error));
            })
    },

    addPart(part: IPart): ThunkActionType {
        return async (dispatch: DispatchType) => {
            partService.addPart(part)
                .then(part => {
                    message.success('Успешно добавленно!');
                    dispatch(PartsActions.addPartsSuccess(part));
                })
                .catch(error => {
                    message.error('При добавлении произошла ошибка!');
                    dispatch(PartsActions.addPartFailure(error));
                })
        };
    },

    deletePart(part_id: string): ThunkActionType {
        return async (dispatch: DispatchType) => {
            partService.deletePart(part_id)
                .then(() => {
                    message.success('Успешно удалено!');
                    dispatch(PartsActions.deletePartSuccess(part_id));
                })
                .catch(error => {
                    message.error('При удалении произошла ошибка!');
                    dispatch(PartsActions.deletePartFailure(error));
                })
        };
    },

    updatePart(oldProject: IPart, newProject: IPart): ThunkActionType {
        return async (dispatch: DispatchType) => {
            partService.updatePart(oldProject, newProject)
                .then(projects => {
                    message.success('Успешно обновленно!');
                    dispatch(PartsActions.updatePartSuccess(projects));
                })
                .catch(error => {
                    message.error('При обновлении произошла ошибка!');
                    dispatch(PartsActions.updatePartFailure(error));
                })
        };

    },

    getPartTypes: (): ThunkActionType => async (dispatch: DispatchType) => {
        partService.getPartTypes()
            .then(part_types => {
                dispatch(PartsActions.getPartTypesSuccess(part_types));
            })
            .catch(error => {
                dispatch(PartsActions.getPartTypesFailure(error));
            })
    }
};