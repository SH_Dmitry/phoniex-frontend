import {partConstants} from "./parts.constants";
import {PartActionTypes} from "./parts.operations";
import {IFilter, IPagination, IPart, IPartType, ISort} from "../part.types";
import {emptyNObject, INObjects} from "../../../store/base.types";

const _ = require('lodash');

const initialState = {
    parts: emptyNObject as INObjects<IPart>,
    types: emptyNObject as INObjects<IPartType>,
    part_count: 10 as number,
    //pagination options
    pagination: {
        page: 1 as number,
        page_size: 10 as number
    } as IPagination,
    //sort options
    sort: {
        column: "name",
        direct: "ASC"
    } as ISort,
    //filter options
    filters: [
        {
            column: "type_id",
            value: 1,
            exactly: true
        }
    ] as Array<IFilter>
};
export type PartState = typeof initialState

export function partsReducer(state = initialState, action: PartActionTypes): PartState {
    switch (action.type) {
        case partConstants.GET_PARTS_SUCCESS:
            return {
                ...state,
                parts: {
                    byId: action.payload.entities.parts,
                    allIds: action.payload.result
                }
            };
        case partConstants.ADD_PART_SUCCESS: {
            const parts = action.payload.entities.parts;
            return {
                ...state,
                parts: {
                    byId: _.assign(state.parts.byId, parts),
                    allIds: _.union(state.parts.allIds, Object.getOwnPropertyNames(parts))
                }
            }
        }
        case partConstants.DELETE_PART_SUCCESS: {
            return {
                ...state,
                parts:
                    {
                        byId: _.omit(state.parts.byId, [action.payload]),
                        allIds: _.filter(state.parts.allIds, {part_id: action.payload})
                    }
            }
        }
        case partConstants.UPDATE_PART_SUCCESS: {
            const part_id = action.payload.result[0];
            const part = action.payload.entities.parts[part_id];
            return {
                ...state,
                parts:
                    {
                        byId: {
                            ...state.parts.byId,
                            [part_id]: part
                        },
                        allIds: _.union(state.parts.allIds, action.payload.result)
                    }
            }
        }

        case partConstants.GET_PART_TYPES_SUCCESS: {
            return {
                ...state,
                types: {
                    byId: action.payload.entities.part_types,
                    allIds: action.payload.result
                }
            }
        }

        //Filter reducers
        case partConstants.ADD_FILTER: {
            return {
                ...state,
                filters: state.filters.concat(action.payload),
                pagination: {
                    ...state.pagination,
                    page: 1
                }
            }
        }

        case partConstants.UPDATE_FILTER: {
            return {
                ...state,
                filters: state.filters.map(filter => {
                    if (filter.column == action.payload.column) {
                        return action.payload
                    }
                    return filter
                }),
                pagination: {
                    ...state.pagination,
                    page: 1
                }
            }
        }

        case partConstants.DELETE_FILTER: {
            return {
                ...state,
                filters: _.without(state.filters, action.payload)
            }
        }

        case partConstants.DELETE_ALL_FILTERS: {
            return {
                ...state,
                filters: [] as Array<IFilter>
            }
        }

        //Pagination reducers
        case partConstants.UPDATE_PAGE_SIZE: {
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    page_size: action.payload
                }
            }
        }
        case partConstants.UPDATE_CURRENT_PAGE: {
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    page: action.payload
                }
            }
        }


        case partConstants.UPDATE_PART_COUNT: {
            return {
                ...state,
                part_count: action.payload
            }
        }
        default:
            return state
    }
}