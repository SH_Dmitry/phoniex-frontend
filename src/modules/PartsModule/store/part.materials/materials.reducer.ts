import {PartMaterialsActionTypes} from "./materials.operations";
import {IPartMaterial} from "../../../MaterialsModule/materials.types";
import {partMaterialConstants} from "./materials.constants";
import {emptyNObject, INObjects} from "../../../../store/base.types";


const _ = require('lodash');

const initialState = {
    materials: emptyNObject as INObjects<IPartMaterial>
};

export type PartMaterialsStateType = typeof initialState

export function materialsReducer(state = initialState, action: PartMaterialsActionTypes): PartMaterialsStateType {
    switch (action.type) {
        case partMaterialConstants.GET_PART_MATERIALS_SUCCESS: {
            return {
                ...state,
                materials: {
                    byId: action.payload.entities.materials,
                    allIds: action.payload.result
                }
            }
        }
        case partMaterialConstants.ADD_MATERIAL_TO_PART_SUCCESS: {
            const materials = action.payload.entities.materials;
            return {
                ...state,
                materials: {
                    byId: _.merge(state.materials.byId, materials),
                    allIds: _.union(state.materials.allIds, action.payload.result)
                }
            }
        }
        case partMaterialConstants.UPDATE_PART_MATERIAL_SUCCESS: {
            const material_id = action.payload.result[0];
            const material = action.payload.entities.materials[material_id];
            return {
                ...state,
                materials:
                    {
                        byId: {
                            ...state.materials.byId,
                            [material_id]: material
                        },
                        allIds: _.union(state.materials.allIds, action.payload.result)
                    }
            }
        }
        case partMaterialConstants.DELETE_PART_MATERIAL_SUCCESS: {
            return {
                ...state,
                materials:
                    {
                        byId: _.omit(state.materials.byId, [action.payload]),
                        allIds: _.filter(state.materials.allIds, {part_id: action.payload})
                    }
            }
        }
        default: return state
    }
}