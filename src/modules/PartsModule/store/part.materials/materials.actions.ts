import {INPartMaterial} from "../../../MaterialsModule/materials.types";
import {partMaterialConstants} from "./materials.constants";

export const PartMaterialsActions = {
    //Material actions success
    getPartsMaterialsSuccess: (payload: INPartMaterial) => ({
        type: partMaterialConstants.GET_PART_MATERIALS_SUCCESS,
        payload
    } as const),
    addPartMaterialSuccess: (payload: INPartMaterial) => ({
        type: partMaterialConstants.ADD_MATERIAL_TO_PART_SUCCESS,
        payload
    } as const),
    updatePartMaterialSuccess: (payload: INPartMaterial) => ({
        type: partMaterialConstants.UPDATE_PART_MATERIAL_SUCCESS,
        payload
    } as const),
    deletePartMaterialSuccess: (payload: string) => ({
        type: partMaterialConstants.DELETE_PART_MATERIAL_SUCCESS,
        payload
    } as const),

    //Material actions failure
    getPartMaterialsFailure: (error: Error) => ({
        type: partMaterialConstants.GET_PART_MATERIALS_FAILURE,
        payload: error,
        error: true
    } as const),
    addPartMaterialFailure: (error: Error) => ({
        type: partMaterialConstants.ADD_MATERIAL_TO_PART_FAILURE,
        payload: error,
        error: true
    } as const),
    updatePartMaterialFailure: (error: Error) => ({
        type: partMaterialConstants.UPDATE_PART_MATERIAL_FAILURE,
        payload: error,
        error: true
    } as const),
    deletePartMaterialFailure: (error: Error) => ({
        type: partMaterialConstants.DELETE_PART_MATERIAL_FAILURE,
        payload: error,
        error: true
    } as const),
};