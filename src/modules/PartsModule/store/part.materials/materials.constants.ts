export const partMaterialConstants = {
    GET_PART_MATERIALS: 'part/get/material/request' as const,
    GET_PART_MATERIALS_SUCCESS: 'part/get/material/success' as const,
    GET_PART_MATERIALS_FAILURE: 'part/get/material/failure' as const,

    ADD_MATERIAL_TO_PART: 'part/add/material/request',
    ADD_MATERIAL_TO_PART_SUCCESS: 'part/add/material/success' as const,
    ADD_MATERIAL_TO_PART_FAILURE: 'part/add/material/failure' as const,

    UPDATE_PART_MATERIAL: 'part/update/material/request',
    UPDATE_PART_MATERIAL_SUCCESS: 'part/update/material/success' as const,
    UPDATE_PART_MATERIAL_FAILURE: 'part/update/material/failure' as const,

    DELETE_PART_MATERIAL: 'part/delete/material/request',
    DELETE_PART_MATERIAL_SUCCESS: 'part/delete/material/success' as const,
    DELETE_PART_MATERIAL_FAILURE: 'part/delete/material/failure' as const,
};