import {GetActionsType, ThunkActionType} from "../../../../store/base.types";
import {Dispatch} from "redux";
import {PartMaterialsActions} from "./materials.actions";
import {partMaterialService} from "../../../MaterialsModule/services/materials.service";
import {IPartMaterial} from "../../../MaterialsModule/materials.types";

export type PartMaterialsActionTypes = GetActionsType<typeof PartMaterialsActions>
type DispatchType = Dispatch<PartMaterialsActionTypes>

export const PartMaterialsOperations = {
    getPartMaterials: (part_id: string): ThunkActionType => async (dispatch: DispatchType) => {
        partMaterialService.getMaterials(part_id)
            .then(result => {
                dispatch(PartMaterialsActions.getPartsMaterialsSuccess(result));
            })
            .catch(error => {
                dispatch(PartMaterialsActions.getPartMaterialsFailure(error));
            })
    },
    addPartMaterial: (part_id:string, material_id:string): ThunkActionType => async (dispatch: DispatchType) => {
        partMaterialService.addMaterial(part_id, material_id)
            .then(result => {
                dispatch(PartMaterialsActions.addPartMaterialSuccess(result));
            })
            .catch(error => {
                dispatch(PartMaterialsActions.addPartMaterialFailure(error));
            })
    },
    updatePartMaterial: (part_id:string, material:IPartMaterial): ThunkActionType => async (dispatch: DispatchType) => {
        partMaterialService.updateMaterial(part_id, material)
            .then(result => {
                dispatch(PartMaterialsActions.updatePartMaterialSuccess(result));
            })
            .catch(error => {
                dispatch(PartMaterialsActions.updatePartMaterialFailure(error));
            })
    },
    deletePartMaterial: (part_id:string, material_id:string): ThunkActionType => async (dispatch: DispatchType) => {
        partMaterialService.deleteMaterial(part_id, material_id)
            .then(() => {
                dispatch(PartMaterialsActions.deletePartMaterialSuccess(material_id));
            })
            .catch(error => {
                dispatch(PartMaterialsActions.deletePartMaterialFailure(error));
            })
    }
};