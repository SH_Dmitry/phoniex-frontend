import {combineReducers} from "redux";
import {commentReducer} from "./part.comments/comments.reducer";
import {historyReducer} from "./part.history/history.reducer";
import {infoReducer} from "./part.info/info.reducer";
import {structureReducer} from "./part.structure/structure.reducer";
import {techprocessReducer} from "./part.techprocess/techprocess.reducer";
import {materialsReducer} from "./part.materials/materials.reducer";

export const partInfoReducer = combineReducers({
    comments: commentReducer,
    history: historyReducer,
    info: infoReducer,
    structure: structureReducer,
    techprocess: techprocessReducer,
    materials: materialsReducer,
});