import {partConstants} from "./parts.constants";
import {IFilter, INParts, INPartTypes, ISort} from "../part.types";

export const PartsActions = {

    //Action Success
    getAllPartsSuccess: (payload: INParts) => ({
        type: partConstants.GET_PARTS_SUCCESS,
        payload
    } as const),
    addPartsSuccess: (payload: INParts) => ({
        type: partConstants.ADD_PART_SUCCESS,
        payload
    } as const),
    updatePartSuccess: (payload: INParts) => ({
        type: partConstants.UPDATE_PART_SUCCESS,
        payload
    } as const),
    deletePartSuccess: (payload: string) => ({
        type: partConstants.DELETE_PART_SUCCESS,
        payload
    } as const),
    getPartTypesSuccess: (payload: INPartTypes) => ({
        type: partConstants.GET_PART_TYPES_SUCCESS,
        payload
    } as const),

    //Action Failure
    getAllPartsFailure: (error: Error) => ({
        type: partConstants.GET_PARTS_FAILURE,
        payload: error,
        error: true
    } as const),

    addPartFailure: (error: Error) => ({type: partConstants.ADD_PART_FAILURE, payload: error, error: true} as const),
    updatePartFailure: (error: Error) => ({
        type: partConstants.UPDATE_PART_FAILURE,
        payload: error,
        error: true
    } as const),
    deletePartFailure: (error: Error) => ({
        type: partConstants.DELETE_PART_FAILURE,
        payload: error,
        error: true
    } as const),
    getPartTypesFailure: (error: Error) => ({
        type: partConstants.GET_PART_TYPES_FAILURE,
        payload: error,
        error: true
    } as const),

    //Filter actions
    addFilter: (filter: IFilter) => ({type: partConstants.ADD_FILTER, payload: filter} as const),
    updateFilter: (filter: IFilter) => ({type: partConstants.UPDATE_FILTER, payload: filter} as const),
    deleteFilter: (filter: IFilter) => ({type: partConstants.DELETE_FILTER, payload: filter} as const),
    deleteAllFilter: () => ({type: partConstants.DELETE_ALL_FILTERS} as const),

    //Sort action
    updateSort: (sort: ISort) => ({type: partConstants.UPDATE_SORT, payload: sort} as const),

    //Pagination actions
    changePartPerPage: (page_size: number) => ({type: partConstants.UPDATE_PAGE_SIZE, payload: page_size} as const),
    changePage: (page: number) => ({type: partConstants.UPDATE_CURRENT_PAGE, payload: page} as const),
    changePartCount: (parts_count: number) => ({type: partConstants.UPDATE_PART_COUNT, payload: parts_count} as const),

};



