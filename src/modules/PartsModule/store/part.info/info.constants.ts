export const partInfoConstants = {
    GET_PART: 'part.info/part/get/request' as const,
    GET_PART_SUCCESS: 'part.info/part/get/success' as const,
    GET_PART_FAILURE: 'part.info/part/get/failure' as const,

    UPDATE_PART_INFO_STATUS: 'part.info/part/status/change/request' as const,
    UPDATE_PART_INFO_STATUS_SUCCESS: 'part.info/part/status/change/success' as const,
    UPDATE_PART_INFO_STATUS_FAILURE: 'part.info/part/status/change/failure' as const,
};