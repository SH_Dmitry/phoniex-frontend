import {GetActionsType, ThunkActionType} from "../../../../store/base.types";
import {Dispatch} from "redux";
import {PartInfoActions} from "./info.actions";
import {partInfoService} from "../../services/info.service";
import {IPart} from "../../part.types";
import {partService} from "../../services/parts.service";
import {message} from "antd";

export type PartInfoActionTypes = GetActionsType<typeof PartInfoActions>
type DispatchType = Dispatch<PartInfoActionTypes>

export const PartInfoOperations = {
    getPartById: (part_id): ThunkActionType => async (dispatch: DispatchType) => {
        partInfoService.getPartById(part_id)
            .then(part => {
                console.log(part);
                dispatch(PartInfoActions.getPartByIdSuccess(part));
            })
            .catch(error => {
                dispatch(PartInfoActions.getPartByIdFailure(error));
            })
    },
    updatePart(oldPart: IPart, newPart: IPart): ThunkActionType {
        return async (dispatch: DispatchType) => {
            partService.updatePart(oldPart, newPart)
                .then(part => {
                    message.success('Успешно обновленно!');
                    dispatch(PartInfoActions.updatePartStatusSuccess(part));
                })
                .catch(error => {
                    message.error('При обновлении произошла ошибка!');
                    dispatch(PartInfoActions.updatePartStatusFailure(error));
                })
        };
    }
};