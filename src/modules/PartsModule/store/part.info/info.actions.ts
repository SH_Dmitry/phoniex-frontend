import {partInfoConstants} from "./info.constants";
import {INParts} from "../../part.types";

export const PartInfoActions = {
    //Action Success
    getPartByIdSuccess: (payload: INParts) => ({
        type: partInfoConstants.GET_PART_SUCCESS,
        payload
    } as const),
    updatePartStatusSuccess: (payload: INParts) => ({
        type: partInfoConstants.UPDATE_PART_INFO_STATUS_SUCCESS,
        payload
    } as const),

    //Action Failure
    getPartByIdFailure: (error: Error) => ({
        type: partInfoConstants.GET_PART_FAILURE,
        payload: error,
        error: true
    } as const),
    updatePartStatusFailure: (error: Error) => ({
        type: partInfoConstants.UPDATE_PART_INFO_STATUS_FAILURE,
        payload: error,
        error: true
    } as const)
};