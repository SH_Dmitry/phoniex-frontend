import {PartInfoActionTypes} from "./info.operations";
import {partInfoConstants} from "./info.constants";
import {IPart} from "../../part.types";

const initialState = {
    part: null as IPart,
};

export type PartInfoState = typeof initialState

export function infoReducer(state = initialState, action: PartInfoActionTypes): PartInfoState {
    switch (action.type) {
        case partInfoConstants.GET_PART_SUCCESS:
            return {
                ...state,
                part: action.payload.entities.parts[action.payload.result.toString()]
            };
        case partInfoConstants.UPDATE_PART_INFO_STATUS_SUCCESS:
            return {
                ...state,
                part: action.payload.entities.parts[action.payload.result.toString()]
            };
        default: return state
    }
}