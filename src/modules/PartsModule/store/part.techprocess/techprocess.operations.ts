import {GetActionsType} from "../../../../store/base.types";
import {PartTechprocessActions} from "./techprocess.actions";

export type PartTechprocessActionTypes = GetActionsType<typeof PartTechprocessActions>
// type DispatchType = Dispatch<PartTechprocessActionTypes>
//
// export const PartTechprocessThunk = {
//
// };