import {PartTechprocessActionTypes} from "./techprocess.operations";

const initialState = {

};

export type PartTechprocessStateType = typeof initialState

export function techprocessReducer(state = initialState, action: PartTechprocessActionTypes): PartTechprocessStateType {
    switch (action.type) {
        default: return state
    }
}