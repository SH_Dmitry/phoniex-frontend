import React from "react";
import {Card} from "antd";
import {useSelector} from "react-redux";
import {AppState} from "../../../../store/app.reducer";
import {IPart} from "../../part.types";
import {DrawerCallerHOC} from "../../components/drawers/DrawerCallerHOC";
import {HistoryDrawer} from "../../components/drawers/HistoryDrawer";

export const PartInfoBlock = () => {
    const part: IPart = useSelector((state: AppState) => state.part_info.info.part);
    return (
        <>
            <Card
                style={{height:"100%"}}
                extra={<DrawerCallerHOC
                    Drawer={HistoryDrawer}
                    part_id={part.part_id}
                    name={"История изменений"}/>
                }
                title={"Информация"}>
            </Card>
        </>
    )
};