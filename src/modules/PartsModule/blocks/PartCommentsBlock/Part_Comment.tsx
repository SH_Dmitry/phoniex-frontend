import {Avatar, Button, Comment, Form} from "antd";
import icon from "../../../../assets/icons/user.png";
import React, {useState} from "react";
import TextArea from "antd/es/input/TextArea";
import {GetShortPersonalData} from "../../../../tools/converters";
import {useSelector} from "react-redux";
import {AppState} from "../../../../store/app.reducer";


type Part_CommentProps = {
    comment_id: string,
    onAddReply: (comment_id: string, text: string) => void
}

const Editor = ({onFinish, visible}) => (
    <div hidden={!visible}>
        <Form onFinish={onFinish}>
            <Form.Item name="text">
                <TextArea rows={4}/>
            </Form.Item>
            <Form.Item>
                <Button htmlType="submit" type="primary">
                    Отправить
                </Button>
            </Form.Item>
        </Form>
    </div>
);


export const Part_Comment = ({comment_id, onAddReply}: Part_CommentProps) => {
    const comment = useSelector((state: AppState) => state.part_info.comments.comments.byId[comment_id]);
    const [replayed, setReplayed] = useState(false);
    return (
        <Comment
            author={<a>{GetShortPersonalData(comment.personal_data)}</a>}
            datetime={new Date(Date.parse(comment.date_time)).toUTCString()}
            avatar={
                <Avatar
                    src={icon}
                    alt={comment.personal_data}/>
            }
            content={comment.text}
            actions={
                [
                    replayed ? <span key="comment-cancel" onClick={() => setReplayed(false)}>Отмена</span> :
                        <span key="comment-reply" onClick={() => setReplayed(true)}>Ответить</span>
                ]
            }
        >
            <Editor visible={replayed} onFinish={(values) => {
                onAddReply(comment.comment_id, values.text);
                setReplayed(false);
            }}/>
            {comment.replies ? comment.replies.map(reply => (
                <Part_Comment key={reply} comment_id={reply} onAddReply={onAddReply}/>
            )) : null}

        </Comment>
    )
};


