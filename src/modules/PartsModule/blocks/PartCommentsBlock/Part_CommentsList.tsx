'use strict';

import React, {useEffect} from "react";
import {Button, Divider, Empty, Form, List} from "antd";
import {Part_Comment} from "./Part_Comment";
import TextArea from "antd/es/input/TextArea";
import {useDispatch, useSelector} from "react-redux";
import {PartCommentsOperations} from "../../store/part.comments/comments.operations";
import {AppState} from "../../../../store/app.reducer";

interface Part_CommentsBlockProps {
    part_id: string
}


export const Part_CommentsList = ({part_id}: Part_CommentsBlockProps) => {
    const root_comments = useSelector((state: AppState) => state.part_info.comments.root_comments);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(PartCommentsOperations.getPartComments(part_id));
    }, [part_id]);


    const AddComment = (value) => {
        dispatch(PartCommentsOperations.addCommentToPart(part_id, value.text))
    };

    const AddReply = (comment_id: string, text: string) => {
        dispatch(PartCommentsOperations.addReplyToComment(part_id, comment_id, text))
    };

    return (
        <>
            {
                (root_comments.length != 0) ?
                    <List>
                        {root_comments.map(value => (
                            <Part_Comment key={value} comment_id={value} onAddReply={AddReply}/>
                        ))}
                    </List> :
                    <>
                    <Empty description={false}/>
                    </>
            }
            <Divider plain>Новый комментарий</Divider>
            <Form onFinish={AddComment}>
                <Form.Item name="text">
                    <TextArea rows={4}/>
                </Form.Item>
                <Form.Item>
                    <Button htmlType="submit" type="primary">
                        Отправить
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

