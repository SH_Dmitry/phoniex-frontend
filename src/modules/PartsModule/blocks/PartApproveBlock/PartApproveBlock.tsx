import React from "react";
import {Card, Descriptions, Switch} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../../store/app.reducer";
import {IPart} from "../../part.types";

export const PartApproveBlock = () => {
    const dispatch = useDispatch();
    const part: IPart = useSelector((state: AppState) => state.part_info.info.part);
    return (
        <>
            <Card
                style={{height:"100%"}}
                title={"Утверждение"}>
                <Descriptions column={1} bordered>
                    <Descriptions.Item label="Структура">
                        <Switch title={"Структура"}
                                checkedChildren={"Утверждено"}
                                unCheckedChildren={"Не утверждено"}
                                checked={part.has_structure == 1}
                                onChange={checked => {
                                    // dispatch(PartInfoOperations.updatePart(part, {
                                    //     ...part,
                                    //     has_structure: checked == true ? 1 : 0
                                    // }))
                                }}/>
                    </Descriptions.Item>
                    <Descriptions.Item label="Техпроцесс">
                        <Switch
                            title={"Техпроцесс"}
                            checkedChildren={"Утверждено"}
                            unCheckedChildren={"Не утверждено"}
                            checked={part.has_techprocess == 1}
                            onChange={checked => {
                                // dispatch(PartInfoOperations.updatePart(part, {
                                //     ...part,
                                //     has_techprocess: checked == true ? 1 : 0
                                // }))
                            }}
                        />
                    </Descriptions.Item>
                    <Descriptions.Item label="Материалы">
                        <Switch
                            title={"Материалы"}
                            checkedChildren={"Утверждено"}
                            unCheckedChildren={"Не утверждено"}
                            checked={part.has_materials == 1}
                            onChange={checked => {
                                // dispatch(PartInfoOperations.updatePart(part, {
                                //     ...part,
                                //     has_materials: checked == true ? 1 : 0
                                // }))
                            }}
                        />
                    </Descriptions.Item>
                </Descriptions>
            </Card>
        </>
    )
};