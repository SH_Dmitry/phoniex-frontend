import {normalize} from "normalizr";
import {historyScheme} from "../store/part.scheme";
import {INHistory, IRHistory} from "../part.types";
import {apiWrapper} from "../../../tools/api/api.wrapper";
import {bearerSecurity} from "../../SecurityGateModule/services/bearer.security";


export const partHistoryService = {
    async getPartHistory(part_id: string): Promise<INHistory> {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Authorization': bearerSecurity()
            },
        };
        const response = await apiWrapper<Array<IRHistory>>(`/part/${part_id}/history`, requestOptions);
        return normalize(response, historyScheme)
    }
};