import {normalize} from "normalizr";
import {partScheme, partsScheme} from "../store/part.scheme";
import {IHierarchyParts, INParts, IPartStructureSimpleItem, IRPartWithStructure} from "../part.types";
import {bearerSecurity} from "../../SecurityGateModule/services/bearer.security";
import {apiWrapper} from "../../../tools/api/api.wrapper";


export const partStructureService = {
    async getPartStructure(part_id: string): Promise<INParts> {
        const requestOptions = {
            method: 'GET',
            headers: {'Authorization': bearerSecurity()},
        };
        const response = await apiWrapper<IRPartWithStructure>(`/part/${part_id}/structure`, requestOptions);
        return normalize(response, partsScheme);
    },
    async getPartHierarchy(part_id: string): Promise<Array<IHierarchyParts>> {
        const requestOptions = {
            method: 'GET',
            headers: {'Authorization': bearerSecurity()},
        };
        return await apiWrapper<Array<IHierarchyParts>>(`/part/${part_id}/hierarchy`, requestOptions)
    },

    async addPartToStructure(parent_id: string, part_id: string): Promise<INParts> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify({part_id:part_id})
        };
        const response = await apiWrapper<IRPartWithStructure>(`/part/${parent_id}/structure`, requestOptions);
        return normalize(response, partScheme);
    },

    async replacePartStructure(parent_decimal: string, items: Array<IPartStructureSimpleItem>): Promise<void> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify({parent_decimal, items})
        };
        await apiWrapper<IRPartWithStructure>(`/part/null/structure/replace`, requestOptions);
    },

    async updatePartCountStructure(parent_id: string, part_id: string, count: number): Promise<INParts> {
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify({part_id, count})
        };
        const response = await apiWrapper<IRPartWithStructure>(`/part/${parent_id}/structure`, requestOptions);
        return normalize(response, partScheme);
    },

    async deletePartFromStructure(parent_id: string, part_id: string): Promise<void> {
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify({part_id})
        };
        return await apiWrapper<void>(`/part/${parent_id}/structure`, requestOptions)
    },
};