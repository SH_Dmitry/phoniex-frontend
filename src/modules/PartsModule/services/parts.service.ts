import {normalize} from "normalizr";
import {partScheme, partsScheme, partTypeScheme} from "../store/part.scheme";
import {INParts, INPartTypes, INRParts, IOption, IPart, IPartSimple, IPartType, IRParts} from "../part.types";
import {bearerSecurity} from "../../SecurityGateModule/services/bearer.security";
import {apiWrapper} from "../../../tools/api/api.wrapper";

export const partService = {
    async getParts(options: IOption): Promise<INRParts> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify(options)
        };
        const response = await apiWrapper<IRParts>(`/part/list`, requestOptions);
        return {
            part_count: response.part_count,
            parts: normalize(response.parts, partsScheme)
        }

    },
    async addPart(part: IPart): Promise<INParts> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify(part)
        };
        const response = await apiWrapper<IPart>(`/part`, requestOptions);
        return normalize(response, partScheme);
    },
    async deletePart(part_id: string): Promise<void> {
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Authorization': bearerSecurity()
            }
        };
        return await apiWrapper<void>(`/part/${part_id}`, requestOptions);
    },
    async updatePart(oldPart: IPartSimple, newPart: IPartSimple): Promise<INParts> {
        const updatedPart: IPartSimple = {
            ...oldPart,
            name: newPart.name,
            decimal_num: newPart.decimal_num,
            type_id: newPart.type_id,
        };
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify(updatedPart)
        };
        await apiWrapper<IPartSimple>(`/part/` + oldPart.part_id, requestOptions);
        return normalize({updatedPart}, partsScheme)
    },
    async getPartTypes(): Promise<INPartTypes> {
        const requestOptions: RequestInit = {
            method: 'GET',
            headers: {'Authorization': bearerSecurity()},
        };
        const response = await apiWrapper<Array<IPartType>>(`/type/part`, requestOptions);
        return normalize(response, partTypeScheme);
    }
};