import {normalize} from "normalizr";
import {partScheme} from "../store/part.scheme";
import {INParts, IPart} from "../part.types";
import {bearerSecurity} from "../../SecurityGateModule/services/bearer.security";
import {apiWrapper} from "../../../tools/api/api.wrapper";

export const partInfoService = {
    async getPartById(part_id: string): Promise<INParts> {
        const requestOptions = {
            method: 'GET',
            headers: {'Authorization': bearerSecurity()},
        };
        const response = await apiWrapper<IPart>(`/part/${part_id}`, requestOptions);
        return normalize(response, partScheme);
    },
};