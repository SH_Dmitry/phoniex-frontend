import {normalize} from "normalizr";
import {commentsScheme} from "../store/part.scheme";
import {INComments, IRComment} from "../part.types";
import {bearerSecurity} from "../../SecurityGateModule/services/bearer.security";
import {apiWrapper} from "../../../tools/api/api.wrapper";

export const partCommentService = {
    async getPartComments(part_id: string): Promise<INComments> {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Authorization': bearerSecurity()
            },
        };
        const response = await apiWrapper<Array<IRComment>>(`/part/${part_id}/comment`, requestOptions);
        return normalize(response, commentsScheme)
    },

    async addPartComment(part_id:string, text:string): Promise<INComments> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity(),
            },
            body: JSON.stringify({
                text
            })
        };
        const response = await apiWrapper<Array<IRComment>>(`/part/${part_id}/comment`, requestOptions);
        return normalize(response, commentsScheme)
    },

    async addCommentReply(part_id: string, comment_id:string, text:string): Promise<INComments> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity(),
            },
            body: JSON.stringify({
                text
            })
        };
        const response = await apiWrapper<Array<IRComment>>(`/part/${part_id}/comment/${comment_id}/reply`, requestOptions);
        return normalize(response, commentsScheme)
    }
};