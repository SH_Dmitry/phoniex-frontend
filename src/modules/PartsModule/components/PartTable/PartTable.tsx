import React from "react";
import {Button, Dropdown, Menu, Table, Tag} from "antd";

import {FormInstance} from "antd/lib/form";
import {DeleteOutlined, EditOutlined, EllipsisOutlined, InfoCircleOutlined} from "@ant-design/icons/lib";
import {IPart} from "../../part.types";
import {INObjects} from "../../../../store/base.types";
import {ModuleFormDialog} from "../../../../components/dialogs/ModuleFormDialog";
import {PartFormFields} from "../../../../components/forms/FormFields";

const _ = require('lodash');

type PartTableProps = {
    data: INObjects<IPart>
    onDelete: (Part: IPart) => void
    onUpdate: (oldPart: IPart, newPart: IPart) => void
    onInfo: (Part: IPart) => void
}


export function PartTable({data, onDelete, onUpdate, onInfo}: PartTableProps) {
    const [dialogVisibility, setDialogVisibility] = React.useState(false);
    const [editItem, setEditItem] = React.useState(null);
    const formRef = React.createRef<FormInstance>();

    const columns = [
        {
            title: 'Наименование',
            dataIndex: 'name',
            key: 'name',
            width: '20%'
        },
        {
            title: 'Децимальный номер',
            dataIndex: 'decimal_num',
            key: 'decimal_num',
            width: '20%'
        },
        {
            key: "tags",
            dataIndex: "tags",
            render: (text, record: IPart) => (
                <>
                    {record.has_materials == 1 ? <Tag color={"green"}>Материалы</Tag> : null}
                    {record.has_structure == 1 ? <Tag color={"blue"}>Структура</Tag> : null}
                    {record.has_techprocess == 1 ? <Tag color={"volcano"}>Техпроцесс</Tag> : null}
                </>
            )
        },
        {
            dataIndex: 'action',
            key: 'action',
            width: '20%',
            render: (text, record: IPart) => (
                <span>
                    <Button style={{marginRight: 30}}
                            icon={<InfoCircleOutlined/>}
                            onClick={() => {
                                onInfo(record)
                            }}>
                        Открыть карточку</Button>
                    <Dropdown overlay={
                        <Menu>
                            <Menu.Item
                                icon={<EditOutlined/>}
                                onClick={() => {
                                    setEditItem(record);
                                    setDialogVisibility(true);
                                    formRef.current.setFieldsValue(record);
                                }
                                }>Изменить
                            </Menu.Item>
                            <Menu.Item
                                icon={<DeleteOutlined/>}
                                onClick={() => onDelete(record)
                                }>Удалить
                            </Menu.Item>
                        </Menu>
                    }>
                        <Button icon={<EllipsisOutlined />}/>
                    </Dropdown>

                </span>
            )
        }
    ];

    return (
        <>
            <Table
                rowKey={(record) => {
                    return "row" + record.part_id
                }}
                pagination={false}
                columns={columns}
                dataSource={_.toArray(data.byId)}/>
            {/*Modal dialog*/}
            <ModuleFormDialog
                name={"edit_part"}
                title={"Редактировать изделие"}
                submitText={"Сохранить"}
                visible={dialogVisibility}
                fields={PartFormFields}
                formRef={formRef}
                handleClose={() => setDialogVisibility(false)}
                handleSubmit={(value) => {
                    onUpdate(editItem, value);
                    setDialogVisibility(false)
                }}/>

        </>
    )

}