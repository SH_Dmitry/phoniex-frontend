import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Select} from "antd";
import styles from "./PartTypeSelector.module.less"
import {AppState} from "../../../../store/app.reducer";
import {PartOperations} from "../../store/parts.operations";

const _ = require('lodash');
export const PartTypeSelector = () => {
    const types = useSelector((state: AppState) => state.parts.types);
    const filters = useSelector((state: AppState) => state.parts.filters);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(PartOperations.getPartTypes())
    }, []);
    return (
        <>
            <Select
                className={styles.root}
                placeholder="Фильтр по типам"
                allowClear
                onChange={value => {
                    if(value) {
                        const filter = filters.find(value => value.column == "type_id");
                        if (filter) {
                            dispatch(PartOperations.updateFilter({column: "type_id", value, exactly: true}))
                        } else {
                            dispatch(PartOperations.addFilter({column: "type_id", value, exactly: true}))
                        }
                    }
                }}
                onClear={()=>dispatch(PartOperations.deleteFilter(filters.find(value=> value.column=="type_id")))}
                value={filters.find(value=> value.column=="type_id")?filters.find(value=> value.column=="type_id").value:null}>
                {_.values(types.byId).map(value => (
                        <Select.Option key={value.part_type_id} value={value.part_type_id} >
                            <div style={{display: "flex", alignItems: "center"}}>
                                {value.name}
                            </div>
                        </Select.Option>
                    )
                )}
            </Select>
        </>
    )
};