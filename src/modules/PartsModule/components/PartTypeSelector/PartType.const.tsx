import {ApiTwoTone, AppstoreTwoTone, BugOutlined, DatabaseTwoTone, SettingTwoTone} from "@ant-design/icons/lib";
import CSS from 'csstype';
import React from "react";


export const PartTypes = {
    1: {
        id: 1,
        name: "Изделие",
        hasStructure: true,
        icon: (style: any) => {
            return <DatabaseTwoTone style={style}/>;
        }
    },
    2: {
        id: 2,
        name: "Блок,модуль",
        hasStructure: true,
        icon: (style: any) => <AppstoreTwoTone style={style}/>
    },
    3: {
        id: 3,
        name: "Сборка",
        hasStructure: true,
        icon: (style: any) => <ApiTwoTone style={style}/>
    },
    4: {
        id: 4,
        name: "Деталь",
        hasStructure: false,
        icon: (style: any) => <SettingTwoTone style={style}/>
    }
};

export const getPartTypeIcon = (type_id: string, style: CSS.Properties) => {
    if (PartTypes.hasOwnProperty(type_id)) {
        return PartTypes[type_id].icon(style)
    } else {
        return <BugOutlined style={style}/>
    }
};

const getPartTypeName = (type_id: string) => {
    if (PartTypes.hasOwnProperty(type_id)) {
        return PartTypes[type_id].name
    } else {
        return "Не найдено"
    }
};

export const getPartTypeLabel = (type_id: string, style: CSS.Properties) => {
    return (
        <span>
            {getPartTypeIcon(type_id, style)}
           {getPartTypeName(type_id)}
    </span>
    )
};