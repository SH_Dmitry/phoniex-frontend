import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../../store/app.reducer";
import {IFilter} from "../../part.types";
import {Input, Typography} from "antd";

export interface SearchFilterProps {
    name: string,
    column: string
    onAdd: (filter: IFilter) => void
    onChange: (filter: IFilter) => void
    onDelete: (filter: IFilter) => void
}

export const SearchFilter = ({name, column, onAdd, onChange, onDelete}: SearchFilterProps) => {
    const filters = useSelector((state: AppState) => state.parts.filters);
    const dispatch = useDispatch();
    return (
        <>
            <Typography>{name}</Typography>
            <Input
                style={{marginBottom:16}}
                allowClear
                onChange={value => {
                    const filter_value = value.currentTarget.value;
                    if(filter_value) {
                        const filter = filters.find(value => value.column == column);
                        if (filter) {
                            dispatch(onChange({column, value: filter_value, exactly: false}))
                        } else {
                            dispatch(onAdd({column, value: filter_value, exactly: false}))
                        }
                    }
                    else {
                        const filter = filters.find(value => value.column == column);
                        if(filter){
                            dispatch(onDelete(filter))
                        }
                    }
                }}
                value={filters.find(value=> value.column==column)?filters.find(value=> value.column==column).value:null}>
            </Input>
        </>
    )
};