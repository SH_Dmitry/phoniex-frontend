import React from "react";
import {PartOperations} from "../../store/parts.operations";
import {SearchFilter} from "./SearchFilter";


export const SearchFilterPartName = () => {
    return (
        <SearchFilter name={"Наименование"} column={"name"}
                      onAdd={PartOperations.addFilter}
                      onChange={PartOperations.updateFilter}
                      onDelete={PartOperations.deleteFilter}
        />
    )
};

export const SearchFilterPartDec = () => {
    return (
        <SearchFilter name={"Децимальный номер"} column={"decimal_num"}
                      onAdd={PartOperations.addFilter}
                      onChange={PartOperations.updateFilter}
                      onDelete={PartOperations.deleteFilter}
        />
    )
};


