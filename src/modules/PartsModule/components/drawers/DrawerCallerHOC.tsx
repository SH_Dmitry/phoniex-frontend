'use strict';

import React from "react";
import {Button} from "antd";

export interface DrawerProps {
        part_id: string
        visible: boolean
        onCancel: () => void
}

export interface DrawerCallerHOCProps {
        Drawer: React.ComponentType<DrawerProps>,
        part_id: string,
        name: string
}

export const DrawerCallerHOC = ({Drawer,part_id,name}:DrawerCallerHOCProps) => {
        const [visible, setVisibility] = React.useState(false);
        return (
            <>
                <Button onClick={() => setVisibility(true)}>{name}</Button>
                <Drawer part_id={part_id} visible={visible} onCancel={()=>setVisibility(false)}/>
            </>
        )
};