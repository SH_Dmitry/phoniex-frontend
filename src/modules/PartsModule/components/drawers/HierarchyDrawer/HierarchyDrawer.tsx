import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Drawer, Tree} from "antd";
import {PartStructureOperations} from "../../../store/part.structure/structure.operations";
import {DrawerProps} from "../DrawerCallerHOC";
import {AppState} from "../../../../../store/app.reducer";


export const HierarchyDrawer = ({part_id, visible, onCancel}: DrawerProps) => {
    const hierarchy = useSelector((state: AppState) => state.part_info.structure.hierarchy);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(PartStructureOperations.getHierarchy(part_id))
    }, [part_id]);
    return (
        <Drawer
            title="Древовидная структура сборочной еденицы"
            width={720}
            placement="right"
            visible={visible}
            onClose={onCancel}>
            <Tree

                treeData={hierarchy}
                titleRender={(nodeData) => {
                    console.log(nodeData);
                    console.log(typeof nodeData);
                    return (

                        <span>{nodeData.title}</span>
                    )}}
            >
            </Tree>
        </Drawer>
    )
};




