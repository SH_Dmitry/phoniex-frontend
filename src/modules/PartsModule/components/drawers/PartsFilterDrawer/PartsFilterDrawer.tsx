import React from "react";
import {Badge, Button, Divider, Drawer, Typography} from "antd";
import {PartTypeSelector} from "../../PartTypeSelector/PartTypeSelector";
import {SearchFilterPartDec, SearchFilterPartName} from "../../SearchFilter/SearchFilterVariation";
import {FilterOutlined} from "@ant-design/icons/lib";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../../../store/app.reducer";
import {PartOperations} from "../../../store/parts.operations";


export const PartsFilterDrawer = ({text = ""}) => {
    const [visibility, setVisibility] = React.useState(false);
    const dispatch = useDispatch();
    const filters = useSelector((state: AppState) => state.parts.filters);
    return (
        <>

            <Badge count={filters.length?filters.length:0}>
                <Button icon={<FilterOutlined/>} onClick={() => setVisibility(true)}>{text}</Button>
            </Badge>
            <Drawer
                title="Фильтр"
                width={480}
                placement="right"
                visible={visibility}
                footer={<Button onClick={()=>{
                    dispatch(PartOperations.deleteAllFilter());
                    setVisibility(false)
                }}>Очистить</Button>}
                onClose={()=>setVisibility(false)}>
                <Typography>Тип ДСЕ</Typography>
                <PartTypeSelector/>
                <Divider/>
                <SearchFilterPartName/>
                <SearchFilterPartDec/>
            </Drawer>
        </>
    )
};




