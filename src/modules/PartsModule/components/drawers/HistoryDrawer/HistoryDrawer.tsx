'use strict';

import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Drawer, Timeline, Typography} from "antd";
import {GetShortPersonalData} from "../../../../../tools/converters";
import {PartHistoryOperations} from "../../../store/part.history/history.operations";
import {
    ClockCircleOutlined,
    CloseCircleOutlined,
    PlusCircleOutlined,
    SettingOutlined,
    SketchOutlined,
    SyncOutlined,
    ThunderboltOutlined
} from "@ant-design/icons/lib";
import style from './HistoryDrawer.module.less'
import moment from "moment";
import {DrawerProps} from "../DrawerCallerHOC";
import {AppState} from "../../../../../store/app.reducer";

const {Text} = Typography;

export const HistoryDrawer = ({part_id, visible, onCancel}: DrawerProps)=> {
    const history = useSelector((state: AppState) => state.part_info.history.history);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(PartHistoryOperations.getPartHistory(part_id));
        moment.locale();
    }, [part_id]);

    return (
        <Drawer
            title="История изменений"
            width={440}
            placement="right"
            visible={visible}
            onClose={onCancel}>
                <Timeline mode='right' className={style.timeline}>
                    {history.allIds.map(history_id => {
                        const historyItem = history.byId[history_id];
                        return (
                            <Timeline.Item key={history_id}
                                           dot={getIconFromOperation(historyItem.operation)}
                                           color={getColorFromOperation(historyItem.operation)}>
                                <Text>{historyItem.operation}</Text>
                                <Button style={{paddingLeft: 16}}
                                        type="link">{GetShortPersonalData(historyItem.personal_data)}</Button>
                                <br/>
                                <Text>{moment(new Date(Date.parse(historyItem.date_time))).calendar()}</Text>
                            </Timeline.Item>
                        )
                    })}
                </Timeline>
        </Drawer>
    )
};

const getIconFromOperation = (operation: string) => {
    switch (operation) {
        case 'Добавлено':
            return (<PlusCircleOutlined/>);
        case 'Техпроцесс обновлен':
            return (<SettingOutlined/>);
        case 'Материалы обновлены':
            return (<SketchOutlined/>);
        case 'Материалы добавлены':
            return (<SketchOutlined/>);
        case 'Материалы удалены':
            return (<SketchOutlined/>);
        case 'Трудоемкость обновлена':
            return (<ClockCircleOutlined/>);
        case 'Удалено':
            return (<CloseCircleOutlined/>);
        case 'Востановлено':
            return (<ThunderboltOutlined/>);
        case 'Обновлено':
            return (<SyncOutlined/>);
    }
};
const getColorFromOperation = (operation: string) => {
    switch (operation) {
        case 'Добавлено':
            return ("green");
        case 'Техпроцесс обновлен':
            return ("blue");
        case 'Материалы обновлены':
            return ("blue");
        case 'Материалы добавлены':
            return ("green");
        case 'Материалы удалены':
            return ("red");
        case 'Трудоемкость обновлена':
            return ("blue");
        case 'Удалено':
            return ("red");
        case 'Востановлено':
            return ("green");
        case 'Обновлено':
            return ("blue");
    }
};