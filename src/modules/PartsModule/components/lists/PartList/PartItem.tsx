import React from "react";
import {List} from "antd";
import {IPart} from "../../../part.types";
import {CheckCircleTwoTone} from "@ant-design/icons/lib";

interface Part_Props {
    part: IPart
    onClick: (number) => void
    selectedPart: IPart
}

export const PartItem = ({part, onClick, selectedPart}: Part_Props) => {
    return (
        <>
            <List.Item
                key={part.part_id}
                onClick={() => onClick(part)}
                extra={selectedPart?selectedPart.part_id == part.part_id ?<CheckCircleTwoTone style={{fontSize:"2em"}} twoToneColor={"#52c41a"} />:
                    null: null}
            >
                <List.Item.Meta
                    title={part.name}
                    description={part.decimal_num}
                />
            </List.Item>
            <div/>
        </>
    )
};