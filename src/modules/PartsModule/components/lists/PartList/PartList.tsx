import React from "react";
import {useDispatch} from "react-redux";
import {INObjects} from "../../../../../store/base.types";
import {IPart} from "../../../part.types";
import {PartOperations} from "../../../store/parts.operations";
import {Col, List, Pagination, Row} from "antd";
import {PartItem} from "./PartItem";

const _ = require('lodash');

interface PartListProps {
    parts: INObjects<IPart>
    part_count: number
    page_size: number
    page: number
    handelClick: (number)=>void
    selectedPart: IPart
}

export const PartList = ({parts, part_count, page_size, page, handelClick, selectedPart}: PartListProps) => {
    const dispatch = useDispatch();
    const changePage = (values) => {
        dispatch(PartOperations.changePage(values));
    };
    const changePartPerPage = (current, size) => {
        dispatch(PartOperations.changePartPerPage(size));
    };

    return (
        <>
            <List
                dataSource={_.toArray(parts.byId)}
                renderItem={(part: IPart) => (
                    <PartItem part={part} onClick={handelClick} selectedPart={selectedPart}/>
                )}/>
                <Row style ={{marginTop:30}} justify={"space-between"}>
                    <Col>

                    </Col>
                    <Col>
                        <Pagination
                            total={part_count}
                            pageSize={page_size}
                            current={page}
                            onChange={changePage}
                            onShowSizeChange={changePartPerPage}
                        />
                    </Col>
                </Row>

        </>
    )
};