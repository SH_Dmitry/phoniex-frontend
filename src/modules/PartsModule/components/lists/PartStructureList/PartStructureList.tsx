'use strict';

import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {DragItem} from "../../../../../store/base.types";
import {useDrop} from "react-dnd";
import {List} from "antd";
import {PartStructureItem} from "./PartStructureItem";
import {DropArea} from "../../../../../components/dumb/DropArea/DropArea";
import {PartStructureOperations} from "../../../store/part.structure/structure.operations";
import {IPart, IPartDropType} from "../../../part.types";
import {AppState} from "../../../../../store/app.reducer";

const _ = require('lodash');


interface Structure_PartListProps {
    part_id: string
}

export const PartStructureList = ({part_id}: Structure_PartListProps) => {
    const dispatch = useDispatch();
    const structure = useSelector((state: AppState) => state.part_info.structure.structure);
    useEffect(() => {
        dispatch(PartStructureOperations.getStructure(part_id))
    }, [part_id]);
    const handleDelete = (child_id:string)=>{
        dispatch(PartStructureOperations.deletePart(part_id,child_id))
    };

    const handleUpdateCount = (child_id:string, count:number|string|undefined)=>{
        if(typeof count === "number")
        dispatch(PartStructureOperations.updatePartCount(part_id,child_id,count))
    };
    const [{canDrop, isOver}, drop] = useDrop({
        accept: IPartDropType,
        drop(result: DragItem<IPart>) {
            dispatch(PartStructureOperations.addPart(part_id, result.item.part_id))
        },
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        }),
    });

    return (
            <div ref={drop}>
                {canDrop ?
                    <DropArea canDrop={canDrop} isOver={isOver}/>
                    :
                    <List
                        pagination={{ pageSize: 7 }}
                        dataSource={_.values(structure.byId)}
                        renderItem={(item: IPart)=><PartStructureItem part={item} handleDelete={handleDelete} handleUpdateCount={handleUpdateCount}/>}
                    >
                    </List>}
            </div>
    )
};


