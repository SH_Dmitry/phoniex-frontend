import React from "react";
import {List} from "antd";
import {getPartTypeIcon} from "../../PartTypeSelector/PartType.const";
import {IPartStructureItem} from "../../../part.types";

interface PartStructureDumpItemProps {
    part: IPartStructureItem
}

export const PartStructureDumpItem = ({part}: PartStructureDumpItemProps) => {
    return (
        <List.Item
            key={part.part_id}
            extra={"Количество: " + part.count}>
            {getPartTypeIcon(part.type_id, {fontSize: "2em", paddingRight: "16px"})}
            <List.Item.Meta
                title={part.name}
                description={part.decimal_num}
            />
        </List.Item>
    )
};