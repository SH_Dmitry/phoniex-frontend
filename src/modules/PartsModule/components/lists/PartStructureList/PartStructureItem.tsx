import React from "react";
import {InputNumber, List} from "antd";
import {CloseOutlined} from "@ant-design/icons/lib";
import {useHistory} from "react-router-dom";
import {getPartTypeIcon} from "../../PartTypeSelector/PartType.const";
import {IStructurePart} from "../../../part.types";

interface Structure_PartItemProps {
    part: IStructurePart
    handleDelete: (child_id: string) => any
    handleUpdateCount: (child_id: string, count: number | string | undefined) => any
}

export const PartStructureItem = ({part, handleDelete, handleUpdateCount}: Structure_PartItemProps) => {
    const history = useHistory();

    function openInfo() {
        history.push("/main/part/" + part.part_id);
    }

    return (
        <List.Item
            key={part.part_id}
            actions={[
                <InputNumber size="middle"
                             min={1}
                             max={100}
                             defaultValue={part.count}
                             onChange={(value) => {
                                 handleUpdateCount(part.part_id, value)
                             }}/>,
                <CloseOutlined onClick={() => handleDelete(part.part_id)}/>]}
        >
            {getPartTypeIcon(part.type_id, {fontSize: "2em", paddingRight: "16px"})}
            <List.Item.Meta
                title={<a onClick={openInfo}>{part.name}</a>}
                description={part.decimal_num}
            />
        </List.Item>
    )
};