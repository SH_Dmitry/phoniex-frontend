import React from "react";
import {useDispatch} from "react-redux";
import {Card, List, Pagination} from "antd";
import styles from "./PartListDraggable.module.less";
import {PartItemDraggable} from "./PartItemDraggable";
import {IPart} from "../../../part.types";
import {PartOperations} from "../../../store/parts.operations";
import {PartsFilterDrawer} from "../../drawers/PartsFilterDrawer/PartsFilterDrawer";
import {INObjects} from "../../../../../store/base.types";

const _ = require('lodash');

interface PartListDraggableProps {
    parts: INObjects<IPart>
    part_count: number
    page_size: number
    page: number
}

export const PartListDraggable = ({parts, part_count, page_size, page}: PartListDraggableProps) => {
    const dispatch = useDispatch();
    const changePage = (values) => {
        dispatch(PartOperations.changePage(values));
    };
    const changePartPerPage = (current, size) => {
        dispatch(PartOperations.changePartPerPage(size));
    };
    return (
            <Card style={{height:"100%"}} title={"Детали и сборочные единицы"}
                  extra={
                      <div style={{display: "flex", alignItems: "center"}}>
                          <PartsFilterDrawer/>
                      </div>}
                  className={styles.database}>
                <List
                    dataSource={_.toArray(parts.byId)}
                    renderItem={(part: IPart) => (
                        <PartItemDraggable part={part}/>
                    )}/>
                <Pagination
                    style={{marginTop: 30}}
                    total={part_count}
                    pageSize={page_size}
                    current={page}
                    onChange={changePage}
                    onShowSizeChange={changePartPerPage}
                />
            </Card>
    )
};