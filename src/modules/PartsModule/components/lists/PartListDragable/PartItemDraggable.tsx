import React from "react";
import {useDrag} from "react-dnd";
import {List} from "antd";
import {useHistory} from "react-router-dom";
import {getPartTypeIcon} from "../../PartTypeSelector/PartType.const";
import {IPart, IPartDropType} from "../../../part.types";

interface Part_DragProps {
    part: IPart
}

export const PartItemDraggable = ({part}: Part_DragProps) => {
    const [, drag] = useDrag({
        item: {item: part, type: IPartDropType},
    });

    const history = useHistory();

    function openInfo() {
        history.push("/main/part/" + part.part_id);
    }

    return (
        <>
            <div ref={drag}>
                <List.Item key={part.part_id}>
                    {getPartTypeIcon(part.type_id, {fontSize: "2em", paddingRight: "16px"})}
                    <List.Item.Meta
                        title={<a onClick={openInfo}>{part.name}</a>}
                        description={part.decimal_num}
                    />
                </List.Item>
                <div/>
            </div>
        </>
    )
};