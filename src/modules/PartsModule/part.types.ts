import {IResUser} from "../SecurityGateModule";
import {INData} from "../../store/base.types";

//Drop selectors
export const IPartDropType = "part";

//Domain types
export interface IPart {
    part_id: string
    name: string
    decimal_num: string
    type_id: string
    deleted:number
    description?:string
    has_materials?: number
    has_structure?: number
    has_techprocess?: number
}

export interface IPartSimple {
    part_id: string
    name: string
    decimal_num: string
    type_id: string
    deleted:number
}

//Part structure item types
export type IPartStructureSimpleItem = {
    decimal_num: string,
    count: string
}
export type IPartStructureItem = {
    part_id:string,
    name:string,
    decimal_num: string,
    type_id: string
    count: string
}

//Part structure types
export type IPartStructuresSimple = {
    decimal_num: string,
    structures: Array<IPartStructureSimpleItem>
}
export type IPartStructures = {
    part_id: string,
    name: string
    decimal_num: string,
    structures: Array<IPartStructureItem>
}

export interface IStructurePart extends IPart{
    count?: number
    children?: Array<string>
}

export interface IPartType {
    part_type_id: string
    name: string
}

export interface IHierarchyParts {
    key: string
    type_id: string
    title: string
    count: number
    children?: Array<IHierarchyParts>
}

export interface IPartHistory {
    history_id: string
    operation: string
    date_time: string

    user_id: string
    personal_data: string
    state: string
    image: any
}

export interface IComment {
    comment_id: string
    text: string
    date_time: string
    replies: Array<string>

    user_id: string
    personal_data: string
    state: string
    image: any
}

// Get part request body types
export  type IOption = {
    pagination: IPagination
    sort: ISort
    filters: Array<IFilter>
}

export type IPagination = {
    page: number
    page_size: number
}

export type ISort = {
    column: string
    direct: string
}

export type IFilter = {
    column: string
    value: any
    exactly: boolean
}


//Server response types
export type IRParts = {
    parts: Array<IPart>
    part_count: number
}

export type IRPartWithStructure = {
    part_id: string
    name: string
    type_id: string
    children?: Array<IRPartWithStructure>
}

export interface IRComment {
    comments_id: string
    user: IResUser
    replies: Array<IRComment>
    text: string
    date_time: Date
}

export interface IRHistory {
    history_id: string
    user: IResUser
    operation: string
    date_time: Date
}

//Normalizable types
export type INRParts = {
    part_count: number
    parts: INParts
}

export type INParts = {
    result: Array<string>
    entities: {
        parts: INData<IPart>
    }
}

export type INPartTypes = {
    result: Array<string>
    entities: {
        part_types: INData<IPartType>
    }
}

export type INComments = {
    result: Array<string>
    entities: {
        comments: INData<IComment>
    }
}

export type INHistory = {
    result: Array<string>
    entities: {
        history: INData<IPartHistory>
    }
}