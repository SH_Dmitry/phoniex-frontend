import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Empty, PageHeader, Tabs} from "antd";
import {PartInfoTab} from "../../tabs/PartInfoTab";
import {useHistory, useParams} from "react-router-dom";
import {
    ApartmentOutlined,
    ClockCircleOutlined,
    InfoCircleOutlined,
    SettingOutlined,
    SketchOutlined
} from "@ant-design/icons/lib";
import style from "./PartInfo.module.less"
import {PartInfoOperations} from "../../store/part.info/info.operations";
import {PartStructureTab} from "../../tabs/PartStructureTab";
import {PartTypes} from "../../components/PartTypeSelector/PartType.const";
import {PartMaterialsTab} from "../../tabs/PartMaterialsTab";
import {AppState} from "../../../../store/app.reducer";


const {TabPane} = Tabs;


export const PartInfo = () => {
    let {part_id} = useParams();
    const history = useHistory();
    const part = useSelector((state: AppState) => state.part_info.info.part);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(PartInfoOperations.getPartById(part_id));
    }, [part_id]);
    if (part) {
        return (
            <>
                <PageHeader onBack={history.goBack} title={part.name} subTitle={part.decimal_num}/>
                <Tabs  defaultActiveKey="1" className={style.content}>
                    <TabPane tab={<span><InfoCircleOutlined/>Информация</span>} key="1">
                        <PartInfoTab part_id={part_id}/>
                    </TabPane>
                    {PartTypes[part.type_id].hasStructure ?
                        <TabPane tab={<span><ApartmentOutlined/>Струкутра</span>} key="2">
                            <PartStructureTab part_id={part_id}/>
                        </TabPane> : null
                    }
                    <TabPane tab={<span><SettingOutlined/>Тех.процесс</span>} key="3">

                    </TabPane>
                    <TabPane tab={<span><ClockCircleOutlined/>Трудоемкость</span>} key="4">
                        Content of Tab Pane 3
                    </TabPane>
                    <TabPane tab={<span><SketchOutlined />Материалы</span>} key="5">
                        <PartMaterialsTab part_id={part_id}/>
                    </TabPane>
                </Tabs>
            </>
        )
    } else {
        return (<Empty/>)
    }
};
