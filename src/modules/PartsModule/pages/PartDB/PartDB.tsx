import React from "react";
import {Button, Card, Pagination, Result, Typography} from "antd";
import {PartTable} from "../../components/PartTable";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom";
import {PartOperations} from "../../store/parts.operations";
import {AppState} from "../../../../store/app.reducer";
import {ModuleFormDialog} from "../../../../components/dialogs/ModuleFormDialog";
import {IPart} from "../../part.types";
import {PartFormFields} from "../../../../components/forms/FormFields";
import {PartsFilterDrawer} from "../../components/drawers/PartsFilterDrawer/PartsFilterDrawer";
import {ReloadOutlined, SearchOutlined} from "@ant-design/icons/lib";


export const PartDB = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const parts = useSelector((state: AppState) => state.parts.parts);

    //Pagination state
    const page = useSelector((state: AppState) => state.parts.pagination.page);
    const part_count = useSelector((state: AppState) => state.parts.part_count);
    const part_per_page = useSelector((state: AppState) => state.parts.pagination.page_size);

    //Add new part dialog visibility
    const [visibility, setVisibility] = React.useState(false);
    const showDialog = () => {
        setVisibility(true);
    };
    const hideDialog = () => {
        setVisibility(false);
    };

    //Add new part
    const addPart = (values) => {
        dispatch(PartOperations.addPart(values));
        hideDialog();
    };

    //Pagination changes
    const changePage = (values) => {
        dispatch(PartOperations.changePage(values));
    };
    const changePartPerPage = (current, size) => {
        dispatch(PartOperations.changePartPerPage(size));
    };

    return (
        <>
            <Card title={<div style={{display: "inline-flex", alignItems: "center"}}>
                <Button style={{marginRight: 16}} type="text" icon={<ReloadOutlined/>}
                        onClick={() => dispatch(PartOperations.getParts())}/>
                <Typography>База деталей и сборочных единиц</Typography>
            </div>}
                  extra=
                      {<div style={{display: "inline-flex", alignItems: "center"}}>
                          <Button style={{marginRight: 16}} type={"primary"} onClick={showDialog}>Добавить ДСЕ</Button>
                          <PartsFilterDrawer/>
                      </div>}
            >
                {parts.allIds.length > 0 ?
                    <>
                        <PartTable data={parts}
                                   onUpdate={(oldPart, newPart) => dispatch(PartOperations.updatePart(oldPart, newPart))}
                                   onDelete={(Part) => dispatch(PartOperations.deletePart(Part.part_id))}
                                   onInfo={(Part) => history.push("/main/part/" + Part.part_id)}/>
                        <Pagination
                            style={{marginTop: 30}}
                            total={part_count}
                            pageSize={part_per_page}
                            current={page}
                            onChange={changePage}
                            onShowSizeChange={changePartPerPage}>
                        </Pagination>
                    </>
                    :
                    <Result
                        icon={<SearchOutlined/>}
                        title="К сожалению мы ничего не нашли. Возможно стоит изменить параметры фильтрации?"
                        extra={<PartsFilterDrawer text={"Перейти к фильтрам"}/>}
                    />
                }
            </Card>

            {/*Modal dialog*/}
            <ModuleFormDialog<IPart>
                name="add_project"
                visible={visibility}
                title="Создать изделие"
                submitText="Создать"
                initialValues={{
                    part_id: "0",
                    type_id: null,
                    name: "",
                    decimal_num: "",
                    has_materials: 0,
                    has_structure: 0,
                    has_techprocess: 0,
                    deleted: 0,
                    description: ""
                }}
                fields={PartFormFields}
                handleClose={hideDialog}
                handleSubmit={addPart}
            />
        </>
    )
};