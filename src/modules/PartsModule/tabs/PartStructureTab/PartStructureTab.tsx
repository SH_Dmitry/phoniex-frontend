import React from "react";
import {DndProvider} from "react-dnd";
import {HTML5Backend} from "react-dnd-html5-backend";
import {Card, Col, Row} from "antd";
import {DrawerCallerHOC} from "../../components/drawers/DrawerCallerHOC";
import {HierarchyDrawer} from "../../components/drawers/HierarchyDrawer";
import {PartStructureList} from "../../components/lists/PartStructureList";
import {PartListDraggable} from "../../components/lists/PartListDragable";
import {useSelector} from "react-redux";
import {AppState} from "../../../../store/app.reducer";

type partStructureProps = {
    part_id: string
}

export const PartStructureTab = ({part_id}: partStructureProps) => {
    const parts = useSelector((state: AppState) => state.parts.parts);
    const page_size = useSelector((state: AppState) => state.parts.pagination.page_size);
    const page = useSelector((state: AppState) => state.parts.pagination.page);
    const part_count = useSelector((state: AppState) => state.parts.part_count);
    return (
        <>
            <DndProvider backend={HTML5Backend}>
                <Row gutter={[30, 30]}>
                    <Col xs={{span: 24}} lg={{span: 12}}>
                        <Card style={{height:"100%"}} title={"Состав"} extra={
                            <DrawerCallerHOC
                                Drawer={HierarchyDrawer}
                                part_id={part_id}
                                name={"Древовидная структура"}/>
                        }>
                            <PartStructureList part_id={part_id}/>
                        </Card>
                    </Col>
                    <Col xs={{span: 24}} lg={{span: 12}}>
                        <PartListDraggable
                            parts={parts}
                            part_count={part_count}
                            page_size={page_size}
                            page={page}/>
                    </Col>
                </Row>
            </DndProvider>
        </>
    )
};

