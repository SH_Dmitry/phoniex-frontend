import React from "react";
import {Part_CommentsList} from "../../blocks/PartCommentsBlock";
import {PartInfoBlock} from "../../blocks/PartInfoBlock/PartInfoBlock";
import {Card, Col, Row} from "antd";
import {PartApproveBlock} from "../../blocks/PartApproveBlock/PartApproveBlock";

type partInfoProps = {
    part_id: string
}

export const PartInfoTab = ({part_id}: partInfoProps) => {
    return (
        <>
            <Row gutter={[16, 16]}>
                <Col span={18}>
                    <PartInfoBlock/>
                </Col>
                <Col span={6}>
                    <PartApproveBlock/>
                </Col>
            </Row>
            <Row gutter={[16, 16]}>
                <Col span={24}>
                    <Card title="Обсуждение">
                        <Part_CommentsList part_id={part_id}/>
                    </Card>
                </Col>
            </Row>
        </>
    )
};

