import React from "react";
import {DndProvider} from "react-dnd";
import {HTML5Backend} from "react-dnd-html5-backend";
import {Card, Col, Divider, Row, Typography} from "antd";
import {MaterialListDraggable} from "../../../MaterialsModule/container/MaterialListDraggable";
import {PartMaterialsList} from "../../../MaterialsModule/container/PartMaterialsList";
import {MaterialGroupsSelector} from "../../../MaterialsModule/components/selectors/MaterialGroupsSelector";
import {MaterialUndergroupsSelector} from "../../../MaterialsModule/components/selectors/MaterialUndergroupsSelector";

const {Text} = Typography;
type partStructureProps = {
    part_id: string
}

export const PartMaterialsTab = ({part_id}: partStructureProps) => {
    return (
        <>
            <DndProvider backend={HTML5Backend}>
                <Row gutter={[30, 30]}>
                    <Col xs={{span: 24}} lg={{span: 14}}>
                        <Card title="Материалы">
                            <PartMaterialsList part_id={part_id}/>
                        </Card>
                    </Col>
                    <Col xs={{span: 24}} lg={{span: 10}}>
                        <Card title={"База материалов"}>
                            <div style={{display: "block"}}>
                                <span style={{display: "flex"}}>
                                    <Text style={{width: "200px"}}>Категория</Text>
                                    <MaterialGroupsSelector style={{marginBottom: "16px"}}/>
                                </span>
                                <span style={{display: "flex"}}>
                                    <Text style={{width: "200px"}}>Подкатегория</Text>
                                    <MaterialUndergroupsSelector/>
                                </span>
                            </div>
                            <Divider/>
                            <MaterialListDraggable/>
                        </Card>
                    </Col>
                </Row>
            </DndProvider>
        </>
    )
};

