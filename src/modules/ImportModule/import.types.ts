

type IImportMessage<T> = {
    id: number,
    item_type: string, //imported item type
    status: string, // "added", "intersection", "failure"
    item: T, //imported item
    intersection?: T    // Item from db, if import has intersection
    error_message?: string //error message, if import failure
}