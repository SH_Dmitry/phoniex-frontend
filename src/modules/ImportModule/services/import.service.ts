import {bearerSecurity} from "../../SecurityGateModule/services/bearer.security";
import {apiWrapper} from "../../../tools/api/api.wrapper";
import {IGroup, IMaterial, IUndergroup} from "../../MaterialsModule/materials.types";
import {IPartSimple} from "../../PartsModule/part.types";

export const importService = {
    async importGroup(data: string): Promise<Array<IImportMessage<IGroup>>> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify(data)
        };
        return apiWrapper(`/import/group`, requestOptions);
    },

    async importUndergroup(data: string): Promise<Array<IImportMessage<IUndergroup>>> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify(data)
        };
        return await apiWrapper(`/import/undergroup`, requestOptions);
    },
    async importMaterial(data: string): Promise<Array<IImportMessage<IMaterial>>> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify(data)
        };
        return await apiWrapper(`/import/material`, requestOptions);
    },
    async importPart(data: string): Promise<Array<IImportMessage<IPartSimple>>> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify(data)
        };
        return await apiWrapper(`/import/part`, requestOptions);
    },
    async importPartMaterials(data: string): Promise<Array<IImportMessage<any>>> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify(data)
        };
        return await apiWrapper(`/import/part/materials`, requestOptions);
    },
    async importPartStructures(data: string): Promise<Array<IImportMessage<any>>> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify(data)
        };
        return await apiWrapper(`/import/part/structures`, requestOptions);
    }
};

