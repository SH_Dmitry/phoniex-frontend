import React from "react";
import {Col, PageHeader, Row} from "antd";
import {ImportGroupCard} from "../components/ImportBlock/ImportGroupCard";
import {ImportUndergroupCard} from "../components/ImportBlock/ImportUndergroupCard";
import {ImportMaterialCard} from "../components/ImportBlock/ImportMaterialCard";
import {ImportPartCard} from "../components/ImportBlock/ImportPartCard";
import {ImportPartStructureCard} from "../components/ImportBlock/ImportPartStructureCard";
import {ImportPartMaterialCard} from "../components/ImportBlock/ImportPartMaterialCard";


export const ImportMenu = () => {
    return (
        <div>
            <PageHeader title={"Выберите тип загружаемых данных"}
                        subTitle={"Для корректной загрузки даных, они должны соответствовать шаблонам"}/>
            <Row gutter={[16, 16]}>
                <Col span={7}>
                    <ImportGroupCard/>
                </Col>
                <Col span={7}>
                    <ImportUndergroupCard/>
                </Col>
                <Col span={10}>
                    <ImportMaterialCard/>
                </Col>
            </Row>
            <Row gutter={[16, 16]}>
                <Col span={7}>
                    <ImportPartCard/>
                </Col>
                <Col span={7}>
                    <ImportPartMaterialCard/>
                </Col>
                <Col span={10}>
                    <ImportPartStructureCard/>
                </Col>
            </Row>
        </div>
    )
};