import React, {useState} from "react";
import {useSelector} from "react-redux";
import {AppState} from "../../../store/app.reducer";
import {Button, Card, PageHeader, Result, Steps} from "antd";
import {AddedBlock} from "../components/AddedBlock/AddedBlock";
import {IntersectionBlock} from "../components/IntersectionBlock/IntersectionBlock";

const {Step} = Steps;

interface ImportMergeProps {
}


export const ImportMerge = ({}: ImportMergeProps) => {
    const addedMsg = useSelector((state: AppState) => state.import.addedMsg);
    const intersectionMsg = useSelector((state: AppState) => state.import.intersectionMsg);
    const errorsMsg = useSelector((state: AppState) => state.import.errorMsg);
    const [current, setCurrent] = useState(0);
    const type = useSelector((state: AppState) => state.import.type);
    const [showResult, setShowResult] = useState(false);
    const steps = [
        {
            content: addedMsg.length?(showResult) ?
                <AddedBlock messages={addedMsg} type={type}/> :
                <Result
                    status="success"
                    title="Все данные успешно добавлены!"
                    subTitle={"В ходе импорта был добавлено объектов: " + addedMsg.length || 0 }
                    extra={[
                        <Button type="primary" key="console" onClick={() => setShowResult(true)}>
                            Отобразить результаты
                        </Button>
                    ]}
                />:
                <Result
                    status="warning"
                    title="В ходе импорта не было добавленно ни одного объекта!"
                />
            ,
        },
        {
            content: <IntersectionBlock messages={intersectionMsg} type={type}/>,
        },
        {
            title: 'Last',
            content: 'Last-content',
        },
    ];

    const onChange = current => {
        setCurrent(current)
    };

    return (
        <>
            <PageHeader title={"Результаты"} subTitle={"Просмотр результатов и разрешение конфликтов"}/>
            <Card>
                <Steps current={current} type="navigation" onChange={onChange}>
                    <Step title="Добавлены" description={`${addedMsg.length || "0"} записей`}/>
                    <Step title="Замена" description={`${intersectionMsg.length || "0"} записей`}/>
                    <Step title="Ошибки" description={`${errorsMsg.length || "0"} записей`}/>
                </Steps>
            </Card>
            <>
                {steps[current].content}
            </>
        </>
    )
};