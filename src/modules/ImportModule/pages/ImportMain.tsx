import React from "react";
import {Route} from "react-router";
import {ImportMenu} from "./ImportMenu";
import {ImportMerge} from "./ImportMerge";


interface ImportPageProps {
}

export const ImportMain = ({}: ImportPageProps) => {

    return (
        <>
            <Route exact path='/main/import/menu' component={ImportMenu}/>
            <Route exact path='/main/import/merge' component={ImportMerge}/>
        </>
    )
};