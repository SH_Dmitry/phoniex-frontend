import React, {useState} from "react";
import {Card, Collapse, Descriptions, Pagination} from "antd";
import {CheckCircleTwoTone} from "@ant-design/icons/lib";

const {Panel} = Collapse;

interface AddedUndergroupListProps {
    messages: Array<IImportMessage<any>>
}

export const AddedUndergroupList = ({messages}: AddedUndergroupListProps) => {
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    return (
        <Card>
            <Collapse accordion bordered>
                {messages.map((message, index) => {
                    if (index >= (page - 1) * 10 && index < page * 10) {
                        return <Panel header={message.item.name} key={message.item.undergroup_id}
                                      extra={<CheckCircleTwoTone twoToneColor="#52c41a"/>}>
                            <Descriptions bordered column={2}>
                                <Descriptions.Item label="ID">{message.item.undergroup_id}</Descriptions.Item>
                                <Descriptions.Item
                                    label="Наименование">{message.item.name}</Descriptions.Item>
                                <Descriptions.Item
                                    label="Порядковый номер">{message.item.number || 100}</Descriptions.Item>
                                <Descriptions.Item
                                    label="ID группы">{message.item.group_id}</Descriptions.Item>
                            </Descriptions>
                        </Panel>
                    }
                })}
            </Collapse>
            <Pagination style={{marginTop: 30}} total={messages.length || 0} current={page} pageSize={perPage}
                        onChange={(current, perPage) => {
                            setPerPage(perPage);
                            setPage(current)
                        }}/>
        </Card>
    )
};