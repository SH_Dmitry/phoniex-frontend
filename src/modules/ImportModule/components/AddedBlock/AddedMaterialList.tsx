import React, {useState} from "react";
import {Card, Collapse, Descriptions, Pagination} from "antd";
import {ExclamationCircleTwoTone} from "@ant-design/icons/lib";
import {MaterialLabel} from "../../../MaterialsModule/components/MaterialLabel";

const {Panel} = Collapse;

interface AddedMaterialListProps {
    messages: Array<IImportMessage<any>>
}

export const AddedMaterialList = ({messages}: AddedMaterialListProps) => {
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    return (
        <Card>
            <Collapse accordion bordered>
                {messages.map((message, index) => {
                    if (index >= (page - 1) * perPage && index < page * perPage) {
                        return (
                            <Panel header={<MaterialLabel material={message.item}/>}
                                   key={message.item.material_id}
                                   extra={<ExclamationCircleTwoTone twoToneColor="#52c41a"/>}>
                                <Descriptions bordered column={1}>
                                    <Descriptions.Item
                                        label="Идентификатор">{message.item.material_id}</Descriptions.Item>
                                    <Descriptions.Item label="Наименование">{<MaterialLabel
                                        material={message.item}/>}</Descriptions.Item>
                                    <Descriptions.Item
                                        label="Подгруппа">{message.item.undergroup_id || "Не найдена"}</Descriptions.Item>
                                </Descriptions>
                            </Panel>
                        );

                    }
                })}
            </Collapse>
            <Pagination style={{marginTop: 30}} total={messages.length || 0} current={page} pageSize={perPage}
                        onChange={(current, perPage) => {
                            setPerPage(perPage);
                            setPage(current)
                        }}/>
        </Card>
    )
};