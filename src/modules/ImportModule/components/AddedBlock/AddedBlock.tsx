import React from "react";
import {Collapse} from "antd";
import {AddedGroupList} from "./AddedGroupList";
import {AddedUndergroupList} from "./AddedUndergroupList";
import {AddedMaterialList} from "./AddedMaterialList";
import {AddedPartList} from "./AddedPartList";
import {AddedPartMaterialsList} from "./AddedPartMaterialsList";

const {Panel} = Collapse;

interface AddedBlockProps {
    messages: Array<IImportMessage<any>>
    type: string
}

export const AddedBlock = ({messages, type}: AddedBlockProps) => {
    switch (type) {
        case 'group': return (<AddedGroupList messages={messages}/>);
        case 'undergroup': return (<AddedUndergroupList messages={messages}/>);
        case 'material': return (<AddedMaterialList messages={messages}/>);
        case 'part': return (<AddedPartList messages={messages}/>);
        case 'part_materials': return  (<AddedPartMaterialsList messages={messages}/>)
    }
};