import React, {useState} from "react";
import {Card, Collapse, Descriptions, List, Pagination} from "antd";
import {ExclamationCircleTwoTone} from "@ant-design/icons/lib";
import {PartMaterialItem} from "../../../MaterialsModule/components/PartMaterialItem";
import {IPartMaterials} from "../../../MaterialsModule/materials.types";

const {Panel} = Collapse;

interface AddedPartMaterialsListProps {
    messages: Array<IImportMessage<IPartMaterials>>
}

export const AddedPartMaterialsList = ({messages}: AddedPartMaterialsListProps) => {
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    return (
        <Card>
            <Collapse accordion bordered>
                {messages.map((message, index) => {
                    if (index >= (page - 1) * perPage && index < page * perPage) {
                        return (
                            <Panel header={message.item.name + " " + message.item.decimal_num}
                                   key={message.item.part_id}
                                   extra={<ExclamationCircleTwoTone twoToneColor="#52c41a"/>}>
                                <Descriptions bordered column={1}>
                                    <Descriptions.Item
                                        label="Идентификатор">{message.item.part_id}</Descriptions.Item>
                                    <Descriptions.Item
                                        label="Наименование">{message.item.name}</Descriptions.Item>
                                    <Descriptions.Item
                                        label="Децимальный номер">{message.item.decimal_num}</Descriptions.Item>
                                </Descriptions>
                                <List>
                                    {message.item.materials.map(material => {
                                        return <PartMaterialItem material={material}/>
                                    })}
                                </List>
                            </Panel>
                        );

                    }
                })}
            </Collapse>
            <Pagination style={{marginTop: 30}} total={messages.length || 0} current={page} pageSize={perPage}
                        onChange={(current, perPage) => {
                            setPerPage(perPage);
                            setPage(current)
                        }}/>
        </Card>
    )
};