import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../../store/app.reducer";
import {Button, Card, message, Spin} from "antd";
import {UploadOutlined} from "@ant-design/icons/lib";
import ReactJson from "react-json-view";
import {useHistory} from "react-router-dom";
import {FilePicker} from 'react-file-picker'
import {ImportOperations} from "../../store/import.operations";

interface ImportGroupCardProps {
}

const partStructureJson = [
    {
        decimal_num: "Децимальный номер",
        structure: [
            {
                decimal_num: "Децимальный номер",
                structure: [
                    {}
                ]
            }
        ]
    }];

export const ImportPartStructureCard = ({}: ImportGroupCardProps) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const type = useSelector((state: AppState) => state.import.type);
    const loading = useSelector((state: AppState) => state.import.loading);

    const onPartStructureReaderLoad = async (event) => {
        const file = JSON.parse(event.target.result);
        if (file && file[0] && file[0].decimal_num && file[0].structures) {
            await dispatch(ImportOperations.importPartStructures(file));
            history.push("/main/import/merge")
        } else {
            message.error('Неверный формат файла!');
        }
    };
    return (
        <Card style={{height: "100%"}} title="Структура" bordered={false} extra={
            <FilePicker
                extensions={['json']}
                onChange={FileObject => {
                    const reader = new FileReader;
                    reader.readAsText(FileObject);
                    reader.onload = onPartStructureReaderLoad;
                }}>
                <Button icon={<UploadOutlined/>}>Загрузить</Button>
            </FilePicker>}>
            <Spin tip="Загружаем структуру..." spinning={type == "part_structures" && loading}>
                <ReactJson src={partStructureJson} displayDataTypes={false} enableClipboard={false}/>
            </Spin>
        </Card>
    )
};