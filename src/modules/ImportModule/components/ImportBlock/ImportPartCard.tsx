import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../../store/app.reducer";
import {Button, Card, message, Spin} from "antd";
import {UploadOutlined} from "@ant-design/icons/lib";
import ReactJson from "react-json-view";
import {useHistory} from "react-router-dom";
import {FilePicker} from 'react-file-picker'
import {ImportOperations} from "../../store/import.operations";

const partJson = [
    {
        name: "Наименование ДСЕ",
        decimal_num: "Децимальный номер",
        type_id: "Тип ДСЕ"
    }
];

export const ImportPartCard = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const type = useSelector((state: AppState) => state.import.type);
    const loading = useSelector((state: AppState) => state.import.loading);

    const onPartReaderLoad = async (event) => {
        const file = JSON.parse(event.target.result);
        if (file && file[0] && file[0].name && file[0].decimal_num) {
            await dispatch(ImportOperations.importPart(file));
            history.push("/main/import/merge")
        } else {
            message.error('Неверный формат файла!');
        }
    };
    return (
        <Card style={{height: "100%"}} title="Детали и сборочные единицы" bordered={false} extra={
            <FilePicker
                extensions={['json']}
                onChange={FileObject => {
                    const reader = new FileReader;
                    reader.readAsText(FileObject);
                    reader.onload = onPartReaderLoad;
                }}>
                <Button icon={<UploadOutlined/>}>Загрузить</Button>
            </FilePicker>}>
            <Spin tip="Загружаем ДСЕ..." spinning={type == "part" && loading}>
                <ReactJson src={partJson} displayDataTypes={false} enableClipboard={false}/>
            </Spin>
        </Card>
    )
};