import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../../store/app.reducer";
import {Button, Card, message, Spin} from "antd";
import {UploadOutlined} from "@ant-design/icons/lib";
import ReactJson from "react-json-view";
import {ImportOperations} from "../../store/import.operations";
import {useHistory} from "react-router-dom";
import {FilePicker} from 'react-file-picker'

interface ImportGroupCardProps {
}

const groupJson = [
    {
        group_id: "Id группы",
        name: "Наименование группы",
        number: "Место группы в списке"
    }
];

export const ImportGroupCard = ({}: ImportGroupCardProps) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const type = useSelector((state: AppState) => state.import.type);
    const loading = useSelector((state: AppState) => state.import.loading);

    const onGroupReaderLoad = async (event) => {
        const file = JSON.parse(event.target.result);
        if (file && file[0] && file[0].group_id && file[0].name && !file[0].undergroup_id) {
            await dispatch(ImportOperations.importGroup(file));
            history.push("/main/import/merge")
        } else {
            message.error('Неверный формат файла!');
        }
    };
    return (
        <Card style={{height: "100%"}} title="Группы материалов" bordered={false} extra={
            <FilePicker
                extensions={['json']}
                onChange={FileObject => {
                    const reader = new FileReader;
                    reader.readAsText(FileObject);
                    reader.onload = onGroupReaderLoad;
                }}>
                <Button icon={<UploadOutlined/>}>Загрузить</Button>
            </FilePicker>}>
            <Spin tip="Загружаем группы..." spinning={type == "group" && loading}>
                <ReactJson src={groupJson} displayDataTypes={false} enableClipboard={false}/>
            </Spin>
        </Card>

    )
};