import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../../store/app.reducer";
import {Button, Card, message, Spin} from "antd";
import {UploadOutlined} from "@ant-design/icons/lib";
import ReactJson from "react-json-view";
import {ImportOperations} from "../../store/import.operations";
import {useHistory} from "react-router-dom";
import {FilePicker} from 'react-file-picker'

interface ImportGroupCardProps {
}

const undergroupJson = [
    {
        undergroup_id: "Id подгруппы",
        group_id: "Id группы",
        name: "Наименование подгруппы",
        number: "Место подгруппы в списке"
    }
];

export const ImportUndergroupCard = ({}: ImportGroupCardProps) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const type = useSelector((state: AppState) => state.import.type);
    const loading = useSelector((state: AppState) => state.import.loading);

    const onUndergroupReaderLoad = async (event) => {
        const file = JSON.parse(event.target.result);
        if (file && file[0] && file[0].undergroup_id && file[0].name && file[0].group_id) {
            await dispatch(ImportOperations.importUndergroup(file));
            history.push("/main/import/merge")
        } else {
            message.error('Неверный формат файла!');
        }
    };
    return (

        <Card style={{height: "100%"}} title="Подгруппы материалов" bordered={false} extra={
            <FilePicker
                extensions={['json']}
                onChange={FileObject => {
                    const reader = new FileReader;
                    reader.readAsText(FileObject);
                    reader.onload = onUndergroupReaderLoad;
                }}>
                <Button icon={<UploadOutlined/>}>Загрузить</Button>
            </FilePicker>}>
            <Spin tip="Загружаем подгруппы..." spinning={type == "undergroup" && loading}>
                <ReactJson src={undergroupJson} displayDataTypes={false} enableClipboard={false}/>
            </Spin>
        </Card>

    )
};