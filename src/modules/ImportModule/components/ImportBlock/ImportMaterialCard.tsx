import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../../store/app.reducer";
import {Button, Card, message, Spin} from "antd";
import {UploadOutlined} from "@ant-design/icons/lib";
import ReactJson from "react-json-view";
import {ImportOperations} from "../../store/import.operations";
import {useHistory} from "react-router-dom";
import {FilePicker} from 'react-file-picker'

interface ImportGroupCardProps {
}

const materialJson = [
    {
        material_id: "Id материала",
        sortament: "Сортамент материала (может быть NULL)",
        material: "Наименование материала",
        forma: "Форма материала (может быть NULL)",
        undergroup_id: "Id подгруппы",
    }
];

export const ImportMaterialCard = ({}: ImportGroupCardProps) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const type = useSelector((state: AppState) => state.import.type);
    const loading = useSelector((state: AppState) => state.import.loading);

    const onMaterialReaderLoad = async (event) => {
        const file = JSON.parse(event.target.result);
        if (file && file[0] && file[0].material_id && file[0].material && file[0].undergroup_id) {
            await dispatch(ImportOperations.importMaterial(file));
            history.push("/main/import/merge")
        } else {
            message.error('Неверный формат файла!');
        }
    };
    return (

            <Card style={{height: "100%"}} title="Материалы" bordered={false} extra={
                <FilePicker
                    extensions={['json']}
                    onChange={FileObject => {
                        const reader = new FileReader;
                        reader.readAsText(FileObject);
                        reader.onload = onMaterialReaderLoad;
                    }}>
                    <Button icon={<UploadOutlined/>}>Загрузить</Button>
                </FilePicker>
            }>
                <Spin tip="Загружаем материалы..." spinning={type == "material" && loading}>
                <ReactJson src={materialJson} displayDataTypes={false} enableClipboard={false}/>
                </Spin>
            </Card>

    )
};