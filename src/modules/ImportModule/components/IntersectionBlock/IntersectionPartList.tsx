import React, {useState} from "react";
import {Button, Card, Col, Collapse, Descriptions, Pagination, Row, Typography} from "antd";
import {ExclamationCircleTwoTone} from "@ant-design/icons/lib";
import {useDispatch} from "react-redux";
import {getPartTypeLabel} from "../../../PartsModule/components/PartTypeSelector/PartType.const";
import {IPartSimple} from "../../../PartsModule/part.types";
import {ImportOperations} from "../../store/import.operations";

const {Panel} = Collapse;
const {Title} = Typography;

interface IntersectionPartListProps {
    messages: Array<IImportMessage<IPartSimple>>
}

export const IntersectionPartList = ({messages}: IntersectionPartListProps) => {
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    const [active, setActive] = useState("0");
    const dispatch = useDispatch();
    return (
        <Card>
            <Row gutter={16}>
                <Col span={12}>
                    <Title level={4}>База данных</Title>
                    <Collapse accordion bordered activeKey={[active]} onChange={active => setActive(active? active.toString() : null)}>
                        {messages.map((message, index) => {
                            if (index >= (page - 1) * perPage && index < page * perPage) {
                                return (
                                    <Panel header={message.intersection.name + " " + message.intersection.decimal_num}
                                           key={message.id}
                                           extra={<Button type={"link"}>
                                               <ExclamationCircleTwoTone twoToneColor="#ffa940"/>
                                           </Button>}>
                                        <Descriptions bordered column={1}>
                                            <Descriptions.Item
                                                label="Идентификатор">{message.intersection.part_id}</Descriptions.Item>
                                            <Descriptions.Item
                                                label="Наименование">{message.intersection.name}</Descriptions.Item>
                                            <Descriptions.Item
                                                label="Децимальный номер">{message.intersection.decimal_num}</Descriptions.Item>
                                            <Descriptions.Item label="Тип">{
                                                <span>
                                            {getPartTypeLabel(message.intersection.type_id, {paddingRight: "16px"})}
                                        </span>
                                            }</Descriptions.Item>
                                        </Descriptions>
                                    </Panel>
                                );
                            }
                        })}
                    </Collapse>
                </Col>
                <Col span={12}>
                    <Title level={4}>Импортируемые данные</Title>
                    <Collapse accordion bordered activeKey={[active]} onChange={active => setActive(active? active.toString() : null)}>
                        {messages.map((message, index) => {
                            if (index >= (page - 1) * perPage && index < page * perPage) {
                                return (
                                    <Panel header={message.item.name + " " + message.item.decimal_num}
                                           key={message.id}
                                           extra={<Button type={"link"}
                                                          onClick={() => {
                                                              setActive("0");
                                                              dispatch(ImportOperations.updatePart(message))
                                                          }}
                                           >Заменить</Button>}>
                                        <Descriptions bordered column={1}>
                                            <Descriptions.Item
                                                label="Идентификатор">{message.item.part_id}</Descriptions.Item>
                                            <Descriptions.Item
                                                label="Наименование">{message.item.name}</Descriptions.Item>
                                            <Descriptions.Item
                                                label="Децимальный номер">{message.item.decimal_num}</Descriptions.Item>
                                            <Descriptions.Item label="Тип">{
                                                <span>
                            {getPartTypeLabel(message.item.type_id, {paddingRight: "16px"})}
                            </span>
                                            }</Descriptions.Item>
                                        </Descriptions>
                                    </Panel>
                                );
                            }
                        })}
                    </Collapse>
                </Col>
            </Row>
            <Pagination style={{marginTop: 30}} total={messages.length || 0} current={page} pageSize={perPage}
                        onChange={(current, perPage) => {
                            setPerPage(perPage);
                            setPage(current)
                        }}/>
        </Card>
    )
};