import React from "react";
import {IntersectionGroupList} from "./IntersectionGroupList";
import {IntersectionUndergroupList} from "./IntersectionUndergroupList";
import {IntersectionMaterialList} from "./IntersectionMaterialList";
import {IntersectionPartList} from "./IntersectionPartList";
import {IntersectionPartMaterialsList} from "./IntersectionPartMaterialsList";
import {IntersectionPartStructuresList} from "./IntersectionPartStructuresList";

interface IIntersectionBlockProps {
    messages: Array<IImportMessage<any>>
    type: string
}

export const IntersectionBlock = ({messages, type}: IIntersectionBlockProps) => {
    switch (type) {
        case 'group': return (<IntersectionGroupList messages={messages}/>);
        case 'undergroup': return (<IntersectionUndergroupList messages={messages}/>);
        case 'material': return (<IntersectionMaterialList messages={messages}/>);
        case 'part': return (<IntersectionPartList messages={messages}/>);
        case 'part_materials': return (<IntersectionPartMaterialsList messages={messages}/> );
        case 'part_structures': return  (<IntersectionPartStructuresList messages={messages}/>)
    }
};