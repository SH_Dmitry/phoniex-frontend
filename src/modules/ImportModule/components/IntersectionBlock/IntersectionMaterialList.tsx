import React, {useState} from "react";
import {Button, Card, Col, Collapse, Descriptions, Pagination, Row, Typography} from "antd";
import {ExclamationCircleTwoTone} from "@ant-design/icons/lib";
import {IMaterial} from "../../../MaterialsModule/materials.types";
import {MaterialLabel} from "../../../MaterialsModule/components/MaterialLabel";
import {ImportOperations} from "../../store/import.operations";
import {useDispatch} from "react-redux";

const {Panel} = Collapse;
const {Title} = Typography;

interface IntersectionMaterialListProps {
    messages: Array<IImportMessage<IMaterial>>
}

export const IntersectionMaterialList = ({messages}: IntersectionMaterialListProps) => {
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    const [active, setActive] = useState("0");
    const dispatch = useDispatch();
    return (
        <Card>
            <Row gutter={16}>
                <Col span={12}>
                    <Title level={4}>База данных</Title>
                    <Collapse accordion bordered activeKey={active} onChange={active => setActive(active.toString())}>
                        {messages.map((message, index) => {
                            if (index >= (page - 1) * perPage && index < page * perPage) {
                                return (
                                    <Panel header={<MaterialLabel material={message.item}/>}
                                           key={message.intersection.material_id}
                                           extra={<Button type={"link"}>
                                               <ExclamationCircleTwoTone twoToneColor="#ffa940"/>
                                           </Button>}>
                                        <Descriptions bordered column={1}>
                                            <Descriptions.Item
                                                label="Идентификатор">{message.intersection.material_id}</Descriptions.Item>
                                            <Descriptions.Item label="Наименование">{<MaterialLabel
                                                material={message.intersection}/>}</Descriptions.Item>
                                            <Descriptions.Item
                                                label="Подгруппа">{message.intersection.undergroup_id || "Не найдена"}</Descriptions.Item>
                                        </Descriptions>
                                    </Panel>
                                );

                            }
                        })}
                    </Collapse>
                </Col>
                <Col span={12}>
                    <Title level={4}>Импортируемые данные</Title>
                    <Collapse accordion bordered activeKey={active} onChange={active => setActive(active.toString())}>
                        {messages.map((message, index) => {
                            if (index >= (page - 1) * perPage && index < page * perPage) {
                                return (
                                    <Panel header={<MaterialLabel material={message.item}/>}
                                           key={message.item.material_id}
                                           extra={<Button type={"link"}
                                                          onClick={() => {
                                                              setActive("0");
                                                              dispatch(ImportOperations.updateMaterial(message))
                                                          }}
                                           >Заменить</Button>}>
                                        <Descriptions bordered column={1}>
                                            <Descriptions.Item
                                                label="Идентификатор">{message.item.material_id}</Descriptions.Item>
                                            <Descriptions.Item label="Наименование">{<MaterialLabel
                                                material={message.item}/>}</Descriptions.Item>
                                            <Descriptions.Item
                                                label="Подгруппа">{message.item.undergroup_id || "Не найдена"}</Descriptions.Item>
                                        </Descriptions>
                                    </Panel>
                                );
                            }
                        })}
                    </Collapse>
                </Col>
            </Row>
            <Pagination style={{marginTop: 30}} total={messages.length || 0} current={page} pageSize={perPage}
                        onChange={(current, perPage) => {
                            setPerPage(perPage);
                            setPage(current)
                        }}/>
        </Card>
    )
};