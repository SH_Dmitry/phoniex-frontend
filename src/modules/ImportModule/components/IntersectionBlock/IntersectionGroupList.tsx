import React, {useState} from "react";
import {Button, Card, Col, Collapse, Descriptions, Pagination, Row, Typography} from "antd";
import {ExclamationCircleTwoTone} from "@ant-design/icons/lib";
import {IGroup} from "../../../MaterialsModule/materials.types";
import {useDispatch} from "react-redux";
import {ImportOperations} from "../../store/import.operations";

const {Panel} = Collapse;
const {Title} = Typography;

interface IntersectionGroupListProps {
    messages: Array<IImportMessage<IGroup>>
}

export const IntersectionGroupList = ({messages}: IntersectionGroupListProps) => {
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    const [active, setActive] = useState("0");
    const dispatch = useDispatch();
    return (
        <Card>
            <Row gutter={16}>
                <Col span={12}>
                    <Title level={4}>База данных</Title>
                    <Collapse accordion bordered activeKey={active} onChange={active => setActive(active.toString())}>
                        {messages.map((message, index) => {
                            if (index >= (page - 1) * perPage && index < page * perPage) {
                                return (
                                    <Panel header={message.intersection.name} key={message.intersection.group_id}
                                           extra={<Button type={"link"}>
                                               <ExclamationCircleTwoTone twoToneColor="#ffa940"/>
                                           </Button>}>
                                        <Descriptions bordered column={1}>
                                            <Descriptions.Item
                                                label="Идентификатор">{message.intersection.group_id}</Descriptions.Item>
                                            <Descriptions.Item
                                                label="Наименование">{message.intersection.name}</Descriptions.Item>
                                            <Descriptions.Item
                                                label="Порядковый номер">{message.intersection.number || 100}</Descriptions.Item>
                                        </Descriptions>
                                    </Panel>
                                );

                            }
                        })}
                    </Collapse>
                </Col>
                <Col span={12}>
                    <Title level={4}>Импортируемые данные</Title>
                    <Collapse accordion bordered activeKey={active} onChange={active => setActive(active.toString())}>
                        {messages.map((message, index) => {
                            if (index >= (page - 1) * perPage && index < page * perPage) {
                                return (
                                    <Panel header={message.item.name} key={message.item.group_id}
                                           extra={
                                               <Button type={"link"}
                                                       onClick={() => {
                                                           setActive("0");
                                                           dispatch(ImportOperations.updateGroup(message))
                                                       }}
                                               >Заменить</Button>}>
                                        <Descriptions bordered column={1}>
                                            <Descriptions.Item
                                                label="Идентификатор">{message.item.group_id}</Descriptions.Item>
                                            <Descriptions.Item
                                                label="Наименование">{message.item.name}</Descriptions.Item>
                                            <Descriptions.Item
                                                label="Порядковый номер">{message.item.number || 100}</Descriptions.Item>
                                        </Descriptions>
                                    </Panel>
                                );
                            }
                        })}
                    </Collapse>
                </Col>
            </Row>
            <Pagination style={{marginTop: 30}} total={messages.length || 0} current={page} pageSize={perPage}
                        onChange={(current, perPage) => {
                            setPerPage(perPage);
                            setPage(current)
                        }}/>
        </Card>
    )
};