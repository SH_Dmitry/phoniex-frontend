import {ImportActionTypes} from "./import.operations";
import {importConstants} from "./import.constants";

const _ = require('lodash');

const initialState = {
    type: "",
    loading: false,
    error: null,
    addedMsg: [] as Array<IImportMessage<any>>,
    intersectionMsg: [] as Array<IImportMessage<any>>,
    errorMsg: [] as Array<IImportMessage<any>>
};
export type ImportState = typeof initialState

export function ImportReducer(state = initialState, action: ImportActionTypes): ImportState {
    switch (action.type) {
        case importConstants.IMPORT_GROUP:
            return {
                ...state,
                type: "group",
                loading: true
            };
        case
        importConstants.IMPORT_UNDERGROUP:
            return {
                ...state,
                type: "undergroup",
                loading: true
            };
        case
        importConstants.IMPORT_MATERIAL:
            return {
                ...state,
                type: "material",
                loading: true
            };
        case
        importConstants.IMPORT_PART:
            return {
                ...state,
                type: "part",
                loading: true
            };
        case
        importConstants.IMPORT_PART_MATERIALS:
            return {
                ...state,
                type: "part_materials",
                loading: true
            };
        case importConstants.IMPORT_PART_STRUCTURES:
            return {
                ...state,
                type: "part_structures",
                loading: true
            };
        case importConstants.IMPORT_GROUP_FAILURE ||
        importConstants.IMPORT_UNDERGROUP_FAILURE ||
        importConstants.IMPORT_MATERIAL_FAILURE ||
        importConstants.IMPORT_PART_FAILURE ||
        importConstants.IMPORT_PART_MATERIALS_FAILURE ||
        importConstants.IMPORT_PART_STRUCTURES_FAILURE:
            return {
                ...state,
                type: "",
                error: action.error,
                loading: false
            };
        case importConstants.IMPORT_GROUP_SUCCESS:
            return {
                ...state,
                type: "group",
                loading: false,
                error: null,
                addedMsg: action.payload.filter(i => i.status == "added"),
                intersectionMsg: action.payload.filter(i => i.status == "intersection"),
                errorMsg: action.payload.filter(i => i.status == "failure")
            };
        case importConstants.IMPORT_UNDERGROUP_SUCCESS:
            return {
                ...state,
                type: "undergroup",
                loading: false,
                error: null,
                addedMsg: action.payload.filter(i => i.status == "added"),
                intersectionMsg: action.payload.filter(i => i.status == "intersection"),
                errorMsg: action.payload.filter(i => i.status == "failure")
            };
        case importConstants.IMPORT_MATERIAL_SUCCESS:
            return {
                ...state,
                type: "material",
                loading: false,
                error: null,
                addedMsg: action.payload.filter(i => i.status == "added"),
                intersectionMsg: action.payload.filter(i => i.status == "intersection"),
                errorMsg: action.payload.filter(i => i.status == "failure")
            };
        case importConstants.IMPORT_PART_SUCCESS:
            return {
                ...state,
                type: "part",
                loading: false,
                error: null,
                addedMsg: action.payload.filter(i => i.status == "added"),
                intersectionMsg: action.payload.filter(i => i.status == "intersection"),
                errorMsg: action.payload.filter(i => i.status == "failure")
            };
        case importConstants.IMPORT_PART_MATERIALS_SUCCESS:
            return {
                ...state,
                type: "part_materials",
                loading: false,
                error: null,
                addedMsg: action.payload.filter(i => i.status == "added"),
                intersectionMsg: action.payload.filter(i => i.status == "intersection"),
                errorMsg: action.payload.filter(i => i.status == "failure")
            };
        case importConstants.IMPORT_PART_STRUCTURES_SUCCESS:
            return {
                ...state,
                type: "part_structures",
                loading: false,
                error: null,
                addedMsg: action.payload.filter(i => i.status == "added"),
                intersectionMsg: action.payload.filter(i => i.status == "intersection"),
                errorMsg: action.payload.filter(i => i.status == "failure")
            };

        case importConstants.IMPORT_UPDATE_GROUP_SUCCESS:
            return {
                ...state,
                intersectionMsg: _.reject(state.intersectionMsg, {id: action.payload.id}),
                addedMsg: _.concat(state.addedMsg, action.payload)
            };
        case importConstants.IMPORT_UPDATE_UNDERGROUP_SUCCESS:
            return {
                ...state,
                intersectionMsg: _.reject(state.intersectionMsg, {id: action.payload.id}),
                addedMsg: _.concat(state.addedMsg, action.payload)
            };
        case importConstants.IMPORT_UPDATE_MATERIAL_SUCCESS:
            return {
                ...state,
                intersectionMsg: _.reject(state.intersectionMsg, {id: action.payload.id}),
                addedMsg: _.concat(state.addedMsg, action.payload)
            };
        case importConstants.IMPORT_UPDATE_PART_SUCCESS:
            return {
                ...state,
                intersectionMsg: _.reject(state.intersectionMsg, {id: action.payload.id}),
                addedMsg: _.concat(state.addedMsg, action.payload)
            };
        case importConstants.IMPORT_UPDATE_MATERIALS_PART_SUCCESS:
            return {
                ...state,
                intersectionMsg: _.reject(state.intersectionMsg, {id: action.payload.id}),
                addedMsg: _.concat(state.addedMsg, action.payload)
            };
        case importConstants.IMPORT_UPDATE_PART_STRUCTURES_SUCCESS:
            return {
                ...state,
                intersectionMsg: _.reject(state.intersectionMsg, {id: action.payload.id}),
                addedMsg: _.concat(state.addedMsg, action.payload)
            };
        default:
            return state
    }
}