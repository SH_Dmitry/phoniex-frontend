export const importConstants = {
    IMPORT_GROUP: 'import/group/request' as const,
    IMPORT_GROUP_SUCCESS: 'import/group/success' as const,
    IMPORT_GROUP_FAILURE: 'import/group/failure' as const,

    IMPORT_UPDATE_GROUP: 'import/group/update/request' as const,
    IMPORT_UPDATE_GROUP_SUCCESS: 'import/group/update/success' as const,
    IMPORT_UPDATE_GROUP_FAILURE: 'import/group/update/failure' as const,

    IMPORT_UNDERGROUP: 'import/undergroup/request' as const,
    IMPORT_UNDERGROUP_SUCCESS: 'import/undergroup/success' as const,
    IMPORT_UNDERGROUP_FAILURE: 'import/undergroup/failure' as const,

    IMPORT_UPDATE_UNDERGROUP: 'import/undergroup/update/request' as const,
    IMPORT_UPDATE_UNDERGROUP_SUCCESS: 'import/undergroup/update/success' as const,
    IMPORT_UPDATE_UNDERGROUP_FAILURE: 'import/undergroup/update/failure' as const,

    IMPORT_MATERIAL: 'import/material/request' as const,
    IMPORT_MATERIAL_SUCCESS: 'import/material/success' as const,
    IMPORT_MATERIAL_FAILURE: 'import/material/failure' as const,

    IMPORT_UPDATE_MATERIAL: 'import/material/update/request' as const,
    IMPORT_UPDATE_MATERIAL_SUCCESS: 'import/material/update/success' as const,
    IMPORT_UPDATE_MATERIAL_FAILURE: 'import/material/update/failure' as const,

    IMPORT_PART: 'import/part/request' as const,
    IMPORT_PART_SUCCESS: 'import/part/success' as const,
    IMPORT_PART_FAILURE: 'import/part/failure' as const,

    IMPORT_UPDATE_PART: 'import/part/update/request' as const,
    IMPORT_UPDATE_PART_SUCCESS: 'import/part/update/success' as const,
    IMPORT_UPDATE_PART_FAILURE: 'import/part/update/failure' as const,

    IMPORT_PART_MATERIALS: 'import/part/materials/request' as const,
    IMPORT_PART_MATERIALS_SUCCESS: 'import/part/materials/success' as const,
    IMPORT_PART_MATERIALS_FAILURE: 'import/part/materials/failure' as const,

    IMPORT_UPDATE_MATERIALS_PART: 'import/part/materials/update/request' as const,
    IMPORT_UPDATE_MATERIALS_PART_SUCCESS: 'import/part/materials/update/success' as const,
    IMPORT_UPDATE_MATERIALS_PART_FAILURE: 'import/part/materials/update/failure' as const,

    IMPORT_PART_STRUCTURES: 'import/part/structures/request' as const,
    IMPORT_PART_STRUCTURES_SUCCESS: 'import/part/structures/success' as const,
    IMPORT_PART_STRUCTURES_FAILURE: 'import/part/structures/failure' as const,

    IMPORT_UPDATE_PART_STRUCTURES: 'import/part/structures/update/request' as const,
    IMPORT_UPDATE_PART_STRUCTURES_SUCCESS: 'import/part/structures/update/success' as const,
    IMPORT_UPDATE_PART_STRUCTURES_FAILURE: 'import/part/structures/update/failure' as const,
};