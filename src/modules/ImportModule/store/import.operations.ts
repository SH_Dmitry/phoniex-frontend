import {GetActionsType, ThunkActionType} from "../../../store/base.types";
import {message} from "antd";
import {ImportActions} from "./import.actions";
import {Dispatch} from "redux";
import {importService} from "../services/import.service";
import {IGroup, IMaterial, IUndergroup} from "../../MaterialsModule/materials.types";
import {materialService} from "../../MaterialsModule/services/materials.service";
import {IPartSimple, IPartStructures} from "../../PartsModule/part.types";
import {partService} from "../../PartsModule/services/parts.service";
import {partStructureService} from "../../PartsModule/services/structure.service";

export type ImportActionTypes = GetActionsType<typeof ImportActions>
type DispatchType = Dispatch<ImportActionTypes>



export const ImportOperations = {

    //Import operation
    importGroup:(data:string): ThunkActionType => {
        return async (dispatch: DispatchType, getState) => {
            try {
                dispatch(ImportActions.importGroupRequest());
                const result = await importService.importGroup(data);
                dispatch(ImportActions.importGroupSuccess(result));
            }
            catch (error) {
                await dispatch(ImportActions.importGroupFailure(error));
                message.error('Загрузка файла потерпела неудачу')
            }
        }
    },
    importUndergroup:(data:string): ThunkActionType => {
        return async (dispatch: DispatchType, getState) => {
            try {
                dispatch(ImportActions.importUndergroupRequest());
                const result = await importService.importUndergroup(data);
                dispatch(ImportActions.importUndergroupSuccess(result));
            }
            catch (error) {
                await dispatch(ImportActions.importUndergroupFailure(error));
                message.error('Загрузка файла потерпела неудачу')
            }
        }
    },
    importMaterial:(data:string): ThunkActionType => {
        return async (dispatch: DispatchType, getState) => {
            try {
                dispatch(ImportActions.importMaterialRequest());
                const result = await importService.importMaterial(data);
                dispatch(ImportActions.importMaterialSuccess(result));
            }
            catch (error) {
                await dispatch(ImportActions.importMaterialFailure(error));
                message.error('Загрузка файла потерпела неудачу')
            }
        }
    },
    importPart:(data:string): ThunkActionType => {
        return async (dispatch: DispatchType, getState) => {
            try {
                dispatch(ImportActions.importPartRequest());
                const result = await importService.importPart(data);
                dispatch(ImportActions.importPartSuccess(result));
            }
            catch (error) {
                await dispatch(ImportActions.importPartFailure(error));
                message.error('Загрузка файла потерпела неудачу')
            }
        }
    },
    importPartMaterials:(data:string): ThunkActionType => {
        return async (dispatch: DispatchType, getState) => {
            try {
                dispatch(ImportActions.importPartMaterialsRequest());
                const result = await importService.importPartMaterials(data);
                dispatch(ImportActions.importPartMaterialsSuccess(result));
            }
            catch (error) {
                await dispatch(ImportActions.importPartMaterialsFailure(error));
                message.error('Загрузка файла потерпела неудачу')
            }
        }
    },
    importPartStructures:(data:string): ThunkActionType => {
        return async (dispatch: DispatchType, getState) => {
            try {
                dispatch(ImportActions.importPartStructuresRequest());
                const result = await importService.importPartStructures(data);
                dispatch(ImportActions.importPartStructuresSuccess(result));
            }
            catch (error) {
                await dispatch(ImportActions.importPartStructuresFailure(error));
                message.error('Загрузка файла потерпела неудачу')
            }
        }
    },

    //Update operation
    updateGroup: (msg: IImportMessage<IGroup>) => async (dispatch: DispatchType) => {
        materialService.updateGroup(msg.intersection, msg.item)
            .then(() => {
                message.success('Группа успешно обновлена!');
                msg.status='added';
                msg.intersection=null;
                dispatch(ImportActions.updateGroupSuccess(msg));
            })
            .catch(error => {
                message.error('При обнолении группы произошла ошибка!');
                dispatch(ImportActions.updateGroupFailure(error));
            })
    },
    updateUndergroup: (msg: IImportMessage<IUndergroup>) => async (dispatch: DispatchType) => {
        materialService.updateUndergroup(msg.intersection, msg.item)
            .then(() => {
                message.success('Подгруппа успешно обновлена!');
                msg.status='added';
                msg.intersection=null;
                dispatch(ImportActions.updateUndergroupSuccess(msg));
            })
            .catch(error => {
                message.error('При обнолении подгруппы произошла ошибка!');
                dispatch(ImportActions.updateUndergroupFailure(error));
            })
    },
    updateMaterial: (msg: IImportMessage<IMaterial>) => async (dispatch: DispatchType) => {
        materialService.updateMaterial(msg.intersection, msg.item)
            .then(() => {
                message.success('Материал успешно обновлен!');
                msg.status='added';
                msg.intersection=null;
                dispatch(ImportActions.updateMaterialSuccess(msg));
            })
            .catch(error => {
                message.error('При обнолении материала произошла ошибка!');
                dispatch(ImportActions.updateMaterialFailure(error));
            })
    },
    updatePart: (msg: IImportMessage<IPartSimple>) => async (dispatch: DispatchType) => {
        partService.updatePart(msg.intersection, msg.item)
            .then(() => {
                message.success('ДСЕ успешно обновлена!');
                msg.status='added';
                msg.item.part_id = msg.intersection.part_id;
                msg.intersection=null;
                dispatch(ImportActions.updatePartSuccess(msg));
            })
            .catch(error => {
                message.error('При обнолении ДСЕ произошла ошибка!');
                dispatch(ImportActions.updatePartFailure(error));
            })
    },
    updatePartStructure: (msg: IImportMessage<IPartStructures>) => async (dispatch: DispatchType) => {

        partStructureService.replacePartStructure(msg.intersection.decimal_num, msg.item.structures)
            .then(() => {
                message.success('Структура ДСЕ успешно обновлена!');
                msg.status='added';
                msg.intersection=null;
                dispatch(ImportActions.updatePartStructuresSuccess(msg));
            })
            .catch(error => {
                message.error('При обнолении структуры ДСЕ произошла ошибка!');
                dispatch(ImportActions.updatePartStructuresFailure(error));
            })
    },
};