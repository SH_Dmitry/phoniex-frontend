import {importConstants} from "./import.constants";
import {IGroup, IMaterial, IUndergroup} from "../../MaterialsModule/materials.types";
import {IPartSimple, IPartStructures} from "../../PartsModule/part.types";
import {IPartMaterials} from "../../../../../backend/src/domains/part/part.materials/types";

export const ImportActions = {
    //Action Request
    importGroupRequest: () => ({
        type: importConstants.IMPORT_GROUP,
    } as const),
    importUndergroupRequest: () => ({
        type: importConstants.IMPORT_UNDERGROUP,
    } as const),
    importMaterialRequest: () => ({
        type: importConstants.IMPORT_MATERIAL,
    } as const),
    importPartRequest: () => ({
        type: importConstants.IMPORT_PART,
    } as const),
    importPartMaterialsRequest: () => ({
        type: importConstants.IMPORT_PART_MATERIALS,
    } as const),
    importPartStructuresRequest: () => ({
        type: importConstants.IMPORT_PART_STRUCTURES,
    } as const),

    //Action Success import
    importGroupSuccess: (payload: Array<IImportMessage<IGroup>>) => ({
        type: importConstants.IMPORT_GROUP_SUCCESS,
        payload
    } as const),
    importUndergroupSuccess: (payload: Array<IImportMessage<IUndergroup>>) => ({
        type: importConstants.IMPORT_UNDERGROUP_SUCCESS,
        payload
    } as const),
    importMaterialSuccess: (payload: Array<IImportMessage<IMaterial>>) => ({
        type: importConstants.IMPORT_MATERIAL_SUCCESS,
        payload
    } as const),
    importPartSuccess: (payload: Array<IImportMessage<IPartSimple>>) => ({
        type: importConstants.IMPORT_PART_SUCCESS,
        payload
    } as const),
    importPartMaterialsSuccess: (payload: Array<IImportMessage<IPartMaterials>>) => ({
        type: importConstants.IMPORT_PART_MATERIALS_SUCCESS,
        payload
    } as const),
    importPartStructuresSuccess: (payload: Array<IImportMessage<IPartStructures>>) => ({
        type: importConstants.IMPORT_PART_STRUCTURES_SUCCESS,
        payload
    } as const),

    //Action Success update
    updateGroupSuccess: (payload: IImportMessage<IGroup>) => ({
        type: importConstants.IMPORT_UPDATE_GROUP_SUCCESS,
        payload
    } as const),
    updateUndergroupSuccess: (payload: IImportMessage<IUndergroup>) => ({
        type: importConstants.IMPORT_UPDATE_UNDERGROUP_SUCCESS,
        payload
    } as const),
    updateMaterialSuccess: (payload: IImportMessage<IMaterial>) => ({
        type: importConstants.IMPORT_UPDATE_MATERIAL_SUCCESS,
        payload
    } as const),
    updatePartSuccess: (payload: IImportMessage<IPartSimple>) => ({
        type: importConstants.IMPORT_UPDATE_PART_SUCCESS,
        payload
    } as const),
    updatePartMaterialsSuccess: (payload: IImportMessage<IPartMaterials>) => ({
        type: importConstants.IMPORT_UPDATE_MATERIALS_PART_SUCCESS,
        payload
    } as const),
    updatePartStructuresSuccess: (payload: IImportMessage<IPartStructures>) => ({
        type: importConstants.IMPORT_UPDATE_PART_STRUCTURES_SUCCESS,
        payload
    } as const),


    //Action Failure import
    importGroupFailure: (error: Error) => ({
        type: importConstants.IMPORT_GROUP_FAILURE,
        payload: error,
        error: true
    } as const),
    importUndergroupFailure: (error: Error) => ({
        type: importConstants.IMPORT_UNDERGROUP_FAILURE,
        payload: error,
        error: true
    } as const),
    importMaterialFailure: (error: Error) => ({
        type: importConstants.IMPORT_MATERIAL_FAILURE,
        payload: error,
        error: true
    } as const),
    importPartFailure: (error: Error) => ({
        type: importConstants.IMPORT_PART_FAILURE,
        payload: error,
        error: true
    } as const),
    importPartMaterialsFailure: (error: Error) => ({
        type: importConstants.IMPORT_PART_MATERIALS_FAILURE,
        payload: error,
        error: true
    } as const),
    importPartStructuresFailure: (error: Error) => ({
        type: importConstants.IMPORT_PART_STRUCTURES_FAILURE,
        payload: error,
        error: true
    } as const),

    //Action Failure update
    updateGroupFailure: (error: Error) => ({
        type: importConstants.IMPORT_UPDATE_GROUP_FAILURE,
        payload: error,
        error: true
    } as const),
    updateUndergroupFailure: (error: Error) => ({
        type: importConstants.IMPORT_UPDATE_UNDERGROUP_FAILURE,
        payload: error,
        error: true
    } as const),
    updateMaterialFailure: (error: Error) => ({
        type: importConstants.IMPORT_UPDATE_MATERIAL_FAILURE,
        payload: error,
        error: true
    } as const),
    updatePartFailure: (error: Error) => ({
        type: importConstants.IMPORT_UPDATE_PART_FAILURE,
        payload: error,
        error: true
    } as const),
    updatePartMaterialsFailure: (error: Error) => ({
        type: importConstants.IMPORT_UPDATE_MATERIALS_PART_FAILURE,
        payload: error,
        error: true
    } as const),
    updatePartStructuresFailure: (error: Error) => ({
        type: importConstants.IMPORT_UPDATE_PART_STRUCTURES_FAILURE,
        payload: error,
        error: true
    } as const),
};



