import {IUser} from "..";
import {apiWrapper} from "../../../tools/api/api.wrapper";

export async function login(username: string, password: string): Promise<IUser> {
    const requestOptions: RequestInit = {
        method: 'POST',
        headers: {'content-type': 'application/json'},
        body: JSON.stringify({username, password})
    };
    const response = await apiWrapper<IUser>('/user/signin', requestOptions);
    localStorage.setItem('user', JSON.stringify(response));
    return response;
}

export function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}


