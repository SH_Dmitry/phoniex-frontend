import React from "react";
import {Button, Form, Input} from "antd";

const layout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 5 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
const buttonLayout = {
    wrapperCol: { span: 16, offset: 5},
};

export const SignInForm = (props) => {
    const onFinish = values => {
        props.onSubmit(values);
    };

    return (
        <Form
            {...layout}
            name="basic"
            onFinish={onFinish}
        >
            <Form.Item
                label="Логин"
                name="username"
                rules={[{ required: true, message: 'Пожалуйста, введите логин!' }]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label="Пароль"
                name="password"
                rules={[{ required: true, message: 'Пожалуйста, введите пароль!' }]}
            >
                <Input.Password />
            </Form.Item>

            <Form.Item {...buttonLayout}>
                <Button type="primary" htmlType="submit" block>
                    Войти
                </Button>
            </Form.Item>
        </Form>
    );
};