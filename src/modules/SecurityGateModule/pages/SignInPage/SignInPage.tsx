import React, {useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import logo from "../../../../assets/logo.png";
import {AuthThunks} from "../../store/auth/auth.actions";
import {SignInForm} from "../../";
import {Card, Col, Divider, Row, Typography} from "antd";

import styles from './SignInPage.module.less'
import {AppState} from "../../../../store/app.reducer";


const {Title} = Typography;

export const SignInPage = () => {
    const history = useHistory();
    const loggedIn = useSelector((state: AppState) => state.security.auth.loggedIn);
    const dispatch = useDispatch();
    useEffect(() => {
        if (loggedIn) {
            history.push("/main/partsDb")
        }
    }, []);

    const handleSubmit = (values) => {
        if (values.username && values.password) {
            dispatch(AuthThunks.login(values.username, values.password))
        }
    };

    return (
        <div className={styles.root}>
            <Card >
                <Row className={styles.header}>
                    <Col>
                        <img className={styles.logo}  src={logo} alt="Logo"/>
                    </Col>
                    <Col className = {styles.title}>
                        <Title>PHONIEX</Title>
                        <Title level ={3}>Автоматизированная система технологической подготовки производства</Title>
                    </Col>
                </Row>
                <Divider/>
                <div className={styles.content}>
                    <SignInForm onSubmit={handleSubmit}/>
                </div>
            </Card>
        </div>
    );
};