import {schema} from 'normalizr'

//Base scheme
export const user = new schema.Entity('users', {}, {idAttribute: 'user_id'});


export interface IResUser {
    user_id: string
    personal_data: string
    state: string
}

export interface IUser {
    user_id: string
    username: string
    personal_data: string
    state: string
    token: string
    role: string
    image?: any
}

export interface IPublicUser {
    user_id: string
    personal_data: string
    state: string
    image?: any
}