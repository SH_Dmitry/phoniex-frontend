import {combineReducers} from "redux";
import {authReducer} from "./auth/auth.reducer";

export const securityReducer = combineReducers({
    auth:authReducer
});
