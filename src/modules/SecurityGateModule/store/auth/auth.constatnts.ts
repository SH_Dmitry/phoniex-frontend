export const authenticationConstants = {
    LOGIN_REQUEST: 'LOGIN_REQUEST' as const,
    LOGIN_SUCCESS: 'LOGIN_SUCCESS' as const,
    LOGIN_FAILURE: 'LOGIN_FAILURE' as const,

    LOGOUT: 'USERS_LOGOUT' as const,
};