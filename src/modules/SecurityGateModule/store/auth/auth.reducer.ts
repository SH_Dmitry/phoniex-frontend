import {authenticationConstants} from "./auth.constatnts";
import {AuthActionTypes} from "./auth.actions";
import {IUser} from "../users.types";

let user: IUser | null = JSON.parse(localStorage.getItem('user'));

const initialState = {
        loggedIn: !!user as boolean,
        user
    };

export type AuthenticationState = typeof initialState
export const authReducer =(state = initialState, action:AuthActionTypes):AuthenticationState => {
    switch (action.type) {
        case authenticationConstants.LOGIN_SUCCESS:
            return { ...state, loggedIn: true, user: action.payload
            };
        case authenticationConstants.LOGOUT:
            return { ...state, loggedIn: false, user: null};
        default:
            return state
    }
};
