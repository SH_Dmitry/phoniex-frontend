import history from '../../../../tools/history.helper';
import {authenticationConstants} from "./auth.constatnts";
import {GetActionsType, ThunkActionType} from "../../../../store/base.types";
import {Dispatch} from "redux";
import {login, logout} from "../../services/authentication.service";
import {IUser} from "../users.types";

export type AuthActionTypes = GetActionsType<typeof Actions | typeof  AuthActions>
type DispatchType = Dispatch<AuthActionTypes>

const Actions = {
    loginSuccess: (user: IUser) => ({type: authenticationConstants.LOGIN_SUCCESS, payload: user}),
    loginFailure: (error: Error) => ({type: authenticationConstants.LOGIN_FAILURE, payload: error, error: true})
};

export const AuthActions = {
    logout: () => {
        logout();
        history.push('/');
        return {type: authenticationConstants.LOGOUT};
    }
};

export const AuthThunks = {
    login(username: string, password: string): ThunkActionType {
        return async (dispatch: DispatchType) => {
            login(username, password)
                .then(user => {
                    dispatch(Actions.loginSuccess(user));
                })
                .then(() => {
                    history.push('/main')
                })
                .catch(error => {
                    dispatch(Actions.loginFailure(error));
                })
        };
    },

};


