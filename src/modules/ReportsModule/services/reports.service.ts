import {apiFileWrapper} from "../../../tools/api/api.wrapper";
import {bearerSecurity} from "../../SecurityGateModule/services/bearer.security";


export const reportService = {
    async getMaterialReport({part_id,name, decimal_num}) {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify({name, decimal_num})
        };
        await apiFileWrapper(`/report/material/${part_id}`, requestOptions, "Materials_Report_for_" + part_id +".doc");
    }
};