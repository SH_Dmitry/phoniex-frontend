import React, {useState} from "react";
import {Button, Card, Col, PageHeader, Row} from "antd";
import {ApartmentOutlined, ClockCircleOutlined, SettingOutlined, SketchOutlined} from "@ant-design/icons/lib";
import {ReportMaterialWizard} from "..";

export const ReportsPage = () => {
    const [materialVisibility, setMaterialVisibility] = useState(false);
    return (
        <>
            <PageHeader title={"Документы"}/>
            <Row gutter={[16, 16]}>
                <Col span={12}>
                    <Card style={{height: "100%"}} title={'Технико-нормировочная карта'}
                          actions={[
                              <Button type={"link"} onClick={() => {
                              }}>Сформировать</Button>
                          ]}
                    >
                        <p>
                            Технико-нормировочная карта (ТНК) — документ, разрабатываемый совместно с другими
                            технологическими
                            документами к операции и содержащий расчетные данные по элементам нормы времени (выработки)
                            и
                            описание
                            выполняемых приемов.
                        </p>
                        <span>
                            Необходимо: <ApartmentOutlined/> <SettingOutlined/> <ClockCircleOutlined/>
                        </span>
                    </Card>
                </Col>
                <Col span={12}>
                    <Card style={{height: "100%"}} title={'Сводная ведомость материалов'}
                          actions={[
                              <Button type={"link"} onClick={() => {
                                  setMaterialVisibility(true);
                              }}>Сформировать</Button>
                          ]}
                    >
                        <p>
                            Сводная ведомость материалов - это список материалов или запчастей, необходимых для
                            производства, сборки или ремонта конечного продукта,
                            с указанием количества по каждому пункту. Ведомость материалов - компактная форма
                            представления
                            материалов, необходимых
                            для создания конечного продукта.
                        </p>
                        <span color={"red"}>
                            Необходимо: <ApartmentOutlined/> <SketchOutlined/>
                        </span>
                    </Card>
                </Col>
            </Row>
            <PageHeader title={"Информационные отчеты"}/>
            <Row gutter={[16, 16]}>
                <Col span={12}>
                    <Card style={{height: "100%"}} title={'Список заимствований'}
                          actions={[
                              <Button type={"link"} onClick={() => {
                              }}>Показать</Button>
                          ]}>
                        Подготовит список сборочных едениц содержащих выбранную ДСЕ
                    </Card>
                </Col>
                <Col span={12}>
                    <Card style={{height: "100%"}} title={'Список использования материалов'}
                          actions={[
                              <Button type={"link"} onClick={() => {
                              }}>Показать</Button>
                          ]}>
                        Подготовит список сборочных едениц и ДСЕ содержащих выбранный материал
                    </Card>
                </Col>
            </Row>
                <ReportMaterialWizard visibility={materialVisibility} setVisibility={setMaterialVisibility}/>
        </>
    );
};
