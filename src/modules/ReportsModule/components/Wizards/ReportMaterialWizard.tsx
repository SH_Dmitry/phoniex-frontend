import React, {useState} from "react";
import {Card, Col, Descriptions, Input, message, Modal, Row, Steps, Typography} from "antd";
import {CloudDownloadOutlined, ControlOutlined, LoadingOutlined, TableOutlined} from "@ant-design/icons/lib";
import {reportService} from "../../services/reports.service";
import {useSelector} from "react-redux";
import {AppState} from "../../../../store/app.reducer";
import {PartList} from "../../../PartsModule/components/lists/PartList/PartList";
import {IPart} from "../../../PartsModule/part.types";
import {PartsFilterDrawer} from "../../../PartsModule/components/drawers/PartsFilterDrawer/PartsFilterDrawer";

const {Step} = Steps;


export const ReportMaterialWizard = ({visibility, setVisibility}) => {
    const [current, setCurrent] = useState(0);
    const [part, setPart] = useState(null as IPart);
    const [name, setName] = useState("");
    const [decimal, setDecimal] = useState("");
    const [load, setLoad] = useState(false);

    const parts = useSelector((state: AppState) => state.parts.parts);
    const page_size = useSelector((state: AppState) => state.parts.pagination.page_size);
    const page = useSelector((state: AppState) => state.parts.pagination.page);
    const part_count = useSelector((state: AppState) => state.parts.part_count);

    const handelClick = (value:IPart)=>
    {
        setPart(value);
        setName(value.name);
        setDecimal(value.decimal_num);
    };
    const steps = [
        {
            icon:<TableOutlined/>,
            title: 'Выбор ДСЕ',
            content: <Card title={"База деталей и сборочных единиц"} style={{marginTop:30}} extra={<PartsFilterDrawer/>}>
                <PartList
                    parts={parts}
                    part_count={part_count}
                    page_size={page_size}
                    page={page}
                    handelClick={handelClick}
                    selectedPart={part}
                />
            </Card>
        },
        {
            icon: <ControlOutlined/>,
            title: 'Параметры',
            content: <div style={{margin: 30}}>
                <Row gutter={[24, 24]}>
                    <Col span={8}>
                        <Typography>Наименование</Typography>
                    </Col>
                    <Col span={16}>
                        <Input value={name} onChange={e => setName(e.currentTarget.value)} title={"Наименование"}/>
                    </Col>
                </Row>
                <Row gutter={[24, 24]}>
                    <Col span={8}>
                        <Typography>Децимальный номер</Typography>
                    </Col>
                    <Col span={16}>
                        <Input value={decimal} onChange={e => setDecimal(e.currentTarget.value)}
                               title={"Децимальный номер"}/>
                    </Col>
                </Row>
            </div>
        },
        {
            icon: <CloudDownloadOutlined/>,
            title: 'Загрузка',
            content: <div style={{margin: 30}}>
                <Descriptions column={1}>
                    <Descriptions.Item label="Наименование">{name}</Descriptions.Item>
                    <Descriptions.Item label="Децимальный номер">{decimal}</Descriptions.Item>
                </Descriptions>
            </div>,
        },
    ];
    return (
        <Modal visible={visibility}
               title={"Создать сводную ведомость материалов"}
               width={720}
               onCancel={
                   () => {
                       setVisibility(false);
                       setName("");
                       setDecimal("");
                       setPart(null);
                       setCurrent(0)
                   }
               }
               okText={current < steps.length - 1 ? "Далее" : load ? <LoadingOutlined/> :
                   <span><CloudDownloadOutlined/> Скачать сводную ведомость </span>}
               okButtonProps={{disabled: !part}}
               onOk={() => {
                   if (current < steps.length - 1) {
                       setCurrent(current + 1)
                   } else {
                       setLoad(true);
                       reportService.getMaterialReport({part_id: part.part_id, name: name, decimal_num: decimal}).then(() => {
                           message.info("Загрузка началась");
                           setLoad(false);
                           setCurrent(0);
                           setVisibility(false)
                       })
                   }
               }}
               cancelText={"Отмена"}>
            <Steps current={current}>
                {steps.map(item => (
                    <Step icon={item.icon} key={item.title} title={item.title}/>
                ))}
            </Steps>
            <div>{steps[current].content}</div>
        </Modal>
    )
};