import React from "react";

import {useDispatch, useSelector} from "react-redux";
import {Button, Card, Col, Row, Typography} from "antd";
import {MaterialTable} from "../../container/MaterialTable";
import styles from "./MaterialDB.module.less"
import {PlusOutlined} from "@ant-design/icons/lib";
import {MaterialGroupsSelector} from "../../components/selectors/MaterialGroupsSelector";
import {MaterialUndergroupsSelector} from "../../components/selectors/MaterialUndergroupsSelector";
import {IMaterial} from "../../materials.types";
import {MaterialOperations} from "../../store/materials.operations";
import {AppState} from "../../../../store/app.reducer";
import {ModuleFormDialog} from "../../../../components/dialogs/ModuleFormDialog";
import {MaterialFormField} from "../../../../components/forms/FormFields";

const {Text} = Typography;


export const MaterialDB = () => {
    const materialState = useSelector((state: AppState) => state.materials);
    const dispatch = useDispatch();

    //Add new material dialog visibility
    const [visibility, setVisibility] = React.useState(false);
    const showDialog = () => {
        setVisibility(true);
    };
    const hideDialog = () => {
        setVisibility(false);
    };

    //Material operations
    const addMaterial = (material: IMaterial, undergroup_id: string) => {
        dispatch(MaterialOperations.addMaterial(material, undergroup_id));
        hideDialog();
    };
    const updateMaterial = (oldMaterial: IMaterial, newMaterial: IMaterial) => {
        dispatch(MaterialOperations.updateMaterial(oldMaterial, newMaterial))
    };
    const deleteMaterial = (material: IMaterial) => {
        dispatch(MaterialOperations.deleteMaterial(material.material_id))
    };


    return (
        <Card title="База материалов"
              extra={<Button onClick={showDialog}>Добавить материал</Button>}>
            <Row className={styles.row}>
                <Col span={2}>
                    <Text>Категория</Text>
                </Col>
                <Col span={6}>
                    <MaterialGroupsSelector/>
                </Col>
                <Col span={1}>
                    <Button style={{width: "100%", marginLeft: 32}}><PlusOutlined/></Button>
                </Col>
            </Row>
            <Row className={styles.row}>
                <Col span={2}>
                    <Text>Подкатегория</Text>
                </Col>
                <Col span={6}>
                    <MaterialUndergroupsSelector/>
                </Col>
                <Col span={1}>
                    <Button style={{width: "100%", marginLeft: 32}}><PlusOutlined/></Button>
                </Col>
            </Row>
            <MaterialTable
                data={materialState.materials}
                onDelete={deleteMaterial}
                onUpdate={updateMaterial}/>

            {/*Modal dialog*/}
            <ModuleFormDialog
                name={"edit"}
                title={"Создать новый материал"}
                submitText={"Добавить"}
                visible={visibility}
                fields={MaterialFormField}
                handleClose={() => setVisibility(false)}
                handleSubmit={(value) => {
                    addMaterial(value, materialState.selected_undergroup);
                    setVisibility(false)
                }}/>
        </Card>
)
};