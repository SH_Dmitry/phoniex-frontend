import React from "react";
import {Button, Popconfirm, Table} from "antd";
import {ColumnType} from "antd/es/table";

import {FormInstance} from "antd/lib/form";
import {IMaterial} from "../../materials.types";
import {MaterialLabel} from "../../components/MaterialLabel";
import {INObjects} from "../../../../store/base.types";
import {MaterialFormField} from "../../../../components/forms/FormFields";
import {ModuleFormDialog} from "../../../../components/dialogs/ModuleFormDialog";

const _ = require('lodash');

type MaterialTableProp = {
    data: INObjects<IMaterial>
    onDelete: (material: IMaterial) => void
    onUpdate: (oldMaterial: IMaterial, newMaterial: IMaterial) => void
}

export function MaterialTable({data, onDelete, onUpdate}: MaterialTableProp) {
    const [DialogVisibility, setDialogVisibility] = React.useState(false);
    const [editItem, setEditItem] = React.useState(null);
    const formRef = React.createRef<FormInstance>();

    const columns: Array<ColumnType<IMaterial>> = [
        {
            title: 'Полное наименование',
            dataIndex: 'search_name',
            key: 'search_name',
            render: (text, record) => (
                <MaterialLabel material={record}/>
            )
        },
        {
            title: 'Материал',
            dataIndex: 'material',
            key: 'material',
        },
        {
            title: 'Сортамент',
            dataIndex: 'sortament',
            key: 'sortament',
        },
        {
            title: 'Форма',
            dataIndex: 'forma',
            key: 'forma',
        },
        {
            title: 'Действия',
            key: 'action',
            render: (text, record) => (
                <span>
                    <Button onClick={() => {
                        setEditItem(record);
                        setDialogVisibility(true);
                        formRef.current.setFieldsValue(record);
                    }}>Изменить</Button>
                    <Popconfirm
                        title="Вы действительно хотите удалить?"
                        onConfirm={() => onDelete(record)}
                        okText="Да"
                        cancelText="Нет"
                        placement="bottomRight">
                        <Button style={{marginLeft: 8}}>Удалить</Button>
                    </Popconfirm>
                </span>
            )
        }
    ];
    return (
        <>
            <Table
                columns={columns}
                dataSource={_.toArray(data.byId)}
            />

            {/*Modal dialog*/}
            <ModuleFormDialog
                name={"edit_material"}
                title={"Редактировать материал"}
                submitText={"Сохранить"}
                visible={DialogVisibility}
                fields={MaterialFormField}
                formRef={formRef}
                handleClose={() => setDialogVisibility(false)}
                handleSubmit={(value) => {
                    onUpdate(editItem, value);
                    setDialogVisibility(false)
                }}/>
        </>
    )
}