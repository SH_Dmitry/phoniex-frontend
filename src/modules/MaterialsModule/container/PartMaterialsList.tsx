import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {DragItem} from "../../../store/base.types";
import {useDrop} from "react-dnd"
import {List} from "antd";
import {DropArea} from "../../../components/dumb/DropArea/DropArea";
import {PartMaterialItem_Editable} from "../components/PartMaterialItem_Editable";
import {IMaterialDropType, IPartMaterial} from "../materials.types";
import {PartMaterialsOperations} from "../../PartsModule/store/part.materials/materials.operations";
import {AppState} from "../../../store/app.reducer";

const _ = require('lodash');


interface PartMaterials_ListProps {
    part_id: string
}

export const PartMaterialsList = ({part_id}: PartMaterials_ListProps) => {
    const dispatch = useDispatch();
    const structure = useSelector((state: AppState) => state.part_info.materials.materials);
    useEffect(() => {
        dispatch(PartMaterialsOperations.getPartMaterials(part_id))
    }, [part_id]);
    const [{canDrop, isOver}, drop] = useDrop({
        accept: IMaterialDropType,
        drop(result: DragItem<IPartMaterial>) {
            dispatch(PartMaterialsOperations.addPartMaterial(part_id, result.item.material_id))
        },
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        }),
    });

    return (
        <div ref={drop}>
            {canDrop ?
                <DropArea canDrop={canDrop} isOver={isOver}/>
                :
                    <List
                        itemLayout="vertical"
                        pagination={{pageSize: 7}}
                        dataSource={_.values(structure.byId)}
                        renderItem={(item: IPartMaterial) => <PartMaterialItem_Editable material={item} part_id={part_id}/>}
                    >
                    </List>
            }
        </div>
    )
};


