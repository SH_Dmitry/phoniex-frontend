import React from "react";
import {useSelector} from "react-redux";
import {List} from "antd";
import {MaterialItemDraggable} from "../components/MaterialItemDraggable";
import {IMaterial} from "../materials.types";
import {AppState} from "../../../store/app.reducer";

const _ = require('lodash');

export const MaterialListDraggable = () => {
    const materials = useSelector((state: AppState) => state.materials.materials);
    return (
        <>
            <List
                pagination={{
                    pageSize: 6,
                }}
                dataSource={_.toArray(materials.byId)}
                renderItem={(material: IMaterial) => (
                    <MaterialItemDraggable material={material}/>
                )}/>
        </>
    )
};