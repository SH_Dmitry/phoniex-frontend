import React from "react";
import {Col, Collapse, Descriptions, List, Row} from "antd";
import {IPartMaterial} from "../materials.types";
import Text from "antd/es/typography/Text";
import {MaterialLabel} from "./MaterialLabel";
import {getUnitById} from "../../../tools/converters";

const _ = require('lodash');

const {Panel} = Collapse;

interface PartMaterialItemProps {
    material: IPartMaterial
}

interface PartMaterialItemHeaderProps {
    material: IPartMaterial
}

export const PartMaterialItemHeader = ({material}: PartMaterialItemHeaderProps) => {
    return (
        <>
            <Row justify={'space-between'}>
                <Col span={16}>
                    <MaterialLabel material={material}/>
                </Col>
                <Col style={{display: "flex", alignItems: 'center'}}>
                    <Text>{"Количество: " + material.count + " " + getUnitById(material.unit_id)}</Text>
                </Col>
            </Row>

        </>
    )
};

export const PartMaterialItem = ({ material}: PartMaterialItemProps) => {
    return (
        <List.Item
            style={{width:"100%"}}
            key={material.material_id}>
            <Collapse style={{width:"100%"}}>
                    <Panel style={{width:"100%"}} header={<PartMaterialItemHeader material={material}/>}
                           key={material.material_id}>
                        <Descriptions size={"middle"} bordered>
                            <Descriptions.Item
                                label={<Text>Количество</Text>}>
                                <Text>{material.count + " " + getUnitById(material.unit_id)}</Text>
                            </Descriptions.Item>
                            <Descriptions.Item
                                label={<Text>Количество по КД</Text>}
                                span={2}>
                                <Text>{(material.kd_count ? material.kd_count : "0") + " кг"}</Text>
                            </Descriptions.Item>
                            <Descriptions.Item
                                label={<Text>Размер</Text>}
                                span={3}>
                                <Text>{material.sizes}</Text>
                            </Descriptions.Item>
                            <Descriptions.Item
                                label={<Text>Комментарий</Text>}>
                                <Text>{material.comment}</Text>
                            </Descriptions.Item>
                        </Descriptions>
                    </Panel>
            </Collapse>
        </List.Item>
    )
};