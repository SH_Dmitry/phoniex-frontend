import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {Select} from "antd";
import {MaterialOperations} from "../../store/materials.operations";
import {AppState} from "../../../../store/app.reducer";

import styles from "./MaterialGroupsSelector.module.less";

export const MaterialUndergroupsSelector = (props: any) => {
    const undergroups = useSelector((state: AppState) => state.materials.undergroups);
    const selected_undergroup = useSelector((state: AppState) => state.materials.selected_undergroup);
    const dispatch = useDispatch();
    const handleChange = (value) => {
        dispatch(MaterialOperations.changeUndergroup(value))
    };
    return (
        <>
            <Select
                className={styles.root}
                size="middle"
                value={selected_undergroup}
                onChange={(value) => handleChange(value)}
                {...props}>
                {undergroups.allIds.map((undergroup_id) => (
                        <Select.Option key={undergroup_id} value={undergroup_id}>
                            <div style={{display: "flex", alignItems: "center"}}>
                                {undergroups.byId[undergroup_id].name}
                            </div>
                        </Select.Option>
                    )
                )}
            </Select>
        </>
    )
};