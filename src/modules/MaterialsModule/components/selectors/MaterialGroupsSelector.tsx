import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Select} from "antd";
import {MaterialOperations} from "../../store/materials.operations";
import {AppState} from "../../../../store/app.reducer";

import styles from "./MaterialGroupsSelector.module.less";

export const MaterialGroupsSelector = (props: any) => {
    const groups = useSelector((state: AppState) => state.materials.groups);
    const selected_group = useSelector((state: AppState) => state.materials.selected_group);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(MaterialOperations.getGroups())
    }, []);
    const handleChange = (value) => {
        dispatch(MaterialOperations.changeGroup(value))
    };
    return (
        <>
            <Select
                className={styles.root}
                size="middle"
                value={selected_group}
                onChange={(value) => handleChange(value)}
                {...props}>
                {groups.allIds.map((group_id) => (
                        <Select.Option key={group_id} value={group_id}>
                            <div style={{display: "flex", alignItems: "center"}}>
                                {groups.byId[group_id].name}
                            </div>
                        </Select.Option>
                    )
                )}
            </Select>
        </>
    )
};