import React from "react";
import {Col, Divider, Row, Typography} from "antd";

const {Text} = Typography;

const styles = {
    divider: {
        padding: 0,
        margin: 0,
    },
    type: {
        display: 'flex',
        justifyContent: "center",
        alignItems: "center",
        marginRight: "16px"
    }
};

interface MaterialLabelProps {
    material: {
        sortament?: string,
        forma?: string,
        material: string
    }
}


export const MaterialLabel = ({material}: MaterialLabelProps) => {
    return (
        <div style={{display: "inline-block"}}>
            {material.sortament ? (
                    <Row style={{}}>
                        <Col style={{display: "flex", alignItems: "center", paddingRight: 10}}>
                            <Text>{material.forma}</Text>
                        </Col>
                        <Col>
                            <Text>{material.sortament}</Text>
                            <Divider style={styles.divider}/>
                            <Text>{material.material}</Text>
                        </Col>
                    </Row>) : (material.forma ? material.forma + " " + material.material : material.material)}
        </div>
    )
};

