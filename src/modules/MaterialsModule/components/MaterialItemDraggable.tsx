import React from "react";
import {useDrag} from "react-dnd";
import {List} from "antd";
import {IMaterial, IMaterialDropType} from "../materials.types";
import {MaterialLabel} from "./MaterialLabel";

interface Material_DragProps {
    material: IMaterial
}

export const MaterialItemDraggable = ({material}: Material_DragProps) => {
    const [, drag] = useDrag({
        item: {item: material, type: IMaterialDropType},
    });
    return (
        <>
            <div ref={drag}>
                <List.Item key={material.material_id}>
                    <MaterialLabel material={material}/>
                </List.Item>
                <div/>
            </div>
        </>
)
};