import React, {useState} from "react";
import {Col, Collapse, Descriptions, Form, Input, List, Row, Select} from "antd";
import {IPartMaterial} from "../materials.types";
import TextArea from "antd/es/input/TextArea";
import Text from "antd/es/typography/Text";
import {MaterialLabel} from "./MaterialLabel";
import {useDispatch} from "react-redux";
import {useForm} from "antd/es/form/Form";
import {PartMaterialsOperations} from "../../PartsModule/store/part.materials/materials.operations";
import {getUnitById} from "../../../tools/converters";

const _ = require('lodash');

const {Panel} = Collapse;
const {Option} = Select;

interface PartMaterial_ItemProps {
    part_id: string
    material: IPartMaterial
}

const selectAfter = (
    <Form.Item name="unit_id" style={{margin: 0}}>
        <Select className="select-after">
            <Option value={1} >шт</Option>
            <Option value={2}>кг</Option>
            <Option value={3}>г</Option>
            <Option value={4}>м</Option>
            <Option value={5}>см</Option>
            <Option value={6}>кв.м</Option>
            <Option value={7}>кв.см</Option>
        </Select>
    </Form.Item>
);

interface PartMaterialItemHeaderProps {
    material: IPartMaterial
    isEdit: boolean,
    onUpdate: (event)=>void
    onEdit: (event)=>void
    onDelete: (event)=>void
}

export const PartMaterialItemHeader = ({material,isEdit, onEdit, onUpdate,onDelete}: PartMaterialItemHeaderProps) => {
    return (
        <>
            <Row justify={'space-between'}>
                <Col span={16}>
                    <MaterialLabel material={material}/>
                </Col>
                <Col style={{display: "flex", alignItems: 'center'}}>
                    <Text>{"Количество: " + material.count + " " + getUnitById(material.unit_id)}</Text>
                </Col>
                {isEdit?
                    <Col style={{display: "flex", alignItems: 'center'}}>
                        <a onClick={onUpdate}>
                            Сохранить
                        </a>
                        <a style={{paddingLeft: 8}} onClick={onEdit}>
                            Отмена
                        </a>
                    </Col>:
                    <Col style={{display: "flex", alignItems: 'center'}}>
                        <a onClick={onEdit}>
                            Изменить
                        </a>
                        <a style={{paddingLeft: 8}} onClick={onDelete}>
                            Удалить
                        </a>
                    </Col>}
            </Row>

        </>
    )
};

export const PartMaterialItem_Editable = ({part_id, material}: PartMaterial_ItemProps) => {
    const [form] = useForm();
    const [isEditing, setEditing] = useState(false);
    const dispatch = useDispatch();

    const handleEdit = (event) => {
        setEditing(!isEditing);
        event.stopPropagation();
    };

    const handleUpdate = (event) => {
        setEditing(false);
        const newData: IPartMaterial = _.assignIn({...material}, form.getFieldsValue(['comment', 'kd_count', "sizes", "count", "unit_id"]));
        dispatch(PartMaterialsOperations.updatePartMaterial(part_id, newData));
        event.stopPropagation();
    };

    const handleDelete = (event) => {
        setEditing(false);
        dispatch(PartMaterialsOperations.deletePartMaterial(part_id,material.material_id));
        event.stopPropagation();
    };

    return (
        <List.Item
            key={material.material_id}
        >
            <Collapse>
                {isEditing ?
                    <Panel header={<PartMaterialItemHeader material={material} isEdit={isEditing} onEdit={handleEdit}
                                                       onUpdate={handleUpdate} onDelete={handleDelete}/>}
                           key={material.material_id}>
                        <Form form={form} initialValues={material}>
                            <Descriptions size={"middle"} bordered>
                                <Descriptions.Item
                                    label={<Text>Количество</Text>}>
                                    <Form.Item name="count" style={{margin: 0}}>
                                        <Input placeholder={"Количество"} addonAfter={selectAfter}/>
                                    </Form.Item>
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={<Text>Количество по КД</Text>}
                                    span={2}>
                                    <Form.Item name="kd_count" style={{margin: 0}}>
                                        <Input placeholder={"Количество по КД"} addonAfter={"кг"}/>
                                    </Form.Item>
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={<Text>Размер</Text>}
                                    span={3}>
                                    <Form.Item name="sizes" style={{margin: 0}}>
                                        <Input placeholder={"Размер"}/>
                                    </Form.Item>
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={<Text>Комментарий</Text>}>
                                    <Form.Item name="comment" style={{margin: 0}}>
                                        <TextArea placeholder={"Комментарий"}/>
                                    </Form.Item>
                                </Descriptions.Item>
                            </Descriptions>
                        </Form>
                    </Panel>
                    :
                    <Panel header={<PartMaterialItemHeader material={material} isEdit={isEditing} onEdit={handleEdit}
                                                       onUpdate={handleUpdate} onDelete={handleDelete}/>}
                           key={material.material_id}>
                        <Descriptions size={"middle"} bordered>
                            <Descriptions.Item
                                label={<Text>Количество</Text>}>
                                <Text>{material.count + " " + getUnitById(material.unit_id)}</Text>
                            </Descriptions.Item>
                            <Descriptions.Item
                                label={<Text>Количество по КД</Text>}
                                span={2}>
                                <Text>{(material.kd_count ? material.kd_count : "0") + " кг"}</Text>
                            </Descriptions.Item>
                            <Descriptions.Item
                                label={<Text>Размер</Text>}
                                span={3}>
                                <Text>{material.sizes}</Text>
                            </Descriptions.Item>
                            <Descriptions.Item
                                label={<Text>Комментарий</Text>}>
                                <Text>{material.comment}</Text>
                            </Descriptions.Item>
                        </Descriptions>
                    </Panel>}
            </Collapse>
        </List.Item>
    )
};