import {materialConstants} from "./materials.constants";
import {INGroup, INMaterial, INUndergroup} from "../materials.types";


export const MaterialActions = {
    //Material actions success
    getMaterialsByUndergroupSuccess: (payload: INMaterial) => ({
        type: materialConstants.GET_MATERIALS_SUCCESS,
        payload
    } as const),
    addMaterialSuccess: (payload: INMaterial) => ({
        type: materialConstants.ADD_MATERIAL_SUCCESS,
        payload
    } as const),
    updateMaterialSuccess: (payload: INMaterial) => ({
        type: materialConstants.UPDATE_MATERIAL_SUCCESS,
        payload
    } as const),
    deleteMaterialSuccess: (payload: string) => ({
        type: materialConstants.DELETE_MATERIAL_SUCCESS,
        payload
    } as const),

    //Material actions failure
    getMaterialsByUndergroupFailure: (error: Error) => ({
        type: materialConstants.GET_MATERIALS_FAILURE,
        payload: error,
        error: true
    } as const),
    addMaterialFailure: (error: Error) => ({
        type: materialConstants.ADD_MATERIAL_FAILURE,
        payload: error,
        error: true
    } as const),
    updateMaterialFailure: (error: Error) => ({
        type: materialConstants.UPDATE_MATERIAL_FAILURE,
        payload: error,
        error: true
    } as const),
    deleteMaterialFailure: (error: Error) => ({
        type: materialConstants.DELETE_MATERIAL_FAILURE,
        payload: error,
        error: true
    } as const),

    //Groups
    //Success
    getGroupsSuccess: (payload: INGroup) => ({
        type: materialConstants.GET_GROUPS_SUCCESS,
        payload
    } as const),
    addGroupSuccess: (payload: INGroup) => ({
        type: materialConstants.ADD_GROUP_SUCCESS,
        payload
    } as const),
    updateGroupSuccess: (payload: INGroup) => ({
        type: materialConstants.UPDATE_GROUP_SUCCESS,
        payload
    } as const),
    deleteGroupSuccess: (payload: string) => ({
        type: materialConstants.DELETE_GROUP_SUCCESS,
        payload
    } as const),
    //Failure
    getGroupsFailure: (error: Error) => ({
        type: materialConstants.GET_GROUPS_FAILURE,
        payload: error,
        error: true
    } as const),
    addGroupFailure: (error: Error) => ({
        type: materialConstants.ADD_GROUP_FAILURE,
        payload: error,
        error: true
    } as const),
    updateGroupFailure: (error: Error) => ({
        type: materialConstants.UPDATE_GROUP_FAILURE,
        payload: error,
        error: true
    } as const),
    deleteGroupFailure: (error: Error) => ({
        type: materialConstants.DELETE_GROUP_FAILURE,
        payload: error,
        error: true
    } as const),
    changeGroup: (group_id: string) => ({
        type: materialConstants.CHANGE_GROUPS,
        payload: {group_id}
    } as const),

    //Undergroups
    //Success
    getUndergroupsSuccess: (payload: INUndergroup) => ({
        type: materialConstants.GET_UNDERGROUPS_SUCCESS,
        payload
    } as const),
    addUndergroupSuccess: (payload: INUndergroup) => ({
        type: materialConstants.ADD_UNDERGROUP_SUCCESS,
        payload
    } as const),
    updateUndergroupSuccess: (payload: INUndergroup) => ({
        type: materialConstants.UPDATE_UNDERGROUP_SUCCESS,
        payload
    } as const),
    deleteUndergroupSuccess: (payload: string) => ({
        type: materialConstants.DELETE_UNDERGROUP_SUCCESS,
        payload
    } as const),
    //Failure
    getUndergroupsFailure: (error: Error) => ({
        type: materialConstants.GET_UNDERGROUPS_FAILURE,
        payload: error,
        error: true
    } as const),
    addUndergroupFailure: (error: Error) => ({
        type: materialConstants.ADD_UNDERGROUP_FAILURE,
        payload: error,
        error: true
    } as const),
    updateUndergroupFailure: (error: Error) => ({
        type: materialConstants.UPDATE_UNDERGROUP_FAILURE,
        payload: error,
        error: true
    } as const),
    deleteUndergroupFailure: (error: Error) => ({
        type: materialConstants.DELETE_UNDERGROUP_FAILURE,
        payload: error,
        error: true
    } as const),
    changeUndergroup: (undergroup_id: string) => ({
        type: materialConstants.CHANGE_UNDERGROUPS,
        payload: {undergroup_id}
    } as const)
};

