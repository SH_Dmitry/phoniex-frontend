export const materialConstants = {
    GET_MATERIALS: 'material/get/request' as const,
    GET_MATERIALS_SUCCESS: 'material/get/success' as const,
    GET_MATERIALS_FAILURE: 'material/get/failure' as const,

    ADD_MATERIAL: 'material/add/request',
    ADD_MATERIAL_SUCCESS: 'material/add/success' as const,
    ADD_MATERIAL_FAILURE: 'material/add/failure' as const,

    UPDATE_MATERIAL: 'material/update/request',
    UPDATE_MATERIAL_SUCCESS: 'material/update/success' as const,
    UPDATE_MATERIAL_FAILURE: 'material/update/failure' as const,

    DELETE_MATERIAL: 'material/delete/request',
    DELETE_MATERIAL_SUCCESS: 'material/delete/success' as const,
    DELETE_MATERIAL_FAILURE: 'material/delete/failure' as const,

    GET_UNDERGROUPS: 'material/undergroups/get/request'  as const,
    GET_UNDERGROUPS_SUCCESS: 'material/undergroups/get/success'  as const,
    GET_UNDERGROUPS_FAILURE: 'material/undergroups/get/failure'  as const,

    ADD_UNDERGROUP: 'material/undergroup/add/request',
    ADD_UNDERGROUP_SUCCESS: 'material/undergroup/add/success' as const,
    ADD_UNDERGROUP_FAILURE: 'material/undergroup/add/failure' as const,

    UPDATE_UNDERGROUP: 'material/undergroup/update/request',
    UPDATE_UNDERGROUP_SUCCESS: 'material/undergroup/update/success' as const,
    UPDATE_UNDERGROUP_FAILURE: 'material/undergroup/update/failure' as const,

    DELETE_UNDERGROUP: 'material/undergroup/delete/request',
    DELETE_UNDERGROUP_SUCCESS: 'material/undergroup/delete/success' as const,
    DELETE_UNDERGROUP_FAILURE: 'material/undergroup/delete/failure' as const,

    CHANGE_UNDERGROUPS: 'material/undergroups/change/request'  as const,

    GET_GROUPS: 'material/groups/get/request'  as const,
    GET_GROUPS_SUCCESS: 'material/groups/get/success'  as const,
    GET_GROUPS_FAILURE: 'material/groups/get/failure'  as const,

    ADD_GROUP: 'material/group/add/request',
    ADD_GROUP_SUCCESS: 'material/group/add/success' as const,
    ADD_GROUP_FAILURE: 'material/group/add/failure' as const,

    UPDATE_GROUP: 'material/group/update/request',
    UPDATE_GROUP_SUCCESS: 'material/group/update/success' as const,
    UPDATE_GROUP_FAILURE: 'material/group/update/failure' as const,

    DELETE_GROUP: 'material/group/delete/request',
    DELETE_GROUP_SUCCESS: 'material/group/delete/success' as const,
    DELETE_GROUP_FAILURE: 'material/group/delete/failure' as const,

    CHANGE_GROUPS: 'material/groups/change/request'  as const,
};