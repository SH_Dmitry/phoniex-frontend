import {IGroup, IMaterial, IUndergroup} from "../materials.types";
import {message} from "antd";
import {GetActionsType, ThunkActionType} from "../../../store/base.types";
import {Dispatch} from "redux";
import {MaterialActions} from "./materials.actions";
import {materialService} from "../services/materials.service";

//Action/Dispatch selectors
export type MaterialsActionTypes = GetActionsType<typeof MaterialActions>
type DispatchType = Dispatch<MaterialsActionTypes>

//Helpers
const MaterialHelpers = {
    getMaterialHelper: (dispatch: DispatchType, undergroup_id: string) => {
        materialService.getMaterials(undergroup_id)
            .then(materials => {
                dispatch(MaterialActions.getMaterialsByUndergroupSuccess(materials));
            })
            .catch(error => {
                dispatch(MaterialActions.getMaterialsByUndergroupFailure(error));
            })
    },
    getUnderGroupHelper: (dispatch: DispatchType, group_id: string) => {
        materialService.getUndergroups(group_id)
            .then(undergroups => {
                dispatch(MaterialActions.getUndergroupsSuccess(undergroups));
                MaterialHelpers.getMaterialHelper(dispatch, undergroups.result[0])
            })
            .catch(error => {
                dispatch(MaterialActions.getUndergroupsFailure(error));
            })
    }
};

export const MaterialOperations = {
    changeGroup: (group_id: string) => async (dispatch: DispatchType) => {
        dispatch(MaterialActions.changeGroup(group_id));
        MaterialHelpers.getUnderGroupHelper(dispatch, group_id)
    },
    changeUndergroup: (undergroup_id: string) => async (dispatch: DispatchType) => {
        dispatch(MaterialActions.changeUndergroup(undergroup_id));
        MaterialHelpers.getMaterialHelper(dispatch, undergroup_id)
    },

    addMaterial: (material: IMaterial, undergroup_id: string) => async (dispatch: DispatchType) => {
        materialService.addMaterial(material, undergroup_id)
            .then((material) => {
                message.success('Материал успешно добавлен!');
                dispatch(MaterialActions.addMaterialSuccess(material));
            })
            .catch(error => {
                message.error('При добавлении материала произошла ошибка!');
                dispatch(MaterialActions.addMaterialFailure(error));
            })
    },
    deleteMaterial: (material_id: string) => async (dispatch: DispatchType) => {
        materialService.deleteMaterial(material_id)
            .then(() => {
                message.success('Материал успешно удален!');
                dispatch(MaterialActions.deleteMaterialSuccess(material_id));
            })
            .catch(error => {
                message.error('При удалении материала произошла ошибка!');
                dispatch(MaterialActions.deleteMaterialFailure(error));
            })
    },
    updateMaterial: (oldMaterial: IMaterial, newMaterial: IMaterial) => async (dispatch: DispatchType) => {
        materialService.updateMaterial(oldMaterial, newMaterial)
            .then((material) => {
                message.success('Материал успешно обновлен!');
                dispatch(MaterialActions.updateMaterialSuccess(material));
            })
            .catch(error => {
                message.error('При обнолении материала произошла ошибка!');
                dispatch(MaterialActions.updateMaterialFailure(error));
            })
    },
    getGroups: (): ThunkActionType => async (dispatch: DispatchType) => {
        materialService.getGroups()
            .then(groups => {
                dispatch(MaterialActions.getGroupsSuccess(groups));
                MaterialHelpers.getUnderGroupHelper(dispatch, groups.result[0])
            })
            .catch(error => {
                dispatch(MaterialActions.getGroupsFailure(error));
            })
    },

    updateGroup: (oldItem: IGroup, newItem: IGroup) => async (dispatch: DispatchType) => {
        materialService.updateGroup(oldItem, newItem)
            .then((group) => {
                message.success('Группа успешно обновлена!');
                dispatch(MaterialActions.updateGroupSuccess(group));
            })
            .catch(error => {
                message.error('При обнолении группы произошла ошибка!');
                dispatch(MaterialActions.updateGroupFailure(error));
            })
    },

    getUnderGroups: (group_id: string): ThunkActionType => async (dispatch: DispatchType) => {
        MaterialHelpers.getUnderGroupHelper(dispatch, group_id);
    },

    updateUndergroup: (oldItem: IUndergroup, newItem: IUndergroup) => async (dispatch: DispatchType) => {
        materialService.updateUndergroup(oldItem, newItem)
            .then((undergroup) => {
                message.success('Подгруппа успешно обновлена!');
                dispatch(MaterialActions.updateUndergroupSuccess(undergroup));
            })
            .catch(error => {
                message.error('При обнолении подгруппы произошла ошибка!');
                dispatch(MaterialActions.updateUndergroupFailure(error));
            })
    },

    getMaterials: (undergroup_id: string): ThunkActionType => async (dispatch: DispatchType) => {
        MaterialHelpers.getMaterialHelper(dispatch, undergroup_id)
    },

};
