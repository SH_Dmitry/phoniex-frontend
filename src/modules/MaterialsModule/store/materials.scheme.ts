import {schema} from "normalizr";

export const group = new schema.Entity("groups", {}, {idAttribute: 'group_id'});
export const undergroup = new schema.Entity("undergroups", {}, {idAttribute:'undergroup_id'});
export const material = new  schema.Entity("materials", {}, {idAttribute: 'material_id'});
export const partMaterial = new  schema.Entity("materials", {}, {idAttribute: 'material_id'});
export const groupScheme = [group];
export const undergroupScheme = [undergroup];
export const materialScheme = [material];
export const partMaterialScheme = [partMaterial];