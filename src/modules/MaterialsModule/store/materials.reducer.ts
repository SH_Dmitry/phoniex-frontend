import {materialConstants} from "./materials.constants";
import {IGroup, IMaterial, IUndergroup} from "../materials.types";
import {MaterialsActionTypes} from "./materials.operations";
import {emptyNObject, INObjects} from "../../../store/base.types";

const _ = require('lodash');

const initialState = {
    materials: emptyNObject as INObjects<IMaterial>,
    selected_material: "1",
    groups: emptyNObject as INObjects<IGroup>,
    selected_group: "1",
    undergroups: emptyNObject as INObjects<IUndergroup>,
    selected_undergroup: '1'
};

export type MaterialState = typeof initialState

export function materialsReducer(state = initialState, action: MaterialsActionTypes): MaterialState {
    switch (action.type) {
        case materialConstants.GET_MATERIALS_SUCCESS:
            return {
                ...state,
                materials:
                    {
                        byId: action.payload.entities.materials,
                        allIds: action.payload.result
                    }
            };
        case  materialConstants.ADD_MATERIAL_SUCCESS:
            const materials = action.payload.entities.materials;
            return {
                ...state,
                materials: {
                    byId: _.assign(state.materials.byId, materials),
                    allIds: _.union(state.materials.allIds, Object.getOwnPropertyNames(materials))
                }
            };
        case  materialConstants.DELETE_MATERIAL_SUCCESS:
            return {
                ...state,
                materials:
                    {
                        byId: _.omit(state.materials.byId, [action.payload]),
                        allIds: _.filter(state.materials.allIds, {materials_id: action.payload})
                    }
            };
        case  materialConstants.UPDATE_MATERIAL_SUCCESS:
            const material_id = action.payload.result[0];
            const material = action.payload.entities.materials[material_id];
            return {
                ...state,
                materials:
                    {
                        byId: {
                            ...state.materials.byId,
                            [material_id]: material
                        },
                        allIds: _.union(state.materials.allIds, action.payload.result)
                    }
            };
        case materialConstants.GET_GROUPS_SUCCESS:
            return {
                ...state,
                groups: {
                    byId:action.payload.entities.groups,
                    allIds: action.payload.result
                },
                selected_group: action.payload.result[0]
            };
        case  materialConstants.UPDATE_GROUP_SUCCESS:
            const group_id = action.payload.result[0];
            const group = action.payload.entities.groups[group_id];
            return {
                ...state,
                groups:
                    {
                        byId: {
                            ...state.groups.byId,
                            [group_id]: group
                        },
                        allIds: _.union(state.groups.allIds, action.payload.result)
                    }
            };
        case  materialConstants.CHANGE_GROUPS:
            return {
                ...state,
                selected_group: action.payload.group_id
            };

        case materialConstants.GET_UNDERGROUPS_SUCCESS:
            return {
                ...state,
                undergroups: {
                    byId: action.payload.entities.undergroups,
                    allIds: action.payload.result
                },
                selected_undergroup: action.payload.result[0]
            };
        case  materialConstants.UPDATE_UNDERGROUP_SUCCESS:
            const undergroup_id = action.payload.result[0];
            const undergroup = action.payload.entities.undergroups[undergroup_id];
            return {
                ...state,
                undergroups:
                    {
                        byId: {
                            ...state.undergroups.byId,
                            [undergroup_id]: undergroup
                        },
                        allIds: _.union(state.undergroups.allIds, action.payload.result)
                    }
            };
        case  materialConstants.CHANGE_UNDERGROUPS:
            return {
                ...state,
                selected_undergroup: action.payload.undergroup_id
            };
        default:
            return state
    }
}
