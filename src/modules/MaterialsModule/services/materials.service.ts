import {
    IGroup,
    IMaterial,
    INGroup,
    INMaterial,
    INPartMaterial,
    INUndergroup,
    IPartMaterial,
    IUndergroup
} from "../materials.types";
import {normalize} from "normalizr";
import {groupScheme, materialScheme, partMaterialScheme, undergroupScheme} from "../store/materials.scheme";
import {apiWrapper} from "../../../tools/api/api.wrapper";
import {bearerSecurity} from "../../SecurityGateModule/services/bearer.security";

export const materialService = {

    async getMaterials(undergroup_id: string): Promise<INMaterial> {
        const requestOptions = {
            method: 'GET',
            headers: {'Authorization': bearerSecurity()}
        };
        const response = await apiWrapper<Array<IMaterial>>(`/material/${undergroup_id}`, requestOptions);
        return normalize(response, materialScheme);
    },

    async addMaterial(material, undergroup_id: string): Promise<INMaterial> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify({
                sortament: material.sortament ? material.sortament : null,
                material: material.material,
                forma: material.forma ? material.sortament :null,
                undergroup_id
            })
        };
        const response = await apiWrapper<IMaterial>(`/material`, requestOptions);
        return normalize([response], materialScheme);
    },

    async deleteMaterial(material_id: string): Promise<void> {
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            }
        };
        await apiWrapper<void>(`/materials/${material_id}`, requestOptions)
    },

    async updateMaterial(oldMaterial: IMaterial, newMaterial: IMaterial): Promise<INMaterial> {
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify({
                sortament: newMaterial.sortament,
                material: newMaterial.material ? newMaterial.material: oldMaterial.material,
                forma: newMaterial.forma
            })
        };
        const response = await apiWrapper<IMaterial>(`/material/${oldMaterial.material_id}`, requestOptions);
        return normalize([response], materialScheme)
    },

    async getGroups(): Promise<INGroup> {
        const requestOptions: RequestInit = {
            method: 'GET',
            headers: {'Authorization': bearerSecurity()}
        };
        const response = await apiWrapper<Array<IGroup>>("/material/group", requestOptions);
        return normalize(response, groupScheme);
    },
    async addGroup(group: IGroup): Promise<INGroup> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify({
                name: group.name,
                number: group.number,
            })
        };
        const response = await apiWrapper<IGroup>(`/material/group`, requestOptions);
        return normalize([response], groupScheme);
    },

    async deleteGroup(group_id: string): Promise<void> {
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            }
        };
        await apiWrapper<void>(`/material/group/${group_id}`, requestOptions)
    },
    async updateGroup(oldItem: IGroup, newItem: IGroup): Promise<INGroup> {
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify({
                name: newItem.name ? newItem.name: oldItem.name,
                number: newItem.number
            })
        };
        const response = await apiWrapper<IGroup>(`/material/group/${oldItem.group_id}`, requestOptions);
        console.log(response);
        return normalize([response], groupScheme)
    },

    async getUndergroups(group_id: string): Promise<INUndergroup> {
        const requestOptions: RequestInit = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()},
        };
        const response = await apiWrapper<Array<IUndergroup>>(`/material/group/${group_id}/undergroup`, requestOptions);
        return normalize(response, undergroupScheme);
    },

    async addUndergroup(undergroup: IUndergroup): Promise<INUndergroup> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify({
                name: undergroup.name,
                number: undergroup.number,
                group_id: undergroup.group_id
            })
        };
        const response = await apiWrapper<IUndergroup>(`/material/undergroup`, requestOptions);
        return normalize([response], undergroupScheme);
    },

    async deleteUndergroup(undergroup_id: string): Promise<void> {
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            }
        };
        await apiWrapper<void>(`/material/undergroup/${undergroup_id}`, requestOptions)
    },

    async updateUndergroup(oldItem: IUndergroup, newItem: IUndergroup): Promise<INUndergroup> {
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()
            },
            body: JSON.stringify({
                undergroup_id: oldItem.undergroup_id,
                name: newItem.name ? newItem.name: oldItem.name,
                number: newItem.number,
                group_id: newItem.group_id
            })
        };
        const response = await apiWrapper<IUndergroup>(`/material/undergroup/${oldItem.undergroup_id}`, requestOptions);
        return normalize([response], undergroupScheme)
    },
};

export const partMaterialService = {
    async getMaterials(part_id: string): Promise<INPartMaterial> {
        const requestOptions = {
            method: 'GET',
            headers: {'Authorization': bearerSecurity()}
        };
        const response = await apiWrapper<Array<IMaterial>>(`/part/${part_id}/material`, requestOptions);
        return normalize(response,partMaterialScheme);
    },
    async addMaterial(part_id: string, material_id: string): Promise<INPartMaterial> {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()},
            body: JSON.stringify({
                material_id,
            })
        };
        const response = await apiWrapper<Array<IMaterial>>(`/part/${part_id}/material`, requestOptions);
        return normalize(response,partMaterialScheme);
    },
    async updateMaterial(part_id: string, material: IPartMaterial): Promise<INPartMaterial> {
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()},
            body: JSON.stringify(material)
        };
        const response = await apiWrapper<Array<IMaterial>>(`/part/${part_id}/material`, requestOptions);
        return normalize(response,partMaterialScheme);
    },
    async deleteMaterial(part_id: string, material_id: string): Promise<void> {
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerSecurity()},
            body: JSON.stringify({material_id})
        };
        await apiWrapper<Array<IMaterial>>(`/part/${part_id}/material`, requestOptions);
    }
};

