import {INData} from "../../store/base.types";


//Drop selectors
export const  IMaterialDropType = "material";

//Base selectors
export type IPartMaterial = {
    material_id: string,
    sortament?: string,
    material: string,
    forma?: string,
    count: number
    kd_count: string,
    sizes?: string,
    comment?: string,
    unit_id: string
    undergroup_id:string
}

export type IPartMaterialSimple = {
    material_id: string,
    count: number
    kd_count: string,
    sizes?: string,
    comment?: string,
    unit_id: string
}

export type IPartMaterialsSimple = {
    decimal_num: string,
    materials: Array<IPartMaterialSimple>
}

export type IPartMaterials= {
    part_id: string,
    name: string,
    decimal_num: string,
    materials: Array<IPartMaterial>
}


export interface IMaterial  {
    material_id: string
    material: string
    forma?: string
    sortament?: string
    undergroup_id: string
}
export interface IGroup {
    group_id: string
    name: string
    number: number
}
export interface IUndergroup {
    undergroup_id: string
    name: string
    group_id: string
    number: number
}

//Server response selectors


//Normalizable selectors
export interface INMaterial {
    result: Array<string>
    entities: {
        materials: INData<IMaterial>
    }
}

export interface INGroup {
    result: Array<string>
    entities: {
        groups: INData<IGroup>
    }
}

export interface INUndergroup {
    result: Array<string>
    entities: {
        undergroups: INData<IUndergroup>
    }
}

export interface INPartMaterial {
    result: Array<string>
    entities: {
        materials: INData<IPartMaterial>
    }
}