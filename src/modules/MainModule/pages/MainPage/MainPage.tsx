import React, {useEffect} from "react";
import {Route} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import {Navigator} from "../../components/Navigator";
import {PartDB, PartInfo} from "../../../PartsModule";
import {MaterialDB} from "../../../MaterialsModule";

import {ReportMaterialWizard, ReportsPage} from "../../../ReportsModule";

import {AuthActions} from "../../../SecurityGateModule/store/auth/auth.actions";
import {Layout} from "antd";

import styles from './MainPage.module.less'
import {AppBar} from "../../components/AppBar";
import {PartOperations} from "../../../PartsModule/store/parts.operations";
import {AppState} from "../../../../store/app.reducer";


import {ImportMain} from "../../../ImportModule";

const { Header, Sider, Content } = Layout;

export const MainPage = () => {
    const user = useSelector((state:AppState) => state.security.auth.user);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(PartOperations.getParts())
    }, []);
    return (
        <Layout className={styles.root}>
            <Header className={styles.header}>
                <AppBar user={user} logout={()=>dispatch(AuthActions.logout())}/>
            </Header>
            <Layout>
                <Sider className={styles.sider}>
                    <Navigator/>
                </Sider>
                <Content className={styles.content}>
                    <Route exact path='/main/partsDb' component={PartDB}/>
                    <Route exact path='/main/materialDb' component={MaterialDB}/>
                    <Route exact path='/main/part/:part_id' component={PartInfo} />
                    <Route exact path='/main/reports' component={ReportsPage} />
                    <Route exact path='/main/reports/material' component={ReportMaterialWizard} />
                    <Route path='/main/import' component={ImportMain} />
                </Content>
            </Layout>
        </Layout>
    )
};
