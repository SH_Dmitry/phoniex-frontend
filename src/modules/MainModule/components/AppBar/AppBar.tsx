import React from "react";
import icon from "../../../../assets/icons/user.png"
import styles from './AppBar.module.less'
import {Button, Typography} from "antd";
import {IUser} from "../../../SecurityGateModule";

const { Text} = Typography;

export interface AppBarProps {
    user: IUser
    logout: () => void
}

export const AppBar = ({user, logout}: AppBarProps) => {
    return (
        <div className={styles.root}>
                    <img className={styles.image} src={icon} alt="icon"/>
                    <div className={styles.title}>
                        <Text>{user.personal_data}</Text>
                        <Text>{user.state}</Text>
                    </div>
                    <Button onClick={logout}>Logout</Button>
        </div>
    );
};