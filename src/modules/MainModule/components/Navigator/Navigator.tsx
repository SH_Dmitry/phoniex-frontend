import React from "react";
import {useHistory} from "react-router-dom";
import {Divider, Menu} from "antd";
import styles from './Navigator.module.less'


export const Navigator = () => {
    const history = useHistory();
    const handleClick = (event: any) => {
        switch (event.key) {
            case "0":
                history.push("/main/projects");
                break;
            case "1":
                history.push("/main/partsDb");
                break;
            case "2":
                history.push("/main/");
                break;
            case "3":
                history.push("/main/materialDb");
                break;
            case "4":
                history.push("/main/reports");
                break;
            case "7":
                history.push("/main/import/menu");
                break;
            default:
                history.push("/main")
        }

    };
    return (
            <Menu
                onClick={handleClick}
                defaultSelectedKeys={['1']}
                className={styles.root}
            >
                    <Menu.Item key="0" disabled>Изделия</Menu.Item>
                    <Menu.Item key="1">База ДСЕ</Menu.Item>
                    <Menu.Item key="2" disabled>Технология</Menu.Item>
                    <Menu.Item key="3">Материалы</Menu.Item>
                    <Menu.Item key="4">Отчеты</Menu.Item>
                    <Menu.Item key="5" disabled>Документация</Menu.Item>
                    <Menu.Item key="6" disabled>Справочник</Menu.Item>
                    <Menu.Item key="7">Загрузка в БД</Menu.Item>
                <Divider/>
            </Menu>
    )
};
