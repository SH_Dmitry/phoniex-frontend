export function GetShortPersonalData(personal_data: string) {
    const _parsonal_data: string[] = personal_data.split(" ");
    return _parsonal_data[0] + " " + _parsonal_data[1].slice(0, 1) + "." + _parsonal_data[2].slice(0, 1) + ".";
}

export const getUnitById = (unit_id: string) => {
    if (unit_id) {
        switch (unit_id.toString()) {
            case "1":
                return "шт";
            case "2":
                return "кг";
            case "3":
                return "г";
            case "4":
                return "м";
            case "5":
                return "см";
            case "6":
                return "кв.м";
            case "7":
                return "кв.см";
            default:
                return "Ед."
        }
    }
};