const baseURL = 'http://localhost:3001';
import {saveAs} from 'file-saver';

export async function apiWrapper<T>(url: string, config: RequestInit): Promise<T> {
    let response = await fetch(new Request(baseURL + url, config));
    if (!response.ok) {
        if (response.status === 401) {
            location.reload(true);
        }
        const error = response || response.statusText;
        return Promise.reject(error);
    }
    if (response.status === 201) {
        return
    }
    return await response.json() as Promise<T>
}

export async function apiFileWrapper<T>(url: string, config: RequestInit, filename: string): Promise<T> {
    let response = await fetch(new Request(baseURL + url, config));

    if (!response.ok) {
        if (response.status === 401) {
            location.reload(true);
        }
        const error = response || response.statusText;
        return Promise.reject(error);
    }
    if (response.status === 201) {
        return
    }

    response.blob().then(blob => {
        console.log(blob);
        saveAs(blob, filename)
    });
    return
}