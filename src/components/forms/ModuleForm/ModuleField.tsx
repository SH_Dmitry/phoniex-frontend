import {Checkbox, Form, Input, Select} from "antd";
import React from "react";

interface BaseType {
    label: string
    name: string
    required: boolean
    message?: string
    placeholder?: string
    readonly?: boolean
}

export interface TextType extends BaseType {
    type: 'text'
}

export interface TextAreaType extends BaseType {
    type: 'text_area'
    rows?: number
}

export interface SelectorType extends BaseType {
    type: 'selector'
    childes: Array<{ id: number, name: string, icon: React.ReactNode }>
}

export interface CheckboxType extends BaseType {
    type: 'checkbox'
}

export type FormFieldType = TextType | TextAreaType | CheckboxType | SelectorType

export const Field = {
    Text: ({
               label,
               name,
               required = false,
               message,
               readonly
           }: TextType) => {
        return (
            <Form.Item
                key={name}
                name={name}
                label={label}
                hasFeedback
                rules={[{required, message}]}>
                <Input readOnly={readonly}/>
            </Form.Item>
        )
    },
    TextArea: ({
                   label,
                   name,
                   required = false,
                   message,
                   rows,
                   placeholder,
                   readonly
               }: TextAreaType) => {
        return (
            <Form.Item
                key={name}
                name={name}
                label={label}
                hasFeedback
                rules={[{required, message}]}>
                <Input.TextArea placeholder={placeholder} rows={rows} readOnly={readonly}/>
            </Form.Item>
        )
    },
    Checkbox: ({
                   label,
                   name,
                   required = false,
                   message,
                   readonly
               }: CheckboxType) => {
        return (
            <Form.Item
                key={name}
                name={name}
                label={label}
                rules={[{required, message}]}
                valuePropName="checked">
                <Checkbox disabled={readonly}/>
            </Form.Item>
        )
    },
    Selector: ({
                   label,
                   name,
                   required = false,
                   message,
                   childes,
                   readonly
               }: SelectorType) => {
        return (
            <Form.Item
                key={name}
                name={name}
                label={label}
                rules={[{required, message}]}>
                <Select disabled={readonly}>
                    {childes.map((value) => (
                            <Select.Option value={value.id}>
                                {value.icon}
                                {value.name}
                            </Select.Option>
                        )
                    )}
                </Select>
            </Form.Item>
        )
    }
};