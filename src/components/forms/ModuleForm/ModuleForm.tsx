import React from "react";
import {Form} from "antd";
import {CheckboxType, Field, FormFieldType, SelectorType, TextAreaType, TextType} from "./ModuleField";
import {FormInstance} from "antd/lib/form";

const layout = {
    labelCol: { span: 9 },
    wrapperCol: { span: 15 },
};

type FormProps<T> = {
    name: string
    initialValues: T | null
    handleSubmit: (event: React.MouseEvent) => void
    fields: Array<FormFieldType>
    formRef?: React.Ref<FormInstance>
}

export function ModuleForm<T>({name, initialValues, handleSubmit, fields, formRef}: FormProps<T>) {
    return (
        <Form
            {...layout}
            id={name}
            name={name}
            ref={formRef}
            initialValues={initialValues}
            onFinish={handleSubmit}>
            {fields.map(field => {
                switch (field.type) {
                    case "text": {
                        const _field = field as TextType;
                        return (
                            <Field.Text
                                type="text"
                                label={_field.label}
                                name={_field.name}
                                required={_field.required}
                                message={_field.message}
                                placeholder={_field.placeholder}
                                readonly={_field.readonly}
                            />
                        )
                    }
                    case "text_area": {
                        const _field = field as TextAreaType;
                        return (
                            <Field.TextArea
                                type="text_area"
                                label={_field.label}
                                name={_field.name}
                                required={_field.required}
                                message={_field.message}
                                placeholder={_field.placeholder}
                                readonly={_field.readonly}
                                rows={_field.rows}
                            />
                        )
                    }
                    case "selector": {
                        const _field = field as SelectorType;
                        return (
                            <Field.Selector
                                type="selector"
                                label={_field.label}
                                name={_field.name}
                                required={_field.required}
                                message={_field.message}
                                childes={_field.childes}
                                readonly={_field.readonly}
                            />
                        )
                    }
                    case "checkbox": {
                        const _field = field as CheckboxType;
                        return (
                            <Field.Checkbox
                                type="checkbox"
                                label={_field.label}
                                name={_field.name}
                                required={_field.required}
                                message={_field.message}
                                readonly={_field.readonly}
                            />
                        )
                    }
                }
            })
            }
        </Form>
    )
}


