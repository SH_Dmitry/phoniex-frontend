import {FormFieldType} from "./ModuleForm";
import {PartTypes} from "../../modules/PartsModule/components/PartTypeSelector/PartType.const";

const _ = require('lodash');

export const ProjectFormFields: Array<FormFieldType> = [
    {
        label: "Наименование",
        name: "name",
        required: true,
        message: "Введите наименование!",
        type: "text"
    },
    {
        label: "Децимальный номер",
        name: "decimal_num",
        required: true,
        message: "Введите децимальный номер!",
        type: "text"
    },
    {
        label:  'Тип',
        name: 'type_id',
        type: "selector",
        readonly: true,
        required: true,
        message: "Выберите тип!",
        childes: _.values(PartTypes)
    }
];

export const PartFormFields: Array<FormFieldType> = [
    {
        label: "Наименование",
        name: "name",
        required: true,
        message: "Введите наименование!",
        type: "text"
    },
    {
        label: "Децимальный номер",
        name: "decimal_num",
        required: true,
        message: "Введите децимальный номер!",
        type: "text"
    },
    {
        label:  'Тип',
        name: 'type_id',
        type: "selector",
        required: true,
        message: "Выберите тип!",
        childes: _.values(PartTypes)
    }
];

export const MaterialFormField: Array<FormFieldType> = [
    {
        label: "Материал",
        name: "material",
        required: true,
        message: "Введите материал!",
        type: "text",
    },
    {
        label: "Форма",
        name: "forma",
        required: false,
        type: "text",
    },
    {
        label: "Сортамент",
        name: "sortament",
        required: false,
        type: "text"
    }
];