import React from 'react';
import {Button, Modal, Typography} from "antd";
import {FormFieldType, ModuleForm} from "../../forms/ModuleForm";

import styles from './ModuleFormDialog.module.less'
import {FormInstance} from "antd/lib/form";

const {Title, Paragraph} = Typography;

type DialogProps<T> = {
    name: string
    title: string,
    description?: string
    submitText: string
    visible: boolean,
    initialValues?: T
    fields: Array<FormFieldType>
    formRef?: React.Ref<FormInstance>
    handleClose: () => void
    handleSubmit: (T) => void
}

const titleWithDescription = (title: string, description: string) => {
    return (
        <>
            <Title level={4}>{title}</Title>
            <Paragraph >{description}</Paragraph>
        </>
    )
};

export function ModuleFormDialog<T>({
                                        name,
                                        title,
                                        description,
                                        submitText,
                                        visible,
                                        initialValues,
                                        fields,
                                        formRef,
                                        handleClose,
                                        handleSubmit
                                    }: DialogProps<T>) {


    return (
        <Modal
            title={{description}?titleWithDescription(title,description):{title}}
            visible={visible}
            onCancel={handleClose}
            centered
            className={styles.root}
            forceRender
            footer={[
                <Button
                    key="back"
                    onClick={handleClose}
                >
                    Отмена
                </Button>,
                <Button
                    key="submit"
                    htmlType="submit"
                    form={name}
                    type="primary"
                >
                    {submitText}
                </Button>,
            ]}
        >
            <ModuleForm<T>
                name={name}
                handleSubmit={handleSubmit}
                initialValues={initialValues}
                fields={fields}
                formRef={formRef}
            />
        </Modal>
    );
}