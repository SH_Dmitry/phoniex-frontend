import styles from "./DropArea.module.less";
import React from "react";
import {SisternodeOutlined} from "@ant-design/icons/lib";

export const DropArea = ({canDrop, isOver}) => {
    const isActive = canDrop && isOver;
    let backgroundColor;
    if (isActive) {
        backgroundColor = '#1890ff'
    } else if (canDrop) {
        backgroundColor = '#1871d9'
    }
    return (
        <>
            <div className={styles.dropArea} style={{backgroundColor}}>
                <SisternodeOutlined style={{fontSize: '64px', color: "white"}}/>
            </div>
        </>
    )
};