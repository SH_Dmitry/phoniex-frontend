const express = require('express');
const app = express();
const portNumber = 8080;
const sourceDir = 'dist';

app.use(express.static(sourceDir));
app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname, 'src/index.html'), function(err) {
    if (err) {
      res.status(500).send(err)
    }
  })
});
app.listen(portNumber, () => {
  console.log(`Express web server started: http://localhost:${portNumber}`);
  console.log(`Serving content from /${sourceDir}/`);
});
