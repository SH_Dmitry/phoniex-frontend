// shared config (dev and prod)
const fs = require("fs");
const lessToJs = require("less-vars-to-js");
const path = require('path');
const {resolve} = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const themeVariables = lessToJs(fs.readFileSync(path.join(__dirname, '../../src/ant-theme-vars.less'), 'utf8'));

module.exports = {
    resolve: {
        extensions: ['.js', 'jsx', '.json', '.ts', '.tsx', ".less", ".css"],
    },
    context: resolve(__dirname, '../../src'),
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loaders: ['babel-loader', 'ts-loader'],
                exclude: [/node_modules/]
            },
            {
                test: /\.(jsx?)$/,
                loaders: ['babel-loader'],
                exclude: [/node_modules/]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.module.less$/,
                use: [
                    {loader: "style-loader"},
                    {
                        loader: "css-loader",
                        options: {
                            modules: true
                        }
                    },
                    {loader: "less-loader"}
                ]
            },
            {
                test: /\.less$/,
                exclude: /\.module.less$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"},
                    {
                        loader: "less-loader",
                        options: {
                            lessOptions: {
                                javascriptEnabled: true,
                                modifyVars: themeVariables,
                                root: path.resolve(__dirname, '../../src')
                            }
                        }
                    }
                ]
            },
            {
                //For images
                test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/,
                use: [
                    'url-loader'
                ]
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, '../../src', 'index.html')
        }),
        new CleanWebpackPlugin(),
    ],
    watchOptions: {
        ignored: /dist/,
    },
};
